package be.uantwerpen;

import java.util.Set;
import java.util.TreeSet;

public class TimeSeriesDataset{
	public double[][] features;
	public int[] labels;
	public int[] originalTimeseriesIdx; //keep mapping to original time series after mapping
	
	public Set<Integer> getLabelSet(){
		Set<Integer> labelSet = new TreeSet<>();
		for(int i=0; i<labels.length; i++) {
			labelSet.add(labels[i]);
		}
		return labelSet;
	}
	
	public double min() {
		double min_v = Double.MAX_VALUE;
		for(int i=0; i< features.length; i++) {
			for(int j=0; j < features[i].length; j+=1) {
				min_v = Math.min(min_v, features[i][j]);
			}
		}
		return min_v;
	}
	
	public double max() {
		double max_v = Double.MIN_VALUE;
		for(int i=0; i< features.length; i++) {
			for(int j=0; j < features[i].length; j+=1) {
				max_v = Math.max(max_v, features[i][j]);
			}
		}
		return max_v;
	}
	
	public Set<Double> unique(){
		TreeSet<Double> set = new TreeSet<>();
		for(int i=0; i< features.length; i++) {
			for(int j=0; j < features[i].length; j+=1) {
				set.add(features[i][j]);
			}
		}
		return set;
	}
	
	public DiscreteTimeSeriesDataset discrete() {
		DiscreteTimeSeriesDataset ts = new DiscreteTimeSeriesDataset();
		ts.labels = labels;
		ts.features = new int[features.length][];
		ts.originalTimeseriesIdx = originalTimeseriesIdx;
		for(int i=0; i< features.length; i++) {
			ts.features[i] = new int[features[i].length];
			for(int j=0; j < features[i].length; j+=1) {
				ts.features[i][j] = (int)features[i][j];
			}
		}
		return ts;
	}
	
	public TimeSeriesDataset copy() {
		TimeSeriesDataset ts = new TimeSeriesDataset();
		ts.labels = labels.clone();
		ts.features = new double[features.length][];
		for(int i=0; i< features.length; i++) {
			ts.features[i] = new double[features[i].length];
			for(int j=0; j < features[i].length; j+=1) {
				ts.features[i][j] = features[i][j];
			}
		}
		return ts;
	}
}