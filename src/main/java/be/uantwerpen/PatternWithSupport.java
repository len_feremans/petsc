package be.uantwerpen;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import be.uantwerpen.util.Pair;

public class PatternWithSupport{
	public List<Integer> pattern;
	public int support;

	public PatternWithSupport() {}
	public PatternWithSupport(List<Integer> pattern, int support) {
		this.pattern = pattern;
		this.support = support;
	}
	
	
	@Override
	public String toString() {
		return "pattern:" + Arrays.toString(pattern.toArray()) + " support: " + support;
	}
	
	public static void save(File file, List<? extends PatternWithSupport> patterns) throws IOException {
		if(!file.getParentFile().exists()) {
			file.getParentFile().mkdirs();
		}
		FileWriter writer = new FileWriter(file);
		for(int i=0; i<patterns.size(); i++) {
			PatternWithSupport pattern = patterns.get(i);
			for(int j=0; j < pattern.pattern.size()-1; j++) {
				writer.write(String.valueOf(pattern.pattern.get(j)));
				writer.write(",");
			}
			writer.write(String.valueOf(pattern.pattern.get(pattern.pattern.size()-1)));
			writer.write(";");
			writer.write(String.valueOf(pattern.support));
			writer.write("\n");
		}
		writer.close();
		System.out.println("Saved " + file.getName());
	}
	
	public static void save2(File file, List<Pair<List<Integer>, Float>> patterns) throws IOException {
		if(!file.getParentFile().exists()) {
			file.getParentFile().mkdirs();
		}
		FileWriter writer = new FileWriter(file);
		for(int i=0; i<patterns.size(); i++) {
			Pair<List<Integer>, Float> pattern = patterns.get(i);
			for(int j=0; j < pattern.getFirst().size()-1; j++) {
				writer.write(String.valueOf(pattern.getFirst().get(j)));
				writer.write(",");
			}
			writer.write(String.valueOf(pattern.getFirst().get(pattern.getFirst().size()-1)));
			writer.write(";");
			writer.write(String.valueOf(pattern.getSecond()));
			writer.write("\n");
		}
		writer.close();
		System.out.println("Saved " + file.getName());
	}
}
