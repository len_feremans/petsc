package be.uantwerpen;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.TreeMap;
import java.util.regex.Pattern;

import be.uantwerpen.util.Utils;

/**
 *  Pattern-based Embedding for Time Series classification (PETS)
 * @author lfereman
 *
 * DONE: (i) (voor time series) transformeer continue time series naar discrete sequence met SAX
 * DONE: (ii) mine K niet-redundante patronen 
 * DONE: (iii) maak voor elke sequentie een representatie met K features, 
 * 		 waarbij elke feature overeenkomt met de met similarity/match met een patroon
 * In pythong: (iv) gebruik standaard binary/multi-class classificatie algoritme om voorspellingen te doen met deze representatie
 *
 */
public class PETS {

	TreeMap<String,Integer> labelsTranslate = new TreeMap<>();
	private int readLabel(String s) {
		try {
			float labelAsFloat = Float.valueOf(s);
			if(labelAsFloat % 1 != 0) {
				System.err.println("Warning: Found non-integer label");
			}
			return (int)labelAsFloat;
		}catch(Exception e) {
			//could be symbol
			if(labelsTranslate.get(s) != null) {
				return labelsTranslate.get(s);
			}
			else {
				labelsTranslate.put(s, labelsTranslate.size()+1);
				return labelsTranslate.get(s);
			}
		}
	}
	/** 
	 * Load file in TS format, see  http://www.timeseriesclassification.com/
	 * @throws IOException 
	 */
	public TimeSeriesDataset readTS(File tsFile) throws IOException {
		try {
			List<String> lines = Utils.readFile(tsFile);
			int nrOfSeries = 0;
			for(String line: lines) {
				if(line.startsWith("#") || line.startsWith("@"))
					continue;
				else
					nrOfSeries++;
			}
			double[][] features = new double[nrOfSeries][];
			int[] labels = new int[nrOfSeries];
			Pattern splitComma = Pattern.compile("(\\s)*(\\,)(\\s)*");
			Pattern splitLabel = Pattern.compile("(\\s)*(\\:)(\\s)*");
			int idx =0;
			for(String line: lines) {
				if(line.startsWith("#") || line.startsWith("@") || line.trim() == "")
					continue;
				//otherwise, parse line like: 1.7327257,-0.58473404,-0.58372869,-0.5786034,1.7327257,-0.58473404:9
				String[] tokens = splitComma.split(line);
				double[] featuresCurrent = new double[tokens.length];
				for(int i=0; i < tokens.length-1; i++) {
					featuresCurrent[i] = Double.valueOf(tokens[i]);
				}
				String[] last = splitLabel.split(tokens[tokens.length-1]);
				featuresCurrent[featuresCurrent.length-1] = Double.valueOf(last[0]);
				int label = readLabel(last[1]);
				features[idx] = featuresCurrent;
				labels[idx] = label;
				idx++;
			}
			TimeSeriesDataset ts = new TimeSeriesDataset();
			ts.features = features;
			ts.labels = labels;
			System.out.println("Loading " + tsFile.getName() +
					" shape: " + String.valueOf(ts.features.length) + "," + String.valueOf(ts.features[0].length)
					+ " labels: " + String.valueOf(labelsTranslate.keySet()));
			return ts;
		}
		catch(Exception e) {
			e.printStackTrace();
			throw(e);
		}
	}
}
