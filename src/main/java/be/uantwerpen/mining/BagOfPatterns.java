package be.uantwerpen.mining;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import be.uantwerpen.DiscreteTimeSeriesDataset;
import be.uantwerpen.PETS;
import be.uantwerpen.TimeSeriesDataset;
import be.uantwerpen.util.CountMap;
import be.uantwerpen.util.Utils;
import net.seninp.jmotif.sax.NumerosityReductionStrategy;
import net.seninp.jmotif.sax.SAXException;
import net.seninp.jmotif.sax.TSProcessor;
import net.seninp.jmotif.sax.alphabet.Alphabet;
import net.seninp.jmotif.sax.alphabet.NormalAlphabet;


/** 
 * Implementation of Lin bag-of-pattern method
 * 
 * Rotation-invariant similarity in time series using bag-of-patterns representation
 * by Jessica Lin, Rohan Khade and Yuan Li
 * In Journal Intelligent Information Systemss (2012) 
 * 
 * Do: normalize each segment and do PAA
 * 
 * Started from jMotif/SAX-SVM, i.e., 
 * see: https://github.com/jMotif/sax-vsm_classic/blob/master/src/main/java/net/seninp/jmotif/text/TextProcessor.java
 * 
 * Note: Bag of fixed sequences would be a better name
 * @author lfereman
 *
 */
public class BagOfPatterns {

	private TSProcessor tp = new TSProcessor(); //From Sax
	private Alphabet na = new NormalAlphabet();

	//changed implemenation WordBag, seemed over-engineerd in jMotif
	public class WordBag{
		public String label;
		public CountMap<String> words = new CountMap<>();

		public WordBag(String label) {
			super();
			this.label = label;
		}

		public synchronized void mergeWith(WordBag otherBag) {
			for (Entry<String, Integer> entry : otherBag.words.getMap().entrySet()) {
				this.words.add(entry.getKey(), entry.getValue());
			}
		}

	}

	public static class Params{
		public int windowSize;
		public int paaSize;
		public int alphabetSize;
		public double nThreshold = 0.01; //threshold for normalisatino
		public NumerosityReductionStrategy nrStartegy = NumerosityReductionStrategy.EXACT; 

	}

	public WordBag seriesToWordBag(String label, double[] ts, Params params) throws SAXException, net.seninp.jmotif.sax.SAXException {

		WordBag resultBag = new WordBag(label);

		// scan across the time series extract sub sequences, and convert them to strings
		char[] previousString = null;

		for (int i = 0; i <= ts.length - params.windowSize; i++) {

			// fix the current subsection
			double[] subSection = Arrays.copyOfRange(ts, i, i + params.windowSize);

			// Z normalize it
			subSection = tp.znorm(subSection, params.nThreshold);

			// perform PAA conversion if needed
			double[] paa = tp.paa(subSection, params.paaSize);

			// Convert the PAA to a string.
			char[] currentString = tp.ts2String(paa, na.getCuts(params.alphabetSize));

			if (null != previousString) {

				if (NumerosityReductionStrategy.EXACT.equals(params.nrStartegy)
						&& Arrays.equals(previousString, currentString)) {
					// NumerosityReduction
					continue;
				}
				//Commented out
				//else if (NumerosityReductionStrategy.MINDIST.equals(params.nrStartegy)
				//		&& sp.checkMinDistIsZero(previousString, currentString)) {
				//continue;
				//}

			}

			previousString = currentString;

			resultBag.words.add(String.valueOf(currentString));
		}

		return resultBag;
	}

	public Map<String, WordBag> labeledSeries2WordBags(Map<String, List<double[]>> data, Params params)
			throws SAXException, SAXException {

		// make a map of resulting bags
		Map<String, WordBag> preRes = new HashMap<String, WordBag>();

		// process series one by one building word bags
		for (Entry<String, List<double[]>> e : data.entrySet()) {

			String classLabel = e.getKey();
			WordBag bag = new WordBag(classLabel);

			for (double[] series : e.getValue()) {
				WordBag cb = seriesToWordBag("tmp", series, params);
				bag.mergeWith(cb);
			}

			preRes.put(classLabel, bag);
		}

		return preRes;
	}

	//interface with PETS
	public DiscreteTimeSeriesDataset computeBagOfPatternsAndEmbeddingPerLabel(TimeSeriesDataset ts, Params params) throws SAXException {
		
		Map<String, List<double[]>> data = makeSAXSVMData(ts);
		Map<String, WordBag> wordbags = labeledSeries2WordBags(data,params);
		//Save "embedding"
		DiscreteTimeSeriesDataset output = new DiscreteTimeSeriesDataset();
		output.features = new int[ts.features.length][];
		output.labels = ts.labels;
		for(int i=0; i<ts.features.length; i++) {
			//get all possible words from all time series, and use them as feature
			WordBag wordBagClass = wordbags.get(String.valueOf(ts.labels[i])); 
			//count words in current sequence. NOTE: Should be cached for efficiency
			WordBag wordBagTs = seriesToWordBag(String.valueOf(ts.labels[i]), ts.features[i],  params);
			int[] embedding = new int[wordBagClass.words.getMap().size()];
			int idx=0;
			for(Entry<String,Integer> entry: wordBagClass.words.getMap().entrySet()) {
				int frequencyInTS = wordBagTs.words.get(entry.getKey());
				embedding[idx] = frequencyInTS;
				idx+=1;
			}
			output.features[i] = embedding;
		}
		return output;
	}

	//interface with PETS
	public WordBag computeBagOfPatterns(TimeSeriesDataset ts, Params params) throws SAXException {
		Map<String, List<double[]>> data = makeSAXSVMData(ts);
		Map<String, WordBag> wordbags = labeledSeries2WordBags(data,params);
		WordBag allWords = new WordBag(null);
		for(WordBag wordbagPerClass: wordbags.values()) {
			allWords.mergeWith(wordbagPerClass);
		}
		//sort patterns alphabetically
		System.out.println("Found " + allWords.words.getMap().size() + " words");
		List<Entry<String,Integer>> lst = new ArrayList<>(allWords.words.getMap().entrySet());
		Collections.sort(lst, (e1, e2) -> e1.getKey().compareTo(e2.getKey()));
		//print
		for(Entry<String,Integer> entry: lst.subList(0, Math.min(lst.size(),10))) {
			//convert abc to [0,1,2]
			List<String> normalRepr = new ArrayList<>();
			for (int i = 0; i < entry.getKey().length(); i++){
			    char ch = entry.getKey().charAt(i);        
			    normalRepr.add(String.valueOf(ch - 'a'));
			}
			System.out.println("Pattern " +  String.join(", ", normalRepr) + ": " + entry.getValue());
		}
		System.out.println("...");
		return allWords;
	}

	private DiscreteTimeSeriesDataset createEmbedding(TimeSeriesDataset ts, Params params, WordBag allWords) throws SAXException {
		//sort words alphabetically
		List<Entry<String,Integer>> lst = new ArrayList<>(allWords.words.getMap().entrySet());
		Collections.sort(lst, (e1, e2) -> e1.getKey().compareTo(e2.getKey()));
		//Save "embedding"
		DiscreteTimeSeriesDataset output = new DiscreteTimeSeriesDataset();
		output.features = new int[ts.features.length][];
		output.labels = ts.labels;
		for(int i=0; i<ts.features.length; i++) {
			//count words in current sequence. NOTE: Should be cached for efficiency
			WordBag wordBagTs = seriesToWordBag(String.valueOf(ts.labels[i]), ts.features[i],  params);
			int[] embedding = new int[allWords.words.getMap().size()];
			int idx=0;
			//in sorted order
			for(Entry<String,Integer> entry: lst) {
				int frequencyInTS = wordBagTs.words.get(entry.getKey());
				embedding[idx] = frequencyInTS;
				idx+=1;
			}
			output.features[i] = embedding;
		}
		return output;
	}

	//convert to format SaxSVM
	private Map<String, List<double[]>> makeSAXSVMData(TimeSeriesDataset ts) {
		Set<Integer> labels = ts.getLabelSet();
		Map<String,List<double[]>> data = new HashMap<>();
		for(Integer label: labels) {
			List<double[]> series = new ArrayList<>();
			for(int i=0; i<ts.features.length; i++) {
				if(label.equals(ts.labels[i])) {
					series.add(ts.features[i]);
				}
			}
			data.put(String.valueOf(label), series);
		}
		return data;
	}


	public static void main(String[] args) throws IOException, SAXException {
		//parameter parsing
		System.out.println(">>" + Arrays.toString(args));
		Map<String,String> paramTypes = new HashMap<String,String>();
		paramTypes.put("input", "file");
		paramTypes.put("output", "file"); //output embedding
		paramTypes.put("input_test", "file");
		paramTypes.put("output_test", "file"); //output embedding
		paramTypes.put("bins", "int"); //SAX
		paramTypes.put("paa_win", "int");
		paramTypes.put("window", "int"); //Mining
		if(args.length < 3) {
			System.err.println("Expected: >>java be.uantwerpen.SAXSVM -input ./data/mytimeseries.ts -bins 10 -paa_win 100"
					+ " -window 5 -output embedding.txt");
		}
		Map<String,Object> values = Utils.getParameters(args, paramTypes);
		//run
		PETS pets = new PETS();
		TimeSeriesDataset ts = pets.readTS(new File((String)values.get("input")));
		BagOfPatterns saxSVM = new BagOfPatterns();
		Params params = new Params();
		params.alphabetSize = (Integer)values.get("bins");
		params.paaSize = (Integer)values.get("paa_win");
		params.windowSize = (Integer)values.get("window");
		params.nrStartegy = NumerosityReductionStrategy.NONE; 
		//DiscreteTimeSeriesDataset ts_embedding = saxSVM.computeBagOfPatternsAndEmbeddingPerLabel(ts, params);
		WordBag wordBag = saxSVM.computeBagOfPatterns(ts, params);
		DiscreteTimeSeriesDataset ts_embedding = saxSVM.createEmbedding(ts, params, wordBag);
		ts_embedding.save(new File((String)values.get("output")));
		System.out.println("Saved " + (String)values.get("output"));
		//for input_test/output_test
		if(values.get("input_test") != null && values.get("output_test") != null) {
			TimeSeriesDataset ts_test = pets.readTS(new File((String)values.get("input_test")));
			DiscreteTimeSeriesDataset ts_embedding_test = saxSVM.createEmbedding(ts_test, params, wordBag);
			ts_embedding_test.save(new File((String)values.get("output_test")));
			System.out.println("Saved " + (String)values.get("output_test"));
		}
	}	

}
