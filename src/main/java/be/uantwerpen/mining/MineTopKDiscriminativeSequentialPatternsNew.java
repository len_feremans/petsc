package be.uantwerpen.mining;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.PriorityQueue;
import java.util.Set;
import java.util.TreeSet;

import be.uantwerpen.DiscreteTimeSeriesDataset;
import be.uantwerpen.DiscretiseNew;
import be.uantwerpen.PETS;
import be.uantwerpen.PatternWithSupport;
import be.uantwerpen.TimeSeriesDataset;
import be.uantwerpen.util.CountMap;
import be.uantwerpen.util.Pair;
import be.uantwerpen.util.Utils;
import net.seninp.jmotif.sax.SAXException;

/**
 * New pattern mining algorithm
 * 
 * Use:
 *  - mine top-k patterns where contrast is high (ignore length of pattern or cohesion) + take positive and negative patterns
 *  - TODO: for multi-class take join patterns  either top-k contrast of t/nr-labels	
 *  - constraints are:
 *  		- duration
 *  		- min_size
 *  		- max_size
 *  - use heuristic:
 *  		- always select patterns with highest sup(A) - (B)
 *  
 * Note: Assumes sliding window during pre-processing as we only count the "first occurrence" w.r.t. gap constraints 
 * Note: Not an exact algorithm... we limit candidates using restriction on Fringe    
 * @author lfereman
 *
 */
public class MineTopKDiscriminativeSequentialPatternsNew {

	private DiscreteTimeSeriesDataset ts;
	private int minsize;
	private int maxsize;
	private double duration; //relative to maxsize, e.g. *2 or *1,1
	private int k;
	private int minsup;
	private int label;
	private CountMap<Integer> labelCounts;

	public static boolean TRACE = false;

	public static class Projection{
		public Map<Integer,Occurrence> projectionInClass = new HashMap<>();
		public Map<Integer,Occurrence> projectionOtherwise = new HashMap<>();	
	}

	class PatternQueueItem implements Comparable<PatternQueueItem>{
		List<Integer> pattern;
		Float support = null;
		
		@Override
		public int compareTo(PatternQueueItem o) { //sort on descending support, e.g. remove() will return pattern with lowest support
			return support.compareTo(o.support);
		}
	}
	
	class QueueItem implements Comparable<QueueItem>{
		List<Integer> pattern;
		Projection projection;
		Set<Integer> candidateItems;
		Float support = null;

		@Override
		public int compareTo(QueueItem o) {
			return o.support.compareTo(support);
		}
	}
	
    public List<Pair<List<Integer>,Float>> mineAllLabels(DiscreteTimeSeriesDataset ts_d, int k, int max_iter_without_change, int minsize, int maxsize, double duration, int minsup) {		
    		List<Pair<List<Integer>,Float>> allPatterns = new ArrayList<Pair<List<Integer>,Float>>();
    		this.labelCounts = ts_d.labelCounts();
    		System.out.println(labelCounts);
    		for(int label: labelCounts.keySet()) {
    			//int k_label = (2 * k)/labelCounts.keySet().size(); //or proportional to size of label
    			int k_label = k;
    			List<Pair<List<Integer>,Float>> patterns = mine(ts_d, label, k_label, max_iter_without_change, minsize, maxsize, duration, minsup);
    			allPatterns.addAll(patterns);
    		}
    		//remark: information is lost on which label support is computed
    		//some patterns might also be duplicated
    		Collections.sort(allPatterns, (p1,p2) -> p2.getSecond().compareTo(p1.getSecond())); //sort on descending support;
    		return allPatterns.subList(0, Math.min(allPatterns.size(), k));
    }
    
    public void setParameters(DiscreteTimeSeriesDataset ts, int label, int k, int minsize, int maxsize, double duration, int minsup) {
    		this.ts = ts;
    		this.label = label;
    		this.minsize = minsize;
    		this.maxsize = maxsize;
    		this.duration = duration;
    		this.k = k;
    		this.minsup = minsup;
    }
	
	public List<Pair<List<Integer>, Float>>  mine(DiscreteTimeSeriesDataset ts, int label, int k, int max_iter_without_change, int minsize, int maxsize, double duration, int minsup) {		
		this.ts = ts;
		this.label = label;
		this.minsize = minsize;
		this.maxsize = maxsize;
		this.duration = duration;
		this.minsup = minsup;
		this.k = k;
		//patterns
		PriorityQueue<PatternQueueItem> patternSet = new PriorityQueue<>();
		//level 1: singletons
		//queue initialisation
		Set<Integer> items = ts.unique(); //excludes "-1" which is a special symbol
		PriorityQueue<QueueItem> queue = new PriorityQueue<QueueItem>(items.size());
		for(Integer item: items) {
			QueueItem queueItem = new QueueItem();
			queueItem.projection = computeProjectionSingleton(item);
			queueItem.pattern = new ArrayList<Integer>();
			queueItem.pattern.add(item);
			queueItem.candidateItems = candidateItems(queueItem.projection, queueItem.pattern);
			queueItem.support = computeSupport(queueItem.projection, queueItem.pattern);
			queue.add(queueItem);
		}
		int last_iteration_with_update_to_heap = 0;
		int iterations=0;
		//levels n+1:
		while(!queue.isEmpty() && (iterations-last_iteration_with_update_to_heap) <  max_iter_without_change) { 
			iterations++;
			QueueItem currentItem =  queue.remove();
			String indent = "";
			if(TRACE) {
				for(int i=0; i<currentItem.pattern.size()-1; i++) {
					indent += "\t";
				}
				System.out.println(indent + "pattern:" + Arrays.toString(currentItem.pattern.toArray()) + " support:" + currentItem.support);
				System.out.println(indent + "   candidate items:" + Arrays.toString(currentItem.candidateItems.toArray()));
				System.out.println(indent + "   projection in class:" + String.valueOf(currentItem.projection.projectionInClass)); 
				System.out.println(indent + "   projection out class:" + String.valueOf(currentItem.projection.projectionOtherwise)); 
				
				//for(Entry<Integer,List<Integer>> entry: first.projection.offsets.entrySet()) {
				//	System.out.println("   seq:" +  entry.getKey() + ": " + Arrays.toString(entry.getValue().toArray()));
				//}
			}
			if(iterations % 100000 == 0) {
				System.out.println("nr patterns: " + patternSet.size());
			}
			//no candidate items and size < minsize
			if(currentItem.candidateItems.size() == 0 && currentItem.pattern.size() < minsize) {
				continue;
			}
			//prune: if possitive support < 0
			if(currentItem.projection.projectionInClass.size() +  currentItem.projection.projectionOtherwise.size() < this.minsup) {
				continue;
			}
			//no candidate items and size > minsize
			if(currentItem.candidateItems.size() == 0 || (currentItem.candidateItems.size() > 0 && currentItem.pattern.size() == maxsize)) {
				//only add if larger or equal to minsize
				PatternQueueItem pattern = new PatternQueueItem();
				pattern.pattern = currentItem.pattern;
				pattern.support = currentItem.support;
				if(TRACE) {
					System.out.print(indent + ">>Adding pattern " + Arrays.toString(currentItem.pattern.toArray()) + "? ");
				}
				if(patternSet.size() < this.k) {
					patternSet.add(pattern);
					last_iteration_with_update_to_heap = iterations;
					if(TRACE) {
						System.out.println("yes (heap < k)");
					}
				}
				else if(pattern.support > patternSet.peek().support) {
					patternSet.remove();
					patternSet.add(pattern);
					last_iteration_with_update_to_heap = iterations;
					if(TRACE) {
						System.out.println("yes (support > min_heap)");
					}
				}
				else {
					if(TRACE){
						System.out.println("no");
					}
				}
				continue;
			}
			//------------------------ recursion --------------------
			//get next item
			int newItem = currentItem.candidateItems.iterator().next();
			//generate pattern with +1 length
			QueueItem nextQueueItem = new QueueItem();
			nextQueueItem.projection = computeProjectionIncremental(currentItem.projection, currentItem.pattern, newItem);
			nextQueueItem.pattern = new ArrayList<Integer>(currentItem.pattern);
			nextQueueItem.pattern.add(newItem);
			nextQueueItem.candidateItems = candidateItems(nextQueueItem.projection, nextQueueItem.pattern);
			nextQueueItem.support = computeSupport(nextQueueItem.projection, nextQueueItem.pattern);
			queue.add(nextQueueItem);
			//add current
			currentItem.candidateItems.remove(newItem);
			queue.add(currentItem);
		}
		//System.out.println("Stopped: #iterations=" + iterations 
		//					  + " queue size: " + queue.size() 
		//					  + " number of iterations without change: " + (iterations - last_iteration_with_update_to_heap));
		//sort on support
		ArrayList<Pair<List<Integer>,Float>> patternsSorted = new ArrayList<>();
		while(!patternSet.isEmpty()){
			PatternQueueItem patternInQueue = patternSet.remove();
			patternsSorted.add(new Pair<>(patternInQueue.pattern, patternInQueue.support));
		}
		Collections.reverse(patternsSorted);
		return patternsSorted;
	}

	//compute sequences and first occurrence into each sequence where singleton occurs
	Projection computeProjectionSingleton(int item) {
		try {
		Projection p = new Projection();
		for(int ts_idx=0; ts_idx<ts.features.length; ts_idx++) {
			//index of item in sequence
			int[] current_ts = ts.features[ts_idx];
			Occurrence occ = null;
			for(int j=0; j < current_ts.length && j<current_ts.length - this.minsize + 1; j++) {
				if(current_ts[j] == item) {
					occ = new Occurrence(j,j+1);
					break;
				}
			}
			if(occ != null) {
				if(ts.labels[ts_idx] == this.label) {
					p.projectionInClass.put(ts_idx, occ);
				}
				else {
					p.projectionOtherwise.put(ts_idx, occ);
				}
			}
		}
		return p;
		}catch(NullPointerException e) {
			System.out.println(ts.features.length);
			System.out.println(Arrays.toString(ts.features[0]));
			e.printStackTrace();
			return null;
		}
	}
	
	//compute projection of <a,b,c> based on <a,b> in and out class
	Projection computeProjectionIncremental(Projection projection, List<Integer> pattern, int newItem) { 
		Projection incrementalProjection = new Projection();
		incrementalProjection.projectionInClass = computeProjectionIncrementalAux(projection.projectionInClass, pattern, newItem);
		incrementalProjection.projectionOtherwise = computeProjectionIncrementalAux(projection.projectionOtherwise, pattern, newItem);
		return incrementalProjection;
	}	

	//compute projection of <a,b,c> based on <a,b>
	Map<Integer,Occurrence>  computeProjectionIncrementalAux(Map<Integer,Occurrence> projection, List<Integer> pattern, int newItem) {
		Map<Integer,Occurrence> p = new HashMap<>();
		for(Entry<Integer,Occurrence> entry: projection.entrySet()) {
			int[] ts_current = ts.features[entry.getKey()];
			Occurrence occ = entry.getValue();
			//maxsize & duration constraint
			//remark: is global constraint on projection windows -> for actual occurrences we need a |pattern_size| * duration constraint!
			//thus filtering needed when computing support
			int max_size_within_duration = (int)(this.maxsize * this.duration);
			int current_max_size = occ.start + max_size_within_duration; 
			//also a gap constraint, e.g. max_size = 6 and duration=1 -> no gaps... or
			//or max_size = 10 and duration 1.1 -> at most 1 gap
			int maximumNumberOfGaps = max_size_within_duration - this.maxsize;
			int currentGaps = (occ.end - occ.start) - pattern.size();
			int remainingGaps = maximumNumberOfGaps - currentGaps;
			Occurrence occNew = null;
			for(int j=occ.end; j < ts_current.length && j < current_max_size && j < occ.end + remainingGaps + 1; j+=1) {
				if(ts_current[j] == newItem) {
					occNew = new Occurrence(occ.start, j+1);
				}
			}
			if(occNew != null) {
				p.put(entry.getKey(), occNew);
			}
		}
		return p;
	}

	//compute candidates with respect to gap constraints
	Set<Integer> candidateItems(Projection projection, List<Integer> pattern){
		Set<Integer> items = new TreeSet<>(); 
		for(Entry<Integer,Occurrence> entry: projection.projectionInClass.entrySet()) {
			int[] ts_current = ts.features[entry.getKey()];
			Occurrence occ = entry.getValue();
			//maxsize & duration constraint
			int max_size_within_duration = (int)(this.maxsize * this.duration);
			int current_max_size = occ.start + max_size_within_duration;
			//also a gap constraint, e.g. max_size = 6 and duration=1 -> no gaps... or
			//or max_size = 10 and duration 1.1 -> at most 1 gap
			int maximumNumberOfGaps = max_size_within_duration - this.maxsize;
			int currentGaps = (occ.end - occ.start) - pattern.size();
			int remainingGaps = maximumNumberOfGaps - currentGaps;
			for(int j=occ.end; j < ts_current.length && j < current_max_size && j < occ.end + remainingGaps + 1; j+=1) {
				items.add(ts_current[j]);
			}
		}
		items.remove(-1); //-1 is a special item, e.g. a "gap" or "null" item
		return items;
	}


	//CHANGED in NEW variant
	//discriminative support: new formula... 
	private float computeSupport(Projection projection, List<Integer> pattern) {
		float supportPos = projection.projectionInClass.size();
		float supportNeg = projection.projectionOtherwise.size();
		return Math.abs(supportPos / (float)(this.labelCounts.get(this.label)) - supportNeg / (float)(this.ts.labels.length - this.labelCounts.get(this.label)));
	}
	
	//save embedding
	//Note: time series is parameter as can be test time series in classification task
	public DiscreteTimeSeriesDataset createEmbeddingWithWindows(DiscreteTimeSeriesDataset ts,  List<Pair<List<Integer>, Float>> patterns){
		//important for projectionPattern! 
		this.ts = ts;
		//create mapping
		Set<Integer> numberOfOriginalTimeseries = new HashSet<>();
		for(int i=0; i<ts.features.length; i++) {
			int original_idx = ts.originalTimeseriesIdx[i];
			numberOfOriginalTimeseries.add(original_idx);
		}
		DiscreteTimeSeriesDataset result = new DiscreteTimeSeriesDataset();
		result.features = new int[numberOfOriginalTimeseries.size()][];
		for(int i=0; i<numberOfOriginalTimeseries.size(); i++) {
			result.features[i] = new int[patterns.size()];
		}
		result.labels = new int[numberOfOriginalTimeseries.size()];
		for(int p=0; p<patterns.size(); p++) {
			PatternWithSupport patternObj = new PatternWithSupport(patterns.get(p).getFirst(), 1);
			Projection projectionP = projectionPattern(patternObj);
			for(int window_ts_idx:projectionP.projectionInClass.keySet()) {
				int series_ts_idx = this.ts.originalTimeseriesIdx[window_ts_idx];
				result.features[series_ts_idx][p]+=1;
			}
			for(int window_ts_idx:projectionP.projectionOtherwise.keySet()) {
				int series_ts_idx = this.ts.originalTimeseriesIdx[window_ts_idx];
				result.features[series_ts_idx][p]+=1;
			}
		}
		return result;
	}
	
	//save pattern occurrences for visualisation purposes
	//Note: save pattern occurrences in non-windowed time series
	//patterns;original timeseries;occurrence1, occurence2
	//0,1,2; 1; 10,11,12; 102,104,110
	//0,1,2; 2; 1,3,5; 103,104,105
	public void createAndSavePatternOccurrences(TimeSeriesDataset originalTs, int window, int paa_win, List<Pair<List<Integer>, Float>> patterns, File patternOccurrencesFile) throws IOException{
		FileWriter writer = new FileWriter(patternOccurrencesFile);
		SlidingWindow sw = new SlidingWindow();
		List<Pair<Integer,Integer>> ranges = sw.computeSlidingWindowRanges(originalTs, window);
		int divider = window / paa_win;
		Map<List<Integer>, Map<Integer,List<List<Integer>>>> occurrences = new HashMap<>();
		for(int p=0; p<patterns.size(); p++) {
			List<Integer> pattern = patterns.get(p).getFirst();
			PatternWithSupport patternObj = new PatternWithSupport(pattern, 1);
			Projection projectionP = projectionPattern(patternObj);
			for(Map<Integer,Occurrence> projection: Arrays.asList(projectionP.projectionInClass,projectionP.projectionOtherwise)) {
				for(Entry<Integer,Occurrence> entry: projection.entrySet()) {
					int sliding_window_idx = entry.getKey();
					Occurrence occurrenceInWindow = entry.getValue();
					Pair<Integer,Integer> rangeInOriginal = ranges.get(sliding_window_idx);
					int series_ts_idx = this.ts.originalTimeseriesIdx[sliding_window_idx];
					//save occurrence in original time series
					List<Integer> positions = new ArrayList<>();
					int patternIdx = 0;
					for(int i=occurrenceInWindow.start; i<occurrenceInWindow.end && patternIdx < pattern.size(); i++) {
						int value = this.ts.features[sliding_window_idx][i];
						if(value == pattern.get(patternIdx)) {
							positions.add(i);
							patternIdx++;
						}
					}
					//in contintinous space offset is i + rangeInOriginal.getFirst()
					//in paa space, position is divided by len(continuous_window)/len(discrete_window) (where len(discrete) = paa_win)
					for(int i=0; i<positions.size(); i++) {
						positions.set(i, positions.get(i) + rangeInOriginal.getFirst()/divider);
					}
					//save result in datastructure
					if(occurrences.get(pattern) == null) {
						occurrences.put(pattern, new HashMap<>());
					}
					if(occurrences.get(pattern).get(series_ts_idx) == null) {
						occurrences.get(pattern).put(series_ts_idx, new ArrayList<>());
					}
					occurrences.get(pattern).get(series_ts_idx).add(positions);
				}
			}
		}
		//save to file
		for(Entry<List<Integer>, Map<Integer,List<List<Integer>>>> entry: occurrences.entrySet()) {
			List<Integer> pattern = entry.getKey();
			for(Entry<Integer,List<List<Integer>>> entry2: entry.getValue().entrySet()) {
				Integer originalTimeseriesIdx = entry2.getKey();
				writer.write(Utils.lst2str(pattern));
				writer.write(";");
				writer.write(String.valueOf(originalTimeseriesIdx));
				writer.write(";");
				for(List<Integer> positions: entry2.getValue()) {
					writer.write(Utils.lst2str(positions));
					writer.write(";");
				}
				writer.write("\n");
			}
		}
		writer.close();
	}

	//"Feels" like this can be faster, still now it is reasonable
	public Projection projectionPattern(PatternWithSupport pattern) {
		Projection projectionCurrent = computeProjectionSingleton(pattern.pattern.get(0));
		List<Integer> patternCurrent = new ArrayList<>();
		patternCurrent.add(pattern.pattern.get(0));
		for(int i=1; i<pattern.pattern.size(); i++) {
			Projection next = computeProjectionIncremental(projectionCurrent, patternCurrent, pattern.pattern.get(i));
			projectionCurrent = next;
			patternCurrent.add(pattern.pattern.get(i));
		}
		return projectionCurrent;
	}

	public static void main(String[] args) throws IOException, SAXException {
		System.out.println(">>" + Arrays.toString(args));
		Map<String,String> paramTypes = new HashMap<String,String>();
		paramTypes.put("input", "file");  //input
		paramTypes.put("input_test", "file");  
		paramTypes.put("output", "file"); //output embedding
		paramTypes.put("output_test", "file"); 
		paramTypes.put("output_patterns", "file"); //output patterns
		paramTypes.put("output_discretisation_no_sliding_window", "file");  //for visualisation of patterns
		paramTypes.put("output_pattern_occurrences_no_sliding_window", "file"); //for visualisation of patterns
		paramTypes.put("bins", "int"); //preprocessing
		paramTypes.put("paa_win", "int"); 
		paramTypes.put("window", "int");
		paramTypes.put("max_size", "int");//mining
		paramTypes.put("min_size", "int");
		paramTypes.put("duration", "float");
		paramTypes.put("k", "int");
		int minsup = 3; //FIXED
		if(args.length < 4) {
			System.err.println("Expected: >>be.uantwerpen.mining.MineTopKDiscriminativeSequentialPatterns -input ./data/mytimeseries.ts -bins 10 -paa_win 100"
					+ " -min_size 2 -max_size 10 -duration 1.1 -k 1000 -output_test embedding.txt");
		}
		Map<String,Object> values = Utils.getParameters(args, paramTypes);
		if((Integer)values.get("min_size") > (Integer)values.get("paa_win")) {
			throw new RuntimeException("Min size can not be smaller than paa_win");
		}
		//pre-processing
		PETS pats = new PETS();
		SlidingWindow sw = new SlidingWindow();
		DiscretiseNew discretise = new DiscretiseNew();
		TimeSeriesDataset ts = pats.readTS(new File((String)values.get("input")));
		TimeSeriesDataset ts_windowed = sw.transform(ts, (Integer)values.get("window"), 1);
		discretise.zNormaliseInPlace(ts_windowed, 0.01);
		discretise.piecewiseAggregateInPlace(ts_windowed, (Integer)values.get("paa_win"));
		discretise.paa2symbols(ts_windowed, (Integer)values.get("bins"));
		DiscreteTimeSeriesDataset ts_d = ts_windowed.discrete();
		System.out.println("Discretised: shape:" + ts_d.features.length + "," + ts_d.features[0].length);
		System.out.println("After pre-processing: " + Arrays.toString(ts_d.features[0]));

		//mine discriminative sequential patterns
		MineTopKDiscriminativeSequentialPatternsNew miner = new MineTopKDiscriminativeSequentialPatternsNew();
		System.out.println(values.entrySet());
		int max_iter_without_change = (Integer)values.get("k");
		List<Pair<List<Integer>,Float>> patterns = miner.mineAllLabels(ts_d,  (Integer)values.get("k"), max_iter_without_change,
						(Integer)values.get("min_size"),(Integer)values.get("max_size"), (Float)values.get("duration"), minsup);
		System.out.println("Found " + patterns.size() + " patterns.");
		for(Pair<List<Integer>,Float> p: patterns.subList(0, Math.min(5, patterns.size()))) {
			System.out.println(p);
		}
		System.out.println("...");
		for(Pair<List<Integer>,Float> p: patterns.subList(Math.max(0, patterns.size()-5), patterns.size())) {
			System.out.println(p);
		}
		if(patterns.size() == 0) {
			return;
		}
		if(values.get("output_patterns") != null) {
			PatternWithSupport.save2(new File((String)values.get("output_patterns")), patterns);
		}
		//save occurrences for visualisation of patterns
		if(values.get("output_pattern_occurrences_no_sliding_window") != null || values.get("output_discretisation_no_sliding_window") != null) {
			//preprocessing: no sliding window
			TimeSeriesDataset ts2 = ts.copy();
			discretise.zNormaliseInPlace(ts2, 0.01); //obviously different
			int paa_single = (Integer)values.get("paa_win") * ts2.features[0].length / (Integer)values.get("window");
			//e.g. if sequence length is 1000 and window is 100 and paa is 10 > normally 10 non-overlapping sliding windows (and 1000-100 sliding windows) 
			//downsampled to 10 each, given a total length of 100 after downsampling
			discretise.piecewiseAggregateInPlace(ts2, paa_single);
			discretise.paa2symbols(ts2, (Integer)values.get("bins"));
			DiscreteTimeSeriesDataset ts2_d = ts2.discrete();
			System.out.println("Saving discretisation without sliding windows"); 
			System.out.println("Discretised: shape:" + ts2_d.features.length + "," + ts2_d.features[0].length);
			System.out.println("After pre-processing: " + Arrays.toString(ts2_d.features[0]));
			if(values.get("output_discretisation_no_sliding_window") != null)
				ts2_d.save(new File((String)values.get("output_discretisation_no_sliding_window")), true, false); //save labels
			//save pattern occurrences
			if(values.get("output_pattern_occurrences_no_sliding_window") != null) {
				miner.createAndSavePatternOccurrences(ts, (Integer)values.get("window"), (Integer)values.get("paa_win"),
						patterns,  new File((String)values.get("output_pattern_occurrences_no_sliding_window")));
			}
		}
		//save embedding
		DiscreteTimeSeriesDataset ts_embedding = miner.createEmbeddingWithWindows(ts_d, patterns);
		System.out.println("Embedding: shape: " + ts_embedding.features.length + "," + ts_embedding.features[0].length);
		ts_embedding.save(new File((String)values.get("output")), false, false);
		
		if(values.get("input_test") != null) {
			//preprocess input
			TimeSeriesDataset ts_test = pats.readTS(new File((String)values.get("input_test")));
			ts_test = sw.transform(ts_test, (Integer)values.get("window"), 1);
			discretise.zNormaliseInPlace(ts_test, 0.01);
			discretise.piecewiseAggregateInPlace(ts_test, (Integer)values.get("paa_win")); 
			discretise.paa2symbols(ts_test, (Integer)values.get("bins"));
			DiscreteTimeSeriesDataset ts_test_d = ts_test.discrete();

			//save embedding input
			DiscreteTimeSeriesDataset ts_embedding_test = miner.createEmbeddingWithWindows(ts_test_d, patterns);
			System.out.println("Embedding: shape: " + ts_embedding_test.features.length + "," + ts_embedding_test.features[0].length);
			ts_embedding_test.save(new File((String)values.get("output_test")), false, false);
		}
	}
	
}
