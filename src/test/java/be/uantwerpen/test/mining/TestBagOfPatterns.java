package be.uantwerpen.test.mining;

import java.io.File;
import java.io.IOException;

import org.junit.Test;

import be.uantwerpen.mining.BagOfPatterns;
import net.seninp.jmotif.sax.SAXException;

public class TestBagOfPatterns {

	@Test
	public void testMain() throws IOException, SAXException {
		File input = new File("./data/lin_clustering_data26.ts");
		String[] args = ("-input " + input.getAbsolutePath() + " -bins 10 -paa_win 5 -window 10 -output ./temp/embedding.txt").split("\\s");
		BagOfPatterns.main(args);
	}
}
