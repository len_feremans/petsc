package be.uantwerpen.test.mining;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.junit.Test;

import be.uantwerpen.DiscreteTimeSeriesDataset;
import be.uantwerpen.DiscretiseNew;
import be.uantwerpen.PETS;
import be.uantwerpen.PatternWithSupport;
import be.uantwerpen.TimeSeriesDataset;
import be.uantwerpen.mining.MineTopKSequentialPatterns;
import be.uantwerpen.mining.SlidingWindow;
import be.uantwerpen.util.Timer;
import net.seninp.jmotif.sax.SAXException;

public class TestMineTopKSequentialPatterns {

	@Test
	public void testMineSimple() {
		MineTopKSequentialPatterns miner = new MineTopKSequentialPatterns();
		MineTopKSequentialPatterns.TRACE = true;
		TimeSeriesDataset ts = new TimeSeriesDataset();
		ts.features = new double[1][];
		ts.features[0] = new double[] {1,1,2,3,-1,-1,-1,-1,-1,1,2,3};
		ts.labels = new int[]{1};
		SlidingWindow sw = new SlidingWindow();
		ts = sw.transform(ts, 5,1);
		DiscreteTimeSeriesDataset ts_d = ts.discrete();
		printTimeSeries(ts_d);
		int k=3,min_size=2,max_size=5;
		double duration=1.0;
		List<PatternWithSupport> patterns = miner.mine(ts_d, k, min_size, max_size, duration);
		System.out.println("Found " + patterns.size() + " patterns");
		for(PatternWithSupport pattern: patterns) {
			System.out.println(pattern);
		}
	}
	
	@Test
	public void testMineSimpleEmbedding() {
		MineTopKSequentialPatterns miner = new MineTopKSequentialPatterns();
		MineTopKSequentialPatterns.TRACE = true;
		TimeSeriesDataset ts = new TimeSeriesDataset();
		ts.features = new double[2][];
		ts.features[0] = new double[] {1,1,2,3,-1,-1,-1,-1,-1,1,2,3};
		ts.features[1] = new double[] {1,2,3,-1,-1,-1,1,3,2,-1,-1,-1, 1,2,2,3};
		ts.labels = new int[]{1,1};
		SlidingWindow sw = new SlidingWindow();
		ts = sw.transform(ts, 5,1);
		DiscreteTimeSeriesDataset ts_d = ts.discrete();
		printTimeSeries(ts_d);
		int k=5,min_size=2,max_size=5;
		double duration=1.0;
		List<PatternWithSupport> patterns = miner.mine(ts_d, k, min_size, max_size, duration);
		System.out.println("Found " + patterns.size() + " patterns");
		for(PatternWithSupport pattern: patterns) {
			System.out.println(pattern);
		}
		DiscreteTimeSeriesDataset embedding = miner.createEmbeddingWithWindows(ts_d, patterns);
		printTimeSeries(embedding);
	}

	@Test
	public void testPerformance() throws IOException, SAXException {
		//e.g. try Best parameters: {'train': '../data/Univariate_ts/Adiac/Adiac_TRAIN.ts',
		//'test': '../data/Univariate_ts/Adiac/Adiac_TEST.ts', 
		//'verbose': False, 'paa_win': 28, 'alphabet': 9, 'window': 90, 'gap': 0, 'support': 15, 'min_size': 12, 'max_size': 16} error: 0.317 + 6%
		//pre-process
		PETS pets = new PETS();
		SlidingWindow sw = new SlidingWindow();
		DiscretiseNew ds = new DiscretiseNew();
		TimeSeriesDataset ts = pets.readTS(new File("./data/Univariate_ts/Adiac/Adiac_TRAIN.ts"));
		ts = sw.transform(ts, 90,1);
		ds.zNormaliseInPlace(ts);
		ds.piecewiseAggregateInPlace(ts, 28);
		ds.paa2symbols(ts, 9);
		DiscreteTimeSeriesDataset ts_d = ts.discrete();
		//mine
		Timer miningT = new Timer("mining");
		MineTopKSequentialPatterns miner = new MineTopKSequentialPatterns();
		List<PatternWithSupport> patterns = miner.mine(ts_d, 1000, 12, 16, 1.0);
		System.out.println("Found " + patterns.size() + " patterns");
		for(int i=0; i<patterns.size() && i<10; i++) {
			System.out.println(patterns.get(i));
		}
		miningT.end();
		//embedding -> 2 second!
		Timer embeddingT = new Timer("embedding");
		DiscreteTimeSeriesDataset embedding = miner.createEmbeddingWithWindows(ts_d, patterns);
		embeddingT.end();
		
		//compare
		/*
		Timer miningT2 = new Timer("mining-gap");
		MineSequentialPatternsWithConstraints cs = new MineSequentialPatternsWithConstraints();
		patterns = cs.mine(ts_d, 12, 16, 0, 15);
		System.out.println("Found " + patterns.size() + " patterns");
		for(int i=0; i<patterns.size() && i<10; i++) {
			System.out.println(patterns.get(i));
		}
		miningT2.end();
		//embedding -> 2 minutes
		Timer embeddingT2 = new Timer("embedding-gap");
		Embedding emb = new Embedding();
		DiscreteTimeSeriesDataset emb2 = emb.embeddingWindows(ts_d, patterns, 0);
		embeddingT2.end();
		*/
	}
	
	@Test
	public void testMain() throws IOException, SAXException {
		MineTopKSequentialPatterns.main(("-input ./data/Univariate_ts/Adiac/Adiac_TRAIN.ts"
				+ " -input_test ./data/Univariate_ts/Adiac/Adiac_TEST.ts"
				+ " -paa_win 10"
				+ " -bins 7"
				+ " -duration 1.0"
				+ " -min_size 6"
				+ " -max_size 10"
				+ " -window 50"
				+ " -k 2193"
				+ " -output ../temp/embedding_train_bosp_fast.txt"
				+ " -output_test ../temp/embedding_test_bosp_fast.txt").split("\\s+"));

	}
	
	private void printTimeSeries(DiscreteTimeSeriesDataset ts_d) {
		for(int i=0; i<ts_d.features.length; i++) {
			for(int j=0; j<ts_d.features[i].length; j++) {
				System.out.print(ts_d.features[i][j]);
				System.out.print(" ");
			}
			System.out.println("");
		}
	}
}
