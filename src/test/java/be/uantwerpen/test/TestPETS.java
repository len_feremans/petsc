package be.uantwerpen.test;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

import org.junit.Test;

import be.uantwerpen.PETS;
import be.uantwerpen.TimeSeriesDataset;
import net.seninp.jmotif.sax.SAXException;

public class TestPETS {
	
	@Test
	public void testReadTS() throws IOException {
		File input = new File("./data/Univariate_ts/ACSF1/ACSF1_TRAIN.ts");
		PETS pats = new PETS();
		TimeSeriesDataset ts = pats.readTS(input);
		System.out.println(Arrays.toString(ts.features[0]));
		System.out.println(ts.labels[0]);
		System.out.println(Arrays.toString(ts.features[ts.features.length-1]));
		System.out.println(ts.labels[ts.labels.length-1]);
	}
	
}