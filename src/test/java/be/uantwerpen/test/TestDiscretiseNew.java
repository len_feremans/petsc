package be.uantwerpen.test;

import org.junit.Test;

import be.uantwerpen.DiscretiseNew;
import be.uantwerpen.TimeSeriesDataset;
import be.uantwerpen.util.Utils;
import net.seninp.jmotif.sax.SAXException;

public class TestDiscretiseNew {


	@Test
	public void testPiecewiseAggregateInPlace() throws SAXException {
		TimeSeriesDataset ts = new TimeSeriesDataset();
		ts.features = new double[2][];
		ts.features[0] = new double[] {1,2,3,1,1,1,2,2,2};
		ts.features[1] = new double[] {1,2,3,1,1,1,2,2,2,1,1};
		DiscretiseNew pats = new DiscretiseNew();
		pats.piecewiseAggregateInPlace(ts, 3);
		System.out.println(Utils.arrayToStringNice(ts.features[0]));
		System.out.println(Utils.arrayToStringNice(ts.features[1]));
		//second example
		ts = new TimeSeriesDataset();
		ts.features = new double[1][];
		ts.features[0] = new double[] {2.02f, 2.33f, 2.99f, 6.85f, 9.20f, 
									   8.80f, 7.50f, 6.00f, 5.85f, 3.85f, 
									   4.85f, 3.85f, 2.22f, 1.45f, 1.34f};
		pats.piecewiseAggregateInPlace(ts, 3);
		System.out.println(Utils.arrayToStringNice(ts.features[0]));
	}
	
	//See https://jmotif.github.io/sax-vsm_site/morea/algorithm/znorm.html
	@Test
	public void testNormaliseInPlace() {
		TimeSeriesDataset ts = new TimeSeriesDataset();
		ts.features = new double[3][];
		ts.features[0] = new double[] {1,2,3,1,1,1,2,2,2};
		ts.features[1] = new double[] {1,2,3,1,1,1,2,2,2,1,1};
		ts.features[2] = new double[] {2.02f, 2.33f, 2.99f, 6.85f, 9.20f, 8.80f, 7.50f, 6.00f, 5.85f, 3.85f, 4.85f, 3.85f, 2.22f, 1.45f, 1.34f};		
		DiscretiseNew pats = new DiscretiseNew();
		pats.zNormaliseInPlace(ts,0.01);
		System.out.println(Utils.arrayToStringNice(ts.features[0]));
		System.out.println(Utils.arrayToStringNice(ts.features[1]));
		System.out.println(Utils.arrayToStringNice(ts.features[2]));	
	}
	
	@Test
	public void testPaa2symbolInPlace() throws SAXException {
		TimeSeriesDataset ts = new TimeSeriesDataset();
		ts.features = new double[1][];
		ts.features[0] = new double[] {1,2,3,1,1,1,2,2,2};	
		DiscretiseNew pats = new DiscretiseNew();
		pats.zNormaliseInPlace(ts);
		pats.paa2symbols(ts, 3);
		System.out.println(Utils.arrayToStringNice(ts.features[0]));		
	}
	
	
	/*
	@Test
	public void testCompareGlobalLocalBinning() throws IOException {
		File input = new File("./data/Univariate_ts/ACSF1/ACSF1_TRAIN.ts");
		PETS pats = new PETS();
		TimeSeriesDataset ts = pats.readTS(input);
		DiscretiseOld discretise = new DiscretiseOld();
		discretise.piecewiseAggregateInPlace(ts, 146);
		TimeSeriesDataset ts1 = ts.copy();
		discretise.equalWidthDiscretisationInPlace(ts1, 10, false);
		TimeSeriesDataset ts2 = ts.copy();
		discretise.equalWidthDiscretisationInPlaceGlobalIntervals(ts2, 10, false);
		System.out.println(Utils.arrayToStringNice(ts.features[0]));
		System.out.println(Utils.arrayToStringNice(ts1.features[0]));
		System.out.println(Utils.arrayToStringNice(ts2.features[0]));		
	}
	*/
}
