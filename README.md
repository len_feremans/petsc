## PETSC: Pattern-based Embedding for Time Series Classification

Implementation of _Pattern-based Embedding for Time Series Classification_, 
by Len Feremans. 

Paper submission pending to the Data Mining and Knowledge Discovery 2021.
> Efficient and interpretable classification of time series is an essential data mining task with many real-world applications. Recently several dictionary- and shapelet-based time series classification methods have been proposed that employ contiguous subsequences of fixed length. Despite the simplicity of only considering contiguous fixed-length candidate patterns, they have been shown to be successful at time series classification. We extend pattern mining to efficiently enumerate long variable-length sequential patterns subject to temporal constraints. Additionally, we propose an ensemble that discovers patterns at  multiple resolutions and considers cohesive sequential patterns that vary both in length, duration and resolution. For time series classification we construct an embedding based on sequential pattern occurrences and learn a linear model. The discovered patterns form the basis for interpretable insight into each class of time series. The pattern-based embedding for time series classification (PETSC) supports both univariate and multivariate time series datasets of varying length subject to noise or missing data.  We experimentally validate that MR-PETSC performs significantly better than baseline interpretable methods such as DTW, BOP and SAX-VSM on univariate and multivariate time series. It performs worse than recent non-interpretable state-of-the-art methods such as HIVE-COTE, ROCKET, TS-CHIEF and InceptionTime on univariate time series, however the average rank of MR-PETSC is not significantly worse than many recent methods, including BOSS, cBOSS, S-BOSS, ProximityForest and ResNET. Moreover, on multivariate datasets PETSC does not perform significantly worse than the current state-of-the-art such as HIVE-COTE, ROCKET, CIF and ResNET. PETSC scales to large datasets and the total time for training and making predictions on all 85 ‘bake off’ datasets in the UCR archive is under 3 hours making it one of the fastest methods available. PETSC is particularly useful as it learns a linear model where each feature represents a sequential pattern in the time domain, which supports straightforward oversight to ensure predictions are trustworthy and fair which is important in financial, medical or bioinformatics applications.

[Preprint paper](https://www.researchgate.net/publication/344433516_PETSC_Pattern-Based_Embedding_for_Time_Series_Classification)

### Summary

**PETSC** takes a *univariate or multivariate time series* as input. **PETSC** than learns a linear model based on cohesive sequential patterns occurrences in order to predict the class for unlabeled time series instances.

**PETSC** consist of 4 major steps:

1. Preprocessing univariate and multivariate using Symbolic Aggregate Approximiation
2. Mining a (non-redundant) set of *sequential patterns* from each time series, either based on frequent sequential patterns with a constraint on relative duration and minimum length or by mining discriminative patterns directly with similar constraints. 
3. Constructing an *embedding* of all time series based on *occurrences*.
4. Classify time series using a *linear model* trained using an elastic net.
		
### Installation 

1. Clone the repository
2. Code is implemented in `Java` for pattern and creating the embedding. Linear model is trained using `Python`
3. For building Java code use `maven` which saves `petsc-0.9.0-jar-with-dependencies.jar` referenced by `Python`
 
```
cd petsc
mvn clean install assembly:single -DskipTests=True
```

### Usage
**PETSC** consists of a baselearner and patterm mining is references from the main code and API in `python`, in the module `common`, see `python/common.py`

- **petsc_soft** which considers near-matching occurrences in time series 
- **petsc_disc** which mines discriminative patterns directly
- **MR-PETSC** which runs PETSC and creates an enemble based on different resolutions 

Parameters for `petsc` are:

- `train_filename`: File in .ts format see [sktime-documentation](https://www.sktime.org/en/latest/examples/loading_data.html)
- `test_filename`: File used for classification
- `window`: control the creation of _fixed sized sliding windows_ in continous time series
- `w`: word size in SAX
- `alphabet`: number of bins using SAX
- `k`: number of pattern to mine, default of 1000 is good
- `min_len`: minimum length of pattern (maximum length of pattern is `w`), default of 3-6 is good
- `rdur`: constraint on relative duration, default of 1.0 is good (no gaps allowed) or 1.2 (for a pattern of size 5, 5*1.2=6 - 5=1 gaps are allowed)

Parameters for `petsc_soft` are same as `petsc`, except:

- `tau` : controls near-matching occurrences, default of 1,2,3 is good
 - `rdur`: is not a parameter and fixed to 1.0 (no gaps)
 
Parameters for `petsc_disc` are same as `petsc`

Parameters for `mr_petsc_params` are same as `petsc`, except:

- `soft`: if True, petsc_soft is used as base-learner (not in paper)
- `stride`: stride for window, default is 1, but can be set higher to speed-up results
- `window`: is not a parameter (i.e. petsc is run by halving the sliding window from |S| to |S|/2, |S|/4, etc.)  

Parameter free version exists, i.e. `run_petsc_classic_with_parameter_optimisation` (parameters train_filename, test_filename, iter=100)
performs by default 100 iteration using **random search** to identify optimal parameters on a validation set and then runs `petsc` using these discovered parameters.
Similarly there is `run_petsc_soft_with_parameter_optimisation`, `run_petsc_disc_with_parameter_optimisation` and `run_mr_petsc_parameter_optimisation`.

For multivariate time series use `mr_petsc_multivariate`.

For visualising attribution, i.e. which part of time series are important for class A or B, run  `run_mr_petsc_and_show_attribution` in `python/experiment_7_show_patterns2.py`


We illustrate both classes in the following example that runs both ROCKET and MR-PETSC

```python
from sktime.utils.data_io import load_from_tsfile_to_dataframe
from sktime.transformers.series_as_features.rocket import Rocket
from sklearn.linear_model import RidgeClassifierCV

import timeit, os
import common
from common import *

dir = '../data/Univariate_ts/'
for dataset in ['SwedishLeaf', 'GunPoint', 'Trace', 'ElectricDevices', 'Strawberry', 'DiatomSizeReduction', 'OSULeaf', 
                'SonyAIBORobotSurface2', 'UWaveGestureLibraryZ', 'OliveOil', 'MiddlePhalanxOutlineAgeGroup', 'MedicalImages']
        print('Dataset: {}'.format(dataset))
        #MR-PETSC DEFAULT
        start = timeit.default_timer()
        train_filename = os.path.join(dir, dataset) + '/' + dataset + '_TRAIN.ts' 
        test_filename = os.path.join(dir, dataset) + '/' + dataset + '_TEST.ts' 
        s = mr_petsc(train_filename, test_filename)
        stop = timeit.default_timer()
        print('MR-PETSC-DEFAULT: {}: {} : {}'.format(dataset,stop-start, s))
      
        #ROCKET
        start = timeit.default_timer()
        X_train, y_train = load_from_tsfile_to_dataframe(train_filename)
        X_test, y_test = load_from_tsfile_to_dataframe(test_filename)
        rocket = Rocket() # by default, ROCKET uses 10,000 kernels
        rocket.fit(X_train)
        X_train_transform = rocket.transform(X_train)
        classifier = RidgeClassifierCV(alphas = np.logspace(-3, 3, 10), normalize = True)
        classifier.fit(X_train_transform, y_train)
        X_test_transform = rocket.transform(X_test)
        s = classifier.score(X_test_transform, y_test)
        stop = timeit.default_timer() 
        print('ROCKET: {}: {} : {}'.format(dataset,stop-start, 1.0 - s))
```

### Results ###
MR-PETSC compares great to STOTA on multivariate datesets. It wins on some univariate datasets, but overall performs worse than STOTA.
Detailed results are found here: [Results.html](./Results.html)


### More information for researchers and contributors ###
The current version is 0.9, and was last update on march 2021. The main implementation is written in `Java` for pattern mining.
We depend on the `Java`-based [jmotif-sax-1.1.5](https://github.com/jMotif/SAX) library for creating the SAX representation. 
Python dependencies are `scikit-learn==0.20.3`, `sktime==0.5.3`, `numpy==1.16.3`, `pandas==0.24.2` and `scipy==1.2.1`.
 
We compare performance with the following state-of-the-art methods:

- `ROCKET`: [ROCKET: exceptionally fast and accurate time series classification using random convolutional kernels](https://link.springer.com/article/10.1007/s10618-020-00701-z). 
- `BOP`: [Rotation-invariant similarity in time series using bag-of-patterns representation](https://link.springer.com/content/pdf/10.1007/s10844-012-0196-5.pdf).
- `SAX-VSM`: [Senin, Pavel, and Sergey Malinchik. "Sax-vsm: Interpretable time series classification using sax and vector space model."](https://ieeexplore.ieee.org/iel7/6724379/6729471/06729617.pdf).

Some datasets are provided in _/data_. All datasets used are available from [timeseriesclassification.com/](https://timeseriesclassification.com/)

To reproduce experiments that compare **MR-PETSC** with state-of-the-art methods run `run_experiments.sh`.
 
### Contributors

- Len Feremans, Adrem Data Lab research group, University of Antwerp, Belgium.

### Licence ###
Copyright (c) [2021] [Len Feremans]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFWARE.