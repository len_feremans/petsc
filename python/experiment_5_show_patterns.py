#1) plot 1:
#   - show time series
#4) Plot 2:
#   - next to each time series: show pattern occurrences
from sktime.utils.data_io import load_from_tsfile_to_dataframe
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import collections
from saxpy.znorm import znorm
from saxpy.paa import paa
from saxpy.sax import ts_to_string
from saxpy.alphabet import cuts_for_asize
import subprocess
from collections import Counter, defaultdict
from utils import *
# --------------------------- load --------------------
train = '../data/lin_clustering_data26.ts'
#train = '../data/Univariate_ts/ACSF1/ACSF1_TRAIN.ts'
#train = '../temp/univariate_selection.ts'
train_x, train_y = load_from_tsfile_to_dataframe(train) 


# --------------------------- pre-process: show two sample of each class  --------------------
ts_filtered = []
ts_labels = []
nr_series = train_y.shape[0]
print("#series:" + str(nr_series))
print("lengths:")
for i in range(0,nr_series):
    label = int(train_y[i])
    ts = train_x['dim_0'].iloc[i].to_numpy()
    ts_filtered.append(ts)
    ts_labels.append(label)
    print("series " + str(i) + " len=" + str(ts.shape[0]) + " first 10=" + str(ts[0:10]))

# --------------------------- run pattern mining no gaps  --------------------
#params:
paa_win = 15
alphabet = 4
duration_constraint = 1.1
min_size = 5
max_size = 15
window = 150 #length is 1000, so 12,5 non-overlapping sliding windows each with 15 elements (len=125 without windowing)
k = 500
#output
embedding_fname1 = '../temp/lin_embedding_bodsp_fast.txt'
patterns_fname = '../temp/lin_bodsp_patterns.txt'
#output specific for visualisation
preprocessing = '../temp/lin_discretisation_no_sw.txt'
occurrences_fname = '../temp/lin_bodsp_pattern_occurrences.txt'
#call
CLASSPATH = '../target/pets-0.1.0-jar-with-dependencies.jar:../lib/jmotif-sax-1.1.5-SNAPSHOT-jar-with-dependencies.jar'
subprocess.call(['java', '-cp', CLASSPATH, 'be.uantwerpen.mining.MineTopKDiscriminativeSequentialPatterns', 
                 '-input', train,
                '-paa_win', str(paa_win), '-bins', str(alphabet), 
                '-duration', str(duration_constraint), 
                '-min_size', str(min_size), '-max_size', str(max_size), 
                '-k', str(k), 
                '-window', str(window),
                '-redundancy_cover', 'False',
                '-output', embedding_fname1,
                '-output_patterns', patterns_fname, 
                '-output_discretisation_no_sliding_window', preprocessing,
                '-output_pattern_occurrences_no_sliding_window', occurrences_fname
               ])


# --------------------------- parse patterns and occurrences  --------------------
#ts_discrete = parse_int_csv_var_length(preprocessing)
ts_discrete, ts_labels_again = parse_discrete_file_with_labels(preprocessing)
embedding_frequent = parse_int_csv_var_length(embedding_fname1)
patterns = parse_patterns(patterns_fname)
occurrences = parse_occurrences(occurrences_fname)


# --------------------------- for each pattern count support for time series in each label --------------------
#compute support in and out label
#support = total number of occurrences, where if if occurres multiple times in a single sequence this is counted
cnt = defaultdict(lambda: Counter())
for pattern in patterns:
    pattern_occurrences = occurrences[tuple(pattern[0])]
    for ts_idx, ts_occurrences in pattern_occurrences.items():
        label = ts_labels[ts_idx]
        support = len(ts_occurrences)
        cnt[tuple(pattern[0])][label]+=support

# --------------------------- for each label create PDF --------------------

def remove_overlapping_occurrences(occurrences):
    remove = set()
    for idx in range(0,len(occurrences)-1):
        occ1 = occurrences[idx]
        occ2 = occurrences[idx+1]
        if occ2[0] < occ1[-1]:
            remove.add(idx+1)
    occurrences_new = [occurrences[idx] for idx in range(0,len(occurrences)) if idx not in remove]
    return occurrences_new

def overlap(occurrences, pattern1, pattern2, jaccard_t):
    occurrences_p1 = occurrences[tuple(pattern1[0])]
    occurrences_p2 = occurrences[tuple(pattern2[0])]
    #Remark: if sliding window, this could be be handled by taking set intersection of tid's
    #1)cover same examples
    setA = set(occurrences_p1.keys())
    setB = set(occurrences_p2.keys())
    inter = setA.intersection(setB)
    d = float(len(occurrences_p1) + len(occurrences_p2) - len(inter))
    jaccard = len(inter) / d if d != 0 else 0
    if jaccard >= jaccard_t: 
        #2)cover same periods:
        setA = set()
        for ts_idx, ts_occurrences in occurrences_p1.items():
            for occurrence in ts_occurrences:
                for idx,pos in enumerate(occurrence):
                    setA.add(ts_idx * 10000 + pos)
        setB = set()
        for ts_idx, ts_occurrences in occurrences_p2.items():
            for occurrence in ts_occurrences:
                for idx,pos in enumerate(occurrence):
                    setB.add(ts_idx * 10000 + pos)
        inter2 = setA.intersection(setB)
        jaccard2 = len(inter2) / float(len(setA) + len(setB) - len(inter2))
        if jaccard2 >= jaccard_t: 
            return True
        else:
             return False
    else:
        return False
    
def remove_overlapping(occurrences, patterns, jaccard_t):
    overlapping = []
    for idx in range(0,len(patterns)):
        if idx % 100 == 0:
            print("Overlapping " + str(idx) + "/" + str(len(patterns)))
    
        for idx2 in range(idx+1, len(patterns)):
           if overlap(occurrences, patterns[idx],patterns[idx2],jaccard_t):
                print("overlapping " + str(patterns[idx]) + "," + str(patterns[idx2]))
                overlapping.append(idx)
                break
    print("Number of patterns: " + str(len(patterns)))
    overlapping = sorted(list(set(overlapping)),reverse=True)
    for idx in overlapping:
        del patterns[idx]
    print("Number of non-overlapping patterns: " + str(len(patterns)))
    return patterns
# Create the PdfPages object to which we will save the pages:
# The with statement makes sure that the PdfPages object is closed properly at
# the end of the block, even if an Exception occurs.
patterns = remove_overlapping(occurrences,patterns,0.5)  
  
  

topKLabels = 8
topKPatterns = 2
labels_sorted = sorted(list(set(ts_labels)))[0:topKLabels]
fig, axes = plt.subplots(nrows=len(labels_sorted), ncols=topKPatterns+1, figsize=(10, 5), gridspec_kw={'width_ratios': [20, 1, 1]})
for label_idx, label in enumerate(labels_sorted):
    #find labels ordered on one-versus-all relative support 
    time_series_with_label = [ts_idx for ts_idx in range(0,len(ts_filtered)) if ts_labels[ts_idx] == label]
    patternsNew = []
    for pattern in patterns:
        countsPattern =  cnt[tuple(pattern[0])]
        cntLabel = countsPattern[label]
        cntOther = 0
        for labeli, support in countsPattern.items():
            if labeli != label:
                cntOther += support
        patternsNew.append((pattern[0], cntLabel, cntOther))
    def contrast(p):
        nr_of_windows = len(ts_filtered[0]) - 120 + 1
        nr_of_windows_with_label = len(time_series_with_label) * nr_of_windows
        nr_of_windows_without_labels= (len(ts_filtered) - len(time_series_with_label))  * nr_of_windows
        rsup1 = p[1] /  nr_of_windows_with_label if nr_of_windows_with_label != 0 else 0
        rsup2 = p[2] / nr_of_windows_without_labels if nr_of_windows_without_labels != 0 else 0
        return rsup1 - rsup2

    patternsNew = sorted(patternsNew, key=lambda p: len(occurrences[tuple(p[0])].get(time_series_with_label[0],[])), reverse=True) #avoid div by zero
    render_patterns = patternsNew[0:topKPatterns]
    styles = ['','o']
    # ----------------------------------------- PLOT 1  ---------------------------------------- 
    idx = time_series_with_label[0]
    label = ts_labels[idx]
    ts_d = ts_discrete[idx]
    #plot first time series with label
    color_label = tableau20[label % len(tableau20)]
    axes[label_idx,0].plot(ts_d, color=color_label, label=str(label),  marker=styles[k % len(styles)], markersize=2)
    #plot alphabet below
    for i,val in enumerate(ts_d):
        axes[label_idx,0].text(i-0.3, 0.5, str(val),fontsize=5,color=(0,0,0),alpha=0.3)
        #axes[label_idx,0].set_ylabel('TP '+ str(idx+1) + "(" + str(label) + ")", fontsize=7, rotation='horizontal', horizontalalignment='right')
        axes[label_idx,0].set_ylim(bottom=0, top=alphabet)
        setup_axes(axes[label_idx,0])
    #plot top-k patterns
    for pattern_idx, pattern in enumerate(render_patterns):  
        pattern_p = pattern[0]
        pattern_freq_in_class = pattern[1]
        pattern_outside_class= pattern[2]
        occurrences_pattern = occurrences[tuple(pattern_p)]
        occurrences_pattern_in_current_ts = occurrences_pattern.get(idx,[])
        occurrences_pattern_in_current_ts = remove_overlapping_occurrences(occurrences_pattern_in_current_ts)
        ts_d_highlight = []
        #show colored highlight, e.g Nan if no match, else value of time series
        ts_d_highlight_black = []
        for i in range(0,len(ts_d)+1):
            ts_d_highlight_black.append(float('nan'))
        for occurrence in occurrences_pattern_in_current_ts:
            for pos in occurrence:
                if pos < len(ts_d): #todo: check later... boundary condition (e.g. pos can be == len(ts_d)
                    ts_d_highlight_black[pos] = ts_d[pos]
        def brighten(color, f):
            return (min(1.0,color[0]*f), min(1.0,color[1]*f),min(1.0,color[2]*f))
        color_pattern = brighten(color_label,0.7)
        if pattern_idx == 1:
             color_pattern = brighten(color_label,0.5)
        axes[label_idx,0].plot(ts_d_highlight_black, color = color_pattern, label=str(pattern_idx), marker=styles[k % len(styles)], markersize=3, alpha=1.0, lw=2)
        for occurrence in occurrences_pattern_in_current_ts:
            #for idx,pos in enumerate(occurrence):
            #    pattern_item = str(pattern_p[idx])
            #    axes[label_idx,0].text(pos-0.3, -0.25 * pattern_idx, pattern_item,fontsize=5,color=color_pattern,alpha=0.8)
            pattern_global_idx = [apattern_idx for apattern_idx, apattern in enumerate(patterns) if apattern[0] == pattern_p][0]
            axes[label_idx,0].text(occurrence[0]-0.3, -0.4 * pattern_idx, 'P' + str(pattern_global_idx),fontsize=5,color=color_pattern,alpha=0.8)
    #plot other columns
    for pattern_idx, pattern in enumerate(render_patterns):  
        pattern_p = pattern[0]
        pattern_global_idx = [apattern_idx for apattern_idx, apattern in enumerate(patterns) if apattern[0] == pattern_p][0]
        color_pattern = brighten(color_label,0.7)
        if pattern_idx == 1:
             color_pattern = brighten(color_label,0.5)
        axes[label_idx,pattern_idx+1].plot(pattern_p, color = color_pattern, label='P' + str(pattern_global_idx), marker=styles[k % len(styles)], markersize=2, alpha=1.0, lw=2)
        axes[label_idx,pattern_idx+1].set_xlabel('P' + str(pattern_global_idx), fontsize=5)
        axes[label_idx,pattern_idx+1].set_ylim(bottom=0, top=alphabet)
        axes[label_idx,pattern_idx+1].set_xlim(-1,10)
        for i,val in enumerate(pattern_p):
            axes[label_idx,pattern_idx+1].text(i-0.3, 0.5, str(val),fontsize=5,color=(0,0,0),alpha=0.3)
        #axes[label_idx,0].set_ylabel('TP '+ str(idx+1) + "(" + str(label) + ")", fontsize=7, rotation='horizontal', horizontalalignment='right')
        setup_axes(axes[label_idx,pattern_idx+1])
        axes[label_idx,pattern_idx+1].get_xaxis().set_visible(True)
        axes[label_idx,pattern_idx+1].get_xaxis().set_ticks([])
    #pdf.savefig()  # saves the current figure into a pdf page

plt.tight_layout()
#plt.show()
plt.savefig("../temp/lin-patterns.pdf", dpi=300)
plt.close()