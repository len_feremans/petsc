from parse_utils import plot_differences
#from table 11 appendix, https://link.springer.com/content/pdf/10.1007/s10618-020-00727-3.pdf
recent_methods1 = '''DEFAULT ROCKET (%) IT (%) MUSE (%) CIF (%) HC (%) mrseql (%) ResNet (%) DTWA (%)
AWR 4.00 99.56 99.10 98.87 97.89 97.99 98.98 98.26 98.94
AF 33.3 24.89 22.00 74.00 25.11 29.33 36.89 36.22 22.44
BM 25.0 99.00 100.0 100.0 99.75 100.0 94.83 100.0 99.92
CR 8.33 100.0 99.44 99.77 98.38 99.26 99.21 99.40 100.0
DDG 20.0 46.13 63.47 -1 56.00 47.60 39.27 63.20 56.67
EW 42.0 86.28 -1 -1 90.33 78.17 72.16 45.45 -1
EP 26.8 99.08 98.65 99.64 98.38 100.0 99.93 99.18 97.37
EC 25.1 44.68 27.92 48.64 72.89 80.68 60.18 28.62 29.57
ER 16.7 98.05 92.10 96.89 95.65 94.26 93.19 87.19 92.89
FD 50.0 69.42 77.24 -1 68.89 69.17 -1 62.97 53.13
FM 49.0 55.27 56.13 54.77 53.90 53.77 55.53 54.70 54.93
HMD 18.9 44.59 42.39 38.02 52.21 37.79 35.23 35.32 30.72
HW 3.8 56.67 65.74 51.85 35.13 50.41 54.04 59.78 60.55
HB 72.2 71.76 73.20 73.59 76.52 72.18 72.52 63.89 68.07
LIB 6.7 90.61 88.72 90.30 91.67 90.28 86.57 94.11 87.85
LSST 31.5 63.15 33.97 63.62 56.17 53.84 60.28 42.94 56.96
MI 50.0 53.13 51.17 -1 51.80 52.17 53.00 49.77 50.37
NATO 16.7 88.54 96.63 87.13 84.41 82.85 86.43 97.11 81.48
PD 10.4 99.56 99.68 98.68 98.97 97.19 97.14 99.64 99.27
PEMS 11.6 85.63 82.83 -1 99.85 97.98 97.15 81.95 78.73
PS 2.6 28.35 36.74 -1 26.56 32.87 -1 30.86 15.39
RS 28.3 92.79 91.69 89.56 89.30 90.64 88.73 91.23 85.79
SRS1 50.2 86.55 84.69 73.58 85.94 86.02 82.86 76.11 81.34
SRS2 50.0 51.35 52.04 49.52 48.87 51.67 49.61 50.24 52.43
SWJ 33.3 45.56 42.00 34.67 45.11 40.67 42.00 30.89 25.56
UW 12.5 94.43 91.23 90.39 92.42 91.31 91.32 88.35 91.51'''

recent_methods2 = '''STC (%) DTWD (%) TapNet (%) gRSF (%) TSF (%) CBOSS (%) RISE (%) DTWI (%)
AWR 97.51 98.87 97.13 98.21 94.82 97.56 95.73 94.31
AF 31.78 23.56 30.22 27.56 29.78 30.44 24.44 34.67
BM 97.92 95.25 99.17 100.00 99.83 98.75 100.0 72.17
CR 98.94 100.0 97.50 97.41 93.15 97.55 97.78 95.74
DDG 43.47 49.20 58.27 44.47 38.87 43.07 50.80 29.27
EW 74.68 64.58 -1 83.00 76.62 62.80 81.93 44.20
EP 98.74 96.30 96.09 96.01 97.34 99.83 99.86 67.03
EC 82.36 30.15 28.99 34.06 45.42 39.62 49.16 30.68
ER 84.28 92.91 89.46 91.98 89.84 84.48 82.44 91.42
FD 69.76 53.28 52.87 55.36 68.95 52.32 51.17 51.53
FM 53.40 54.17 51.33 54.43 53.17 51.03 52.10 55.50
HMD 34.95 30.32 32.34 32.07 48.51 28.87 28.24 26.67
HW 28.77 61.21 32.95 36.96 36.42 49.09 18.27 34.33
HB 72.15 68.88 73.97 74.89 72.28 72.15 73.22 63.80
LIB 84.46 88.04 83.63 75.56 79.72 85.26 81.67 78.63
LSST 57.82 54.76 46.33 58.15 34.31 43.62 50.58 49.57
MI 50.83 52.10 -1 51.87 53.80 52.37 49.83 49.63
NATO 84.35 82.04 90.30 82.37 77.72 82.48 80.59 76.07
PD 97.70 99.28 93.65 96.12 94.11 95.61 87.47 99.22
PEMS 98.40 77.05 79.21 91.27 96.76 96.57 98.98 80.23
PS 30.62 15.39 -1 22.71 14.52 19.43 26.78 10.18
RS 88.09 85.64 85.81 87.79 88.29 88.90 84.17 81.69
SRS1 84.73 81.81 95.68 79.74 84.73 81.33 73.17 80.63
SRS2 51.63 53.69 53.46 48.69 50.65 50.02 50.28 48.48
SWJ 44.00 22.00 35.11 38.44 33.33 36.89 34.00 35.78
UW 87.03 92.28 88.39 89.59 85.05 86.13 71.11 87.58'''

mapping = {'AWR' :'ArticularyWordRecognition', 
           'AF' : 'AtrialFibrillation',
           'BM' : 'BasicMotions',
           'CR' : 'Cricket',
           'DDG': 'DuckDuckGeese',
           'EW' : 'EigenWorms',
           'EP' : 'Epilepsy',
           'EC' : 'EthanolConcentration',
           'ER' : 'ERing',
           'FD' : 'FaceDetection',
           'FM' : 'FingerMovements',
           'HMD': 'HandMovementDirection',
           'HW' : 'Handwriting',
           'HB' : 'Heartbeat',
           'LIB': 'Libras',
           'LSST': 'LSST',
           'MI':  'MotorImagery',
           'NATO' : 'NATOPS',
           'PD': 'PenDigits',
           'PS': '?PS?',
           'PEMS': 'PEMS-SF',
           'RS': 'RacketSports',
           'SRS1' : 'SelfRegulationSCP1',
           'SRS2' : 'SelfRegulationSCP2',
           'SWJ' : 'StandWalkJump',
           'UW' : 'UWaveGestureLibrary'}

def parse_recent_methods(strng):
    import re
    dct = {}
    lines = strng.split('\n')
    header = re.split('\s+', lines[0].replace('(%)',' ').strip())
    for line in lines[1:]:
        tokens = re.split('\s+', line)
        dataset = mapping.get(tokens[0])
        accuracies = [float(val) for val in tokens[1:]]
        accuracies = [val if val != -1 else 0 for val  in accuracies]
        if len(header) != len(accuracies):
            print("error: {} {} {}".format(dataset, len(header),len(accuracies)))
            continue
        for i in range(0, len(header)):
            if not header[i] in dct:
                dct[header[i]] = {}
            dct[header[i]][dataset] = (100.0 - accuracies[i])/100.0
    return dct
        
dct_recent = parse_recent_methods(recent_methods1)
dct_2 = parse_recent_methods(recent_methods2)
dct_recent.update(dct_2)
print(dct_recent)


           
#not: CharacterTrajectories 0.969
#JapaneseVowels 0.959
#Phoneme 0.151
#PS?
#SpokenArabicDigits 0.96
#UWaveGestureLibrary 0.869'''

results_mr_petch_multivariate = {'ArticularyWordRecognition': 0.00666666666666671, 
                                 'AtrialFibrillation': 0.6666666666666667, 
                                 'BasicMotions': 0.050000000000000044, 
                                 'CharacterTrajectories': 0.0759052924791086, 
                                 'Cricket': 0.01388888888888884, 'DuckDuckGeese': 0.52, 
                                 'ERing': 0.08148148148148149, 
                                 'EigenWorms': 0.2290076335877863, 'Epilepsy': 0.01449275362318836, 
                                 'EthanolConcentration': 0.4790874524714829, 
                                 'FaceDetection': 0.4534619750283768, 
                                 'FingerMovements': 0.5, 'HandMovementDirection': 0.7837837837837838, 
                                 'Handwriting': 0.6494117647058824, 'Heartbeat': 0.3268292682926829, 'InsectWingbeat': 1.0, 'JapaneseVowels': 1.0, 
                                 'LSST': 0.46715328467153283, 'Libras': 0.1444444444444445, 'MotorImagery': 0.51, 'NATOPS': 0.12222222222222223, 'PEMS-SF': 0.138728323699422, 
                                 'PenDigits': 1.0, 'PhonemeSpectra': 0.7697584252907843, 'RacketSports': 0.1907894736842105, 
                                 'SelfRegulationSCP1': 0.2832764505119454, 'SelfRegulationSCP2': 0.4666666666666667, 'SpokenArabicDigits': 1.0, 'StandWalkJump': 0.6666666666666667}

results_mr_petch_multivariate_tuned = {'ArticularyWordRecognition': 0.003, 'AtrialFibrillation': 0.6,
                        'BasicMotions': 0.0, 'CharacterTrajectories': 0.059, 'Cricket': 0.0,
                        'ERing': 0.055, 'Epilepsy':0.0,
                        'EthanolConcentration': 0.4449,
                        'FingerMovements': 0.39,
                        'HandMovementDirection': 0.635,
                        'LSST': 0.4399,
                        'Libras': 0.155,
                        'NATOPS': 0.0833,
                        'RacketSports' :0.09210,
                        'SelfRegulationSCP1': 0.2116,
                        #'SelfRegulationSCP2': 0.5278, #default, stride=10
                        'StandWalkJump': 0.6,#default, stride=10
                        'UWaveGestureLibrary': 0.2}            #default, stride=10            

for key, value in results_mr_petch_multivariate_tuned.items():
    results_mr_petch_multivariate[key] = value
                        
                                       
#plus variant: 
#results_mr_petch_multivariate['DuckDuckGeese'] = 0.4400
#results_mr_petch_multivariate['Cricket'] = 0.0
#results_mr_petch_multivariate['CharacterTrajectories']=0.057103064066852394
#results_mr_petch_multivariate['BasicMotions'] = 0.0
#results_mr_petch_multivariate['AtrialFibrillation'] = 0.6
#results_mr_petch_multivariate['ArticularyWordRecognition']= 0.0567

#if error of 1.0 -> not completed
for key, value in list(results_mr_petch_multivariate.items()):
    if value == 1.0:
        print("Missing in PETSC: {}".format(key))
        del results_mr_petch_multivariate[key]
        
        
        
selection_len = ['ArticularyWordRecognition',
'AtrialFibrillation',
'BasicMotions',
#'CharacterTrajectories', -> No resul in great multivariate bakeoff
'Cricket',
'ERing',
'Epilepsy',
'EthanolConcentration',
'FingerMovements',
'HandMovementDirection',
'LSST',
'Libras',
'NATOPS',
'RacketSports',
'SelfRegulationSCP1',
'SelfRegulationSCP2',
'StandWalkJump',
'UWaveGestureLibrary',
'EigenWorms']

datasets = set(dct_recent['ROCKET'].keys())
datasets.remove('?PS?')
datasets = sorted(list(datasets))
print("No datasets: {}".format(len(datasets)))

#ADD MR-PETSC to datasets
dct_recent['MR-PETSC'] = results_mr_petch_multivariate

#TODO: No PenDigists for PETSC
del dct_recent['DEFAULT']
del dct_recent['TapNet'] #missing values, ranked lower than MR-PETSC
#del dct_recent['MUSE'] #missing values, ranked lower than MR-PETSC
del dct_recent['DTWA'] #missing values, ranked lower than MR-PETSC
dct_recent['InceptionTime'] = dct_recent['IT'] #missing values
dct_recent['HIVE-COTE'] = dct_recent['HC'] #missing values
dct_recent['MrSEQL'] = dct_recent['mrseql']
dct_recent['DTW_I'] = dct_recent['DTWI']
dct_recent['DTW_D'] = dct_recent['DTWD']
dct_recent['cBOSS'] = dct_recent['CBOSS']

#rename:
del dct_recent['CBOSS']
del dct_recent['IT']
del dct_recent['HC']
del dct_recent['mrseql']
del dct_recent['DTWI']
del dct_recent['DTWD']
#Not reported by Ruiz:
del dct_recent['InceptionTime'] 
del dct_recent['MrSEQL'] #Not reported by Ruiz
del dct_recent['MUSE'] #Not reported by Ruiz

datasets2 = set(datasets)
for method, d in dct_recent.items():
    for dataset in datasets:
        if not dataset in d or d[dataset] == 0.0 or d[dataset] == -1:
            print('Missing {} for {}'.format(dataset, method))
            if dataset in datasets2:
                datasets2.remove(dataset)                
print("No datasets: {}".format(len(datasets2)))

datasets2 = sorted(list(datasets2))
print('\t\t\t\t\t', end='')
for method, d in dct_recent.items():
    print(method, end ='\t')
print('')
for dataset in datasets2:
    print(dataset.ljust(32), end='\t')
    for method, d in dct_recent.items():
        print('{:.3f}'.format(d[dataset]), end ='\t')
    print('')
    
    
f = open('results_multivariate_cd.csv','w')
f.write('classifier_name,dataset_name,accuracy\n')
for dataset in datasets2:
    for method, results in dct_recent.items():
        f.write('{},{},{}\n'.format(method,dataset,1.0 - results.get(dataset,1.0)))
f.close()
print('saved results_multivariate_cd.csv')

plot_differences(dct_recent['MR-PETSC'], dct_recent['ROCKET'], 'MR-PETSC', 'ROCKET')
plot_differences(dct_recent['MR-PETSC'], dct_recent['ResNet'], 'MR-PETSC', 'ResNET')
plot_differences(dct_recent['MR-PETSC'], dct_recent['DTW_I'], 'MR-PETSC', 'DTW_I')
plot_differences(dct_recent['MR-PETSC'], dct_recent['DTW_D'], 'MR-PETSC', 'DTW_D')

