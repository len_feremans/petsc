from collections import OrderedDict, defaultdict
import numpy as np
from parse_utils import *

#------------- Parsing results exp1 univariate comparison UCR -----------  
#LEN: ECG200 own report for DTW!     

#from Keogh, 1NN-DTW no warping window
results_UCR_DTW = '''SyntheticControl     0.007
GunPoint     0.093
CBF     0.003
FaceAll     0.192
OSULeaf     0.409
SwedishLeaf    0.208
FiftyWords     0.310
Trace     0.000
TwoPatterns     0.000
Wafer     0.020
FaceFour     0.170
Lightning2     0.131
Lightning7     0.274
ECG    0.230
Adiac    0.396
Yoga    0.164
Fish    0.177
Plane    0.000
Car    0.267
Beef    0.367
Coffee    0.000
OliveOil    0.167
CinC_ECG_torso    0.349
ChlorineConcentration    0.352
DiatomSizeReduction    0.033
ECGFiveDays    0.232
FacesUCR    0.095
Haptics    0.623
InlineSkate    0.616
ItalyPowerDemand    0.05
MALLAT    0.066
MedicalImages    0.263
MoteStrain    0.165
SonyAIBORobotSurfaceII    0.169
SonyAIBORobotSurface    0.275
StarLightCurves    0.093
Symbols    0.050
TwoLeadECG    0.096
Cricket_X    0.246
uWaveGestureLibrary_X    0.273
NonInvasiveFetalECGThorax1    0.209
NonInvasiveFetalECGThorax2    0.135
InsectWingbeatSound    0.645
ECG5000    0.076
ArrowHead    0.297
BeetleFly    0.300
BirdChicken    0.250
Ham    0.533
Herring    0.469
PhalangesOutlinesCorrect    0.272
ProximalPhalanxOutlineAgeGroup    0.195
ProximalPhalanxOutlineCorrect    0.216
ProximalPhalanxTW    0.263
ToeSegmentation1    0.228
ToeSegmentation2    0.162
DistalPhalanxOutlineAgeGroup    0.208
DistalPhalanxOutlineCorrect    0.232
DistalPhalanxTW    0.290
Earthquakes    0.258
MiddlePhalanxOutlineAgeGroup    0.250
MiddlePhalanxOutlineCorrect    0.352
MiddlePhalanxTW    0.416
ShapeletSim    0.350
Wine    0.426
WordSynonyms    0.351
Computers    0.300
ElectricDevices    0.399
FordA    0.438
FordB    0.406
HandOutlines    0.202
LargeKitchenAppliances    0.205
Meat    0.067
Phoneme    0.772
RefrigerationDevices    0.536
ScreenType    0.603
ShapesAll    0.232
SmallKitchenAppliances    0.357
Strawberry    0.060
UWaveGestureLibraryAll    0.108
Worms    0.536
WormsTwoClass    0.337'''


results_UCR_DTW_weighted = '''SyntheticControl     0.017 
GunPoint     0.087 
CBF     0.004 
FaceAll     0.192 
OSULeaf     0.388 
SwedishLeaf     0.154 
FiftyWords     0.242 
Trace     0.010 
TwoPatterns     0.002 
Wafer     0.005 
FaceFour     0.114 
Lightning2     0.131 
Lightning7     0.288 
ECG    0.120 
Adiac    0.391 
Yoga    0.155 
Fish    0.154
Plane    0.000 
Car    0.233 
Beef    0.333 
Coffee    0.000 
OliveOil    0.133 
CinC_ECG_torso    0.07 
ChlorineConcentration    0.35 
DiatomSizeReduction    0.065 
ECGFiveDays    0.203 
FacesUCR    0.088 
Haptics    0.588 
InlineSkate    0.613 
ItalyPowerDemand    0.045 
MALLAT    0.086 
MedicalImages    0.253 
MoteStrain    0.134 
SonyAIBORobotSurfaceII    0.141 
SonyAIBORobotSurface    0.305 
StarLightCurves    0.095 
Symbols    0.062 
TwoLeadECG    0.132 
Cricket_X    0.228 
Cricket_Y    0.256
Cricket_Z    0.246
uWaveGestureLibrary_X    0.227 
uWaveGestureLibrary_Y    0.366
uWaveGestureLibrary_Z    0.342
NonInvasiveFetalECGThorax1    0.185 
NonInvasiveFetalECGThorax2    0.129 
InsectWingbeatSound    0.422 
ECG5000    0.075 
ArrowHead    0.200 
BeetleFly    0.300 
BirdChicken    0.300
Ham    0.400 
Herring    0.469 
PhalangesOutlinesCorrect    0.239 
ProximalPhalanxOutlineAgeGroup    0.215 
ProximalPhalanxOutlineCorrect    0.210 
ProximalPhalanxTW    0.263 
ToeSegmentation1    0.250 
ToeSegmentation2    0.092 
DistalPhalanxOutlineAgeGroup    0.228 
DistalPhalanxOutlineCorrect    0.232 
DistalPhalanxTW    0.272 
Earthquakes    0.258 
MiddlePhalanxOutlineAgeGroup    0.253 
MiddlePhalanxOutlineCorrect    0.318 
MiddlePhalanxTW    0.419 
ShapeletSim    0.300 
Wine    0.389 
WordSynonyms    0.252 
Computers    0.380 
ElectricDevices    0.376 
FordA    0.341 
FordB    0.414 
HandOutlines    0.197 
LargeKitchenAppliances    0.205 
Meat    0.067 
Phoneme    0.773 
RefrigerationDevices    0.560 
ScreenType    0.589 
ShapesAll    0.198 
SmallKitchenAppliances    0.328 
Strawberry    0.062 
UWaveGestureLibraryAll    0.034 
Worms    0.586 
WormsTwoClass    0.414'''


results_URC_BOP = '''Adiac    0.5916
ArrowHead    0.7671
Beef    0.5030
BeetleFly    0.8450
BirdChicken    0.8520
Car    0.8183
CBF    0.9628
ChlorineConcentration    0.6358
CinCECGtorso    0.7161
Coffee    0.9436
Computers    0.6882
CricketX    0.5745
CricketY    0.6124
CricketZ    0.5855
DiatomSizeReduction    0.8535
DistalPhalanxOutlineCorrect    0.7146
DistalPhalanxOutlineAgeGroup    0.7332
DistalPhalanxTW    0.6083
Earthquakes    0.7481
ECG200    0.7857
ECG5000    0.9098
ECGFiveDays    0.9082
ElectricDevices    0.7729
FaceAll    0.9389
FaceFour    0.9475
FacesUCR    0.8884
FiftyWords    0.5502
Fish    0.8913
FordA    0.7942
FordB    0.7609
GunPoint    0.9703
Ham    0.7690
HandOutlines    0.8664
Haptics    0.3800
Herring    0.5547
InlineSkate    0.3883
InsectWingbeatSound    0.4852
ItalyPowerDemand    0.8780
LargeKitchenAppliances    0.6870
Lightning2    0.6972
Lightning7    0.5533
Mallat    0.8522
Meat    0.9617
MedicalImages    0.5095
MiddlePhalanxOutlineCorrect    0.7150
MiddlePhalanxOutlineAgeGroup    0.5723
MiddlePhalanxTW    0.4914
MoteStrain    0.8242
NonInvasiveFetalECGThorax1    0.6807
NonInvasiveFetalECGThorax2    0.7452
OliveOil    0.8467
OSULeaf    0.7000
PhalangesOutlinesCorrect    0.7163
Phoneme    0.1480
Plane    0.9871
ProximalPhalanxOutlineCorrect    0.7691
ProximalPhalanxOutlineAgeGroup    0.7700
ProximalPhalanxTW    0.6902
RefrigerationDevices    0.6488
ScreenType    0.4366
ShapeletSim    0.8323
ShapesAll    0.7549
SmallKitchenAppliances    0.7060
SonyAIBORobotSurface1    0.7644
SonyAIBORobotSurface2    0.8451
StarlightCurves    0.9430
Strawberry    0.9645
SwedishLeaf    0.7837
Symbols    0.9333
SyntheticControl    0.9258
ToeSegmentation1    0.9262
ToeSegmentation2    0.9117
Trace    0.9769
TwoLeadECG    0.9005
TwoPatterns    0.9444
UWaveGestureLibraryX    0.5894
UWaveGestureLibraryY    0.5113
UWaveGestureLibraryZ    0.5675
UWaveGestureLibraryAll    0.8138
Wafer    0.9966
Wine    0.8944
WordSynonyms    0.5228
Worms    0.6173
WormsTwoClass    0.7443
Yoga    0.8619'''  
    
results_URC_SAX_SVM = '''Adiac    0.4574
ArrowHead    0.7789
Beef    0.4960
BeetleFly    0.9085
BirdChicken    0.8860
Car    0.8613
CBF    0.9575
ChlorineConcentration    0.6572
CinCECGtorso    0.7299
Coffee    0.9382
Computers    0.6656
CricketX    0.7155
CricketY    0.6601
CricketZ    0.7188
DiatomSizeReduction    0.8433
DistalPhalanxOutlineCorrect    0.7370
DistalPhalanxOutlineAgeGroup    0.7743
DistalPhalanxTW    0.6301
Earthquakes    0.7376
ECG200    0.8354
ECG5000    0.9145
ECGFiveDays    0.9182
ElectricDevices    0.6984
FaceAll    0.9645
FaceFour    0.9434
FacesUCR    0.9227
FiftyWords    0.4407
Fish    0.9405
FordA    0.8232
FordB    0.7427
GunPoint    0.9591
Ham    0.8025
HandOutlines    0.8778
Haptics    0.4223
Herring    0.5938
InlineSkate    0.4066
InsectWingbeatSound    0.5360
ItalyPowerDemand    0.8169
LargeKitchenAppliances    0.8507
Lightning2    0.7439
Lightning7    0.5955
Mallat    0.8506
Meat    0.9540
MedicalImages    0.4778
MiddlePhalanxOutlineCorrect    0.7072
MiddlePhalanxOutlineAgeGroup    0.6203
MiddlePhalanxTW    0.5393
MoteStrain    0.8125
NonInvasiveFetalECGThorax1    0.5404
NonInvasiveFetalECGThorax2    0.6108
OliveOil    0.8463
OSULeaf    0.8598
PhalangesOutlinesCorrect    0.6834
Phoneme    0.1342
Plane    0.9799
ProximalPhalanxOutlineCorrect    0.7986
ProximalPhalanxOutlineAgeGroup    0.7960
ProximalPhalanxTW    0.6670
RefrigerationDevices    0.6292
ScreenType    0.5238
ShapeletSim    0.8086
ShapesAll    0.6872
SmallKitchenAppliances    0.5824
SonyAIBORobotSurface1    0.7543
SonyAIBORobotSurface2    0.8271
StarlightCurves    0.8412
Strawberry    0.9698
SwedishLeaf    0.7056
Symbols    0.8711
SyntheticControl    0.8691
ToeSegmentation1    0.9279
ToeSegmentation2    0.9208
Trace    0.9924
TwoLeadECG    0.9155
TwoPatterns    0.8890
UWaveGestureLibraryX    0.5334
UWaveGestureLibraryY    0.4384
UWaveGestureLibraryZ    0.4809
UWaveGestureLibraryAll    0.8006
Wafer    0.9964
Wine    0.8781
WordSynonyms    0.4546
Worms    0.5932
WormsTwoClass    0.7210
Yoga    0.8359'''


results_boss_all = '''ACSF1    0.87
Adiac    0.7570332480818415
ArrowHead    0.8285714285714286
Beef    0.8333333333333334
BeetleFly    0.9
BirdChicken    0.95
BME    0.8133333333333334
Car    0.8333333333333334
CBF    0.9977777777777778
Chinatown    0.7667638483965015
ChlorineConcentration    0.6627604166666666
CinCECGTorso    0.8891304347826087
Coffee    1.0
Computers    0.74
CricketX    0.7333333333333333
CricketY    0.7538461538461538
CricketZ    0.7435897435897436
Crop    0.6871428571428572
DiatomSizeReduction    0.9281045751633987
DistalPhalanxOutlineAgeGroup    0.7338129496402878
DistalPhalanxOutlineCorrect    0.7391304347826086
DistalPhalanxTW    0.697841726618705
Earthquakes    0.7482014388489209
ECG200    0.87
ECG5000    0.9413333333333334
ECGFiveDays    1.0
ElectricDevices    0.7074309428089742
EOGHorizontalSignal    0.430939226519337
EOGVerticalSignal    0.4116022099447514
EthanolLevel    0.456
FaceAll    0.7816568047337278
FaceFour    1.0
FacesUCR    0.9570731707317073
FiftyWords    0.7054945054945055
Fish    0.9885714285714285
FordA    0.9113636363636364
FordB    0.817283950617284
FreezerRegularTrain    0.9782456140350877
FreezerSmallTrain    0.8842105263157894
GunPoint    1.0
GunPointAgeSpan    1.0
GunPointMaleVersusFemale    1.0
GunPointOldVersusYoung    1.0
Ham    0.6761904761904762
HandOutlines    0.9162162162162162
Haptics    0.4448051948051948
Herring    0.5625
HouseTwenty    0.957983193277311
InlineSkate    0.5163636363636364
InsectEPGRegularTrain    0.9959839357429718
InsectEPGSmallTrain    0.9959839357429718
InsectWingbeatSound    0.5232323232323233
ItalyPowerDemand    0.9086491739552964
LargeKitchenAppliances    0.7706666666666667
Lightning2    0.8524590163934426
Lightning7    0.684931506849315
Mallat    0.9377398720682303
Meat    0.9
MedicalImages    0.718421052631579
MiddlePhalanxOutlineAgeGroup    0.564935064935065
MiddlePhalanxOutlineCorrect    0.7731958762886598
MiddlePhalanxTW    0.551948051948052
MixedShapesRegularTrain    0.9171134020618557
MixedShapesSmallTrain    0.8536082474226804
MoteStrain    0.8242811501597445
NonInvasiveFetalECGThorax1    0.8391857506361323
NonInvasiveFetalECGThorax2    0.9007633587786259
OliveOil    0.8666666666666667
OSULeaf    0.9586776859504132
PhalangesOutlinesCorrect    0.7680652680652681
Phoneme    0.28164556962025317
PigAirwayPressure    0.9519230769230769
PigArtPressure    0.9855769230769231
PigCVP    0.9615384615384616
Plane    1.0
PowerCons    0.9
ProximalPhalanxOutlineAgeGroup    0.8390243902439024
ProximalPhalanxOutlineCorrect    0.8625429553264605
ProximalPhalanxTW    0.7804878048780488
RefrigerationDevices    0.48533333333333334
Rock    0.84
ScreenType    0.464
SemgHandGenderCh2    0.8833333333333333
SemgHandMovementCh2    0.6177777777777778
SemgHandSubjectCh2    0.8222222222222222
ShapeletSim    1.0
ShapesAll    0.9083333333333333
SmallKitchenAppliances    0.72
SmoothSubspace    0.3933333333333333
SonyAIBORobotSurface1    0.632279534109817
SonyAIBORobotSurface2    0.8593913955928646
StarLightCurves    0.9765662943176299
Strawberry    0.9783783783783784
SwedishLeaf    0.9216
Symbols    0.9668341708542714
SyntheticControl    0.9666666666666667
ToeSegmentation1    0.9254385964912281
ToeSegmentation2    0.9615384615384616
Trace    1.0
TwoLeadECG    0.9806848112379281
TwoPatterns    0.993
UMD    0.9861111111111112
UWaveGestureLibraryAll    0.9388609715242882
UWaveGestureLibraryX    0.759073143495254
UWaveGestureLibraryY    0.6823003908431045
UWaveGestureLibraryZ    0.6943048576214406
Wafer    0.9948085658663206
Wine    0.7592592592592593
WordSynonyms    0.6379310344827587
Worms    0.5584415584415584
WormsTwoClass    0.8311688311688312
Yoga    0.9186666666666666'''



def parse_str(txt, acc=False):
    lines = txt.split("\n")
    results_current = {}
    for line in lines:
        tokens = line.split()
        results_current[tokens[0]] = float(tokens[1])
        if acc:
           results_current[tokens[0]] = 1.0 - float(tokens[1])
    return results_current

#baselines
results_DTW = parse_str(results_UCR_DTW)
results_DTW2 = parse_str(results_UCR_DTW_weighted)
for key in results_DTW.keys():
    results_DTW[key] = min(results_DTW[key],results_DTW2[key])
results_BOP = parse_str(results_URC_BOP, acc=True)
results_SAXVSM = parse_str(results_URC_SAX_SVM,acc=True)
results_BOSS = parse_str(results_boss_all,acc=True)

results_mr_petsc = {'ACSF1': {0: {'mr-petsc': 0.31000000000000005, 'mr-petsc++': 0.15000000000000002}}, 'Adiac': {0: {'mr-petsc': 0.3734015345268542, 'mr-petsc++': 0.3120204603580563}}, 'AllGestureWiimoteX': {0: {'mr-petsc': 1, 'mr-petsc++': 1.0}}, 'AllGestureWiimoteY': {0: {'mr-petsc': 1, 'mr-petsc++': 1.0}}, 'AllGestureWiimoteZ': {0: {'mr-petsc': 1, 'mr-petsc++': 1.0}}, 'ArrowHead': {0: {'mr-petsc': 0.22857142857142854, 'mr-petsc++': 0.16000000000000003}}, 'Beef': {0: {'mr-petsc': 0.23333333333333328, 'mr-petsc++': 0.23333333333333328}}, 'BeetleFly': {0: {'mr-petsc': 0.09999999999999998, 'mr-petsc++': 0.0}}, 'BirdChicken': {0: {'mr-petsc': 0.09999999999999998, 'mr-petsc++': 0.09999999999999998}}, 'BME': {0: {'mr-petsc': 0.08666666666666667, 'mr-petsc++': 0.046666666666666634}}, 'Car': {0: {'mr-petsc': 0.09999999999999998, 'mr-petsc++': 0.08333333333333337}}, 'CBF': {0: {'mr-petsc': 0.010000000000000009, 'mr-petsc++': 0.0033333333333332993}}, 'Chinatown': {0: {'mr-petsc': 0.055393586005830886, 'mr-petsc++': 0.03206997084548102}}, 'ChlorineConcentration': {0: {'mr-petsc': 0.40208333333333335, 'mr-petsc++': 0.3494791666666667}}, 'CinCECGTorso': {0: {'mr-petsc': 0.09855072463768111, 'mr-petsc++': 0.048550724637681175}}, 'Coffee': {0: {'mr-petsc': 0.0357142857142857, 'mr-petsc++': 0.0}}, 'Computers': {0: {'mr-petsc': 0.276, 'mr-petsc++': 0.268}}, 'CricketX': {0: {'mr-petsc': 0.35641025641025637, 'mr-petsc++': 0.3025641025641026}}, 'CricketY': {0: {'mr-petsc': 0.3384615384615385, 'mr-petsc++': 0.2974358974358975}}, 'CricketZ': {0: {'mr-petsc': 0.35128205128205126, 'mr-petsc++': 0.24871794871794872}}, 'Crop': {0: {'mr-petsc': 0.49130952380952386, 'mr-petsc++': 0.40214285714285714}}, 'DiatomSizeReduction': {0: {'mr-petsc': 0.05228758169934644, 'mr-petsc++': 0.022875816993464082}}, 'DistalPhalanxOutlineAgeGroup': {0: {'mr-petsc': 0.3309352517985612, 'mr-petsc++': 0.28057553956834536}}, 'DistalPhalanxOutlineCorrect': {0: {'mr-petsc': 0.30797101449275366, 'mr-petsc++': 0.2536231884057971}}, 'DistalPhalanxTW': {0: {'mr-petsc': 0.4676258992805755, 'mr-petsc++': 0.3453237410071942}}, 'DodgerLoopDay': {0: {'mr-petsc': 1, 'mr-petsc++': 1.0}}, 'DodgerLoopGame': {0: {'mr-petsc': 1, 'mr-petsc++': 1.0}}, 'DodgerLoopWeekend': {0: {'mr-petsc': 1, 'mr-petsc++': 1.0}}, 'Earthquakes': {0: {'mr-petsc': 0.3093525179856115, 'mr-petsc++': 0.2302158273381295}}, 'ECG200': {0: {'mr-petsc': 0.14, 'mr-petsc++': 0.14}}, 'ECG5000': {0: {'mr-petsc': 0.06799999999999995, 'mr-petsc++': 0.06511111111111112}}, 'ECGFiveDays': {0: {'mr-petsc': 0.06387921022067367, 'mr-petsc++': 0.022067363530778206}}, 'ElectricDevices': {0: {'mr-petsc': 0.415380625081053, 'mr-petsc++': 0.2916612631305927}}, 'EOGHorizontalSignal': {0: {'mr-petsc': 0.5883977900552486, 'mr-petsc++': 0.5}}, 'EOGVerticalSignal': {0: {'mr-petsc': 0.5856353591160222, 'mr-petsc++': 0.5497237569060773}}, 'EthanolLevel': {0: {'mr-petsc': 0.44199999999999995, 'mr-petsc++': 0.376}}, 'FaceAll': {0: {'mr-petsc': 0.26094674556213016, 'mr-petsc++': 0.21834319526627222}}, 'FaceFour': {0: {'mr-petsc': 0.0, 'mr-petsc++': 0.0}}, 'FacesUCR': {0: {'mr-petsc': 0.16634146341463418, 'mr-petsc++': 0.14195121951219514}}, 'FiftyWords': {0: {'mr-petsc': 0.41758241758241754, 'mr-petsc++': 0.3538461538461538}}, 'Fish': {0: {'mr-petsc': 0.06857142857142862, 'mr-petsc++': 0.02857142857142858}}, 'FordA': {0: {'mr-petsc': 0.10681818181818181, 'mr-petsc++': 0.08712121212121215}}, 'FordB': {0: {'mr-petsc': 0.2358024691358025, 'mr-petsc++': 0.21851851851851856}}, 'FreezerRegularTrain': {0: {'mr-petsc': 0.01684210526315788, 'mr-petsc++': 0.00736842105263158}}, 'FreezerSmallTrain': {0: {'mr-petsc': 0.023157894736842155, 'mr-petsc++': 0.0035087719298245723}}, 'Fungi': {0: {'mr-petsc': 0.053763440860215006, 'mr-petsc++': 0.005376344086021501}}, 'GestureMidAirD1': {0: {'mr-petsc': 0.41538461538461535, 'mr-petsc++': 0.3846153846153846}}, 'GestureMidAirD2': {0: {'mr-petsc': 0.5153846153846153, 'mr-petsc++': 0.523076923076923}}, 'GestureMidAirD3': {0: {'mr-petsc': 0.7384615384615385, 'mr-petsc++': 0.6692307692307693}}, 'GesturePebbleZ1': {0: {'mr-petsc': 1, 'mr-petsc++': 0.2848837209302325}}, 'GesturePebbleZ2': {0: {'mr-petsc': 0.32278481012658233, 'mr-petsc++': 0.2215189873417721}}, 'GunPoint': {0: {'mr-petsc': 0.040000000000000036, 'mr-petsc++': 0.013333333333333308}}, 'GunPointAgeSpan': {0: {'mr-petsc': 0.015822784810126556, 'mr-petsc++': 0.012658227848101222}}, 'GunPointMaleVersusFemale': {0: {'mr-petsc': 0.01898734177215189, 'mr-petsc++': 0.0031645569620253333}}, 'GunPointOldVersusYoung': {0: {'mr-petsc': 0.02857142857142858, 'mr-petsc++': 0.01904761904761909}}, 'Ham': {0: {'mr-petsc': 0.3142857142857143, 'mr-petsc++': 0.21904761904761905}}, 'HandOutlines': {0: {'mr-petsc': 0.1216216216216216, 'mr-petsc++': 0.072972972972973}}, 'Haptics': {0: {'mr-petsc': 0.5097402597402597, 'mr-petsc++': 0.49350649350649356}}, 'Herring': {0: {'mr-petsc': 0.421875, 'mr-petsc++': 0.296875}}, 'HouseTwenty': {0: {'mr-petsc': 0.0672268907563025, 'mr-petsc++': 0.025210084033613467}}, 'InlineSkate': {0: {'mr-petsc': 0.6709090909090909, 'mr-petsc++': 0.6036363636363636}}, 'InsectEPGRegularTrain': {0: {'mr-petsc': 0.008032128514056214, 'mr-petsc++': 0.0}}, 'InsectEPGSmallTrain': {0: {'mr-petsc': 0.052208835341365445, 'mr-petsc++': 0.06024096385542166}}, 'InsectWingbeatSound': {0: {'mr-petsc': 0.49949494949494955, 'mr-petsc++': 0.44141414141414137}}, 'ItalyPowerDemand': {0: {'mr-petsc': 0.13605442176870752, 'mr-petsc++': 0.09037900874635574}}, 'LargeKitchenAppliances': {0: {'mr-petsc': 0.18133333333333335, 'mr-petsc++': 0.10133333333333339}}, 'Lightning2': {0: {'mr-petsc': 0.2622950819672131, 'mr-petsc++': 0.1311475409836066}}, 'Lightning7': {0: {'mr-petsc': 0.31506849315068497, 'mr-petsc++': 0.1917808219178082}}, 'Mallat': {0: {'mr-petsc': 0.15863539445628994, 'mr-petsc++': 0.06481876332622605}}, 'Meat': {0: {'mr-petsc': 0.19999999999999996, 'mr-petsc++': 0.08333333333333337}}, 'MedicalImages': {0: {'mr-petsc': 0.37631578947368416, 'mr-petsc++': 0.3671052631578947}}, 'MelbournePedestrian': {0: {'mr-petsc': 1, 'mr-petsc++': 1.0}}, 'MiddlePhalanxOutlineAgeGroup': {0: {'mr-petsc': 0.577922077922078, 'mr-petsc++': 0.4415584415584416}}, 'MiddlePhalanxOutlineCorrect': {0: {'mr-petsc': 0.26116838487972505, 'mr-petsc++': 0.23024054982817865}}, 'MiddlePhalanxTW': {0: {'mr-petsc': 0.5064935064935066, 'mr-petsc++': 0.4285714285714286}}, 'MixedShapesRegularTrain': {0: {'mr-petsc': 0.06597938144329896, 'mr-petsc++': 0.05731958762886602}}, 'MixedShapesSmallTrain': {0: {'mr-petsc': 0.0903092783505155, 'mr-petsc++': 0.08659793814432992}}, 'MoteStrain': {0: {'mr-petsc': 0.09984025559105436, 'mr-petsc++': 0.09025559105431313}}, 'NonInvasiveFetalECGThorax1': {0: {'mr-petsc': 0.1592875318066158, 'mr-petsc++': 0.1430025445292621}}, 'NonInvasiveFetalECGThorax2': {0: {'mr-petsc': 0.12773536895674298, 'mr-petsc++': 0.10483460559796443}}, 'OliveOil': {0: {'mr-petsc': 0.033333333333333326, 'mr-petsc++': 0.033333333333333326}}, 'OSULeaf': {0: {'mr-petsc': 0.12809917355371903, 'mr-petsc++': 0.04958677685950408}}, 'PhalangesOutlinesCorrect': {0: {'mr-petsc': 0.28321678321678323, 'mr-petsc++': 0.2680652680652681}}, 'Phoneme': {0: {'mr-petsc': 0.770042194092827, 'mr-petsc++': 0.7552742616033755}}, 'PickupGestureWiimoteZ': {0: {'mr-petsc': 0.6599999999999999, 'mr-petsc++': 0.45999999999999996}}, 'PigAirwayPressure': {0: {'mr-petsc': 0.3076923076923077, 'mr-petsc++': 0.20673076923076927}}, 'PigArtPressure': {0: {'mr-petsc': 0.04807692307692313, 'mr-petsc++': 0.024038461538461564}}, 'PigCVP': {0: {'mr-petsc': 0.07692307692307687, 'mr-petsc++': 0.06730769230769229}}, 'PLAID': {0: {'mr-petsc': 0.472998137802607, 'mr-petsc++': 0.3891992551210428}}, 'Plane': {0: {'mr-petsc': 0.0, 'mr-petsc++': 0.0}}, 'PowerCons': {0: {'mr-petsc': 0.16666666666666663, 'mr-petsc++': 0.0888888888888889}}, 'ProximalPhalanxOutlineAgeGroup': {0: {'mr-petsc': 0.2292682926829268, 'mr-petsc++': 0.1609756097560976}}, 'ProximalPhalanxOutlineCorrect': {0: {'mr-petsc': 0.19587628865979378, 'mr-petsc++': 0.147766323024055}}, 'ProximalPhalanxTW': {0: {'mr-petsc': 0.2780487804878049, 'mr-petsc++': 0.19999999999999996}}, 'RefrigerationDevices': {0: {'mr-petsc': 0.544, 'mr-petsc++': 0.45333333333333337}}, 'Rock': {0: {'mr-petsc': 0.09999999999999998, 'mr-petsc++': 0.040000000000000036}}, 'ScreenType': {0: {'mr-petsc': 0.544, 'mr-petsc++': 0.44266666666666665}}, 'SemgHandGenderCh2': {0: {'mr-petsc': 0.21666666666666667, 'mr-petsc++': 0.15333333333333332}}, 'SemgHandMovementCh2': {0: {'mr-petsc': 0.5866666666666667, 'mr-petsc++': 0.39555555555555555}}, 'SemgHandSubjectCh2': {0: {'mr-petsc': 0.2666666666666667, 'mr-petsc++': 0.16222222222222227}}, 'ShakeGestureWiimoteZ': {0: {'mr-petsc': 1, 'mr-petsc++': 0.45999999999999996}}, 'ShapeletSim': {0: {'mr-petsc': 0.12222222222222223, 'mr-petsc++': 0.0}}, 'ShapesAll': {0: {'mr-petsc': 0.16500000000000004, 'mr-petsc++': 0.17000000000000004}}, 'SmallKitchenAppliances': {0: {'mr-petsc': 0.20533333333333337, 'mr-petsc++': 0.16000000000000003}}, 'SmoothSubspace': {0: {'mr-petsc': 1, 'mr-petsc++': 0.30000000000000004}}, 'SonyAIBORobotSurface1': {0: {'mr-petsc': 0.13477537437603992, 'mr-petsc++': 0.0765391014975042}}, 'SonyAIBORobotSurface2': {0: {'mr-petsc': 0.2025183630640084, 'mr-petsc++': 0.09548793284365165}}, 'StarLightCurves': {0: {'mr-petsc': 0.07503642544924716, 'mr-petsc++': 0.04711024769305483}}, 'Strawberry': {0: {'mr-petsc': 0.05945945945945941, 'mr-petsc++': 0.03513513513513511}}, 'SwedishLeaf': {0: {'mr-petsc': 0.12, 'mr-petsc++': 0.09119999999999995}}, 'Symbols': {0: {'mr-petsc': 0.035175879396984966, 'mr-petsc++': 0.024120603015075348}}, 'SyntheticControl': {0: {'mr-petsc': 0.19999999999999996, 'mr-petsc++': 0.046666666666666634}}, 'ToeSegmentation1': {0: {'mr-petsc': 0.03508771929824561, 'mr-petsc++': 0.03508771929824561}}, 'ToeSegmentation2': {0: {'mr-petsc': 0.1461538461538462, 'mr-petsc++': 0.0692307692307692}}, 'Trace': {0: {'mr-petsc': 0.0, 'mr-petsc++': 0.0}}, 'TwoLeadECG': {0: {'mr-petsc': 0.007901668129938533, 'mr-petsc++': 0.0026338893766462146}}, 'TwoPatterns': {0: {'mr-petsc': 0.06874999999999998, 'mr-petsc++': 0.013249999999999984}}, 'UMD': {0: {'mr-petsc': 0.02083333333333337, 'mr-petsc++': 0.01388888888888884}}, 'UWaveGestureLibraryAll': {0: {'mr-petsc': 0.1342825237297599, 'mr-petsc++': 0.09296482412060303}}, 'UWaveGestureLibraryX': {0: {'mr-petsc': 0.2638190954773869, 'mr-petsc++': 0.21105527638190957}}, 'UWaveGestureLibraryY': {0: {'mr-petsc': 0.36404243439419315, 'mr-petsc++': 0.31797878280290337}}, 'UWaveGestureLibraryZ': {0: {'mr-petsc': 0.30960357342266887, 'mr-petsc++': 0.275823562255723}}, 'Wafer': {0: {'mr-petsc': 0.003082414016872148, 'mr-petsc++': 0.0029201817001947017}}, 'Wine': {0: {'mr-petsc': 0.2962962962962963, 'mr-petsc++': 0.16666666666666663}}, 'WordSynonyms': {0: {'mr-petsc': 0.5141065830721003, 'mr-petsc++': 0.46238244514106586}}, 'Worms': {0: {'mr-petsc': 0.33766233766233766, 'mr-petsc++': 0.2597402597402597}}, 'WormsTwoClass': {0: {'mr-petsc': 0.23376623376623373, 'mr-petsc++': 0.19480519480519476}}, 'Yoga': {0: {'mr-petsc': 0.17566666666666664, 'mr-petsc++': 0.17133333333333334}}}
results_mr_petsc_plus = {}
results_mr_petsc_def = {}
for key, value in results_mr_petsc.items():
    if value[0]['mr-petsc++'] != 1.0:   #some error has occurred 
        results_mr_petsc_plus[key] = value[0]['mr-petsc++']
    if value[0]['mr-petsc'] != 1.0:   #some error has occurred 
       results_mr_petsc_def[key] = value[0]['mr-petsc']
        


def parse_file(str, runtime=False): 
    d = defaultdict(lambda: {})                                
    lines = str.split('\n')
    for line in lines:
        if line.startswith('MR-PETSC-DEFAULT:') or line.startswith('DTW:') or line.startswith('BOSS ensemble:') or line.startswith('MrSEQL:')  or line.startswith('ROCKET:'):
            tokens = line.split(':')
            #print(tokens)
            d[tokens[0]][tokens[1].strip()] = float(tokens[3]) #for runtime tokens[2]
            if not line.startswith('MR-PETSC-DEFAULT:'): #for acc otherwise comment
                d[tokens[0]][tokens[1].strip()] = 1.0 - float(tokens[3])
            if runtime:
                d[tokens[0]][tokens[1].strip()] = float(tokens[2]) #for runtime tokens[2]
    return d

run_exp_6_log_fname = '../../data/results/exp6_final.log'
f = open(run_exp_6_log_fname)
run_exp_6 = f.read()
results_run_6 = parse_file(run_exp_6)
results_mr_petsc_file =  results_run_6['MR-PETSC-DEFAULT']
results_DTW_own = results_run_6['DTW']
results_MrSEQL = results_run_6['MrSEQL']
results_ROCKET = results_run_6['ROCKET']

datasets_bakeoff = ['SwedishLeaf', 'GunPoint', 'Trace', 'ElectricDevices', 'Strawberry', 'DiatomSizeReduction', 'OSULeaf', 
                    'SonyAIBORobotSurface2', 'UWaveGestureLibraryZ', 'OliveOil', 'MiddlePhalanxOutlineAgeGroup', 'MedicalImages', 
                    'ECG5000', 'Haptics', 'BeetleFly', 'UWaveGestureLibraryY', 'ToeSegmentation1', 'Symbols', 'Ham', 'Computers', 
                    'NonInvasiveFetalECGThorax1', 'Yoga', 'Adiac', 'WormsTwoClass', 'MoteStrain', 'Herring', 'MiddlePhalanxTW', 'Beef', 
                    'ChlorineConcentration', 'CricketZ', 'CricketY', 'DistalPhalanxOutlineAgeGroup', 'SmallKitchenAppliances', 
                    'RefrigerationDevices', 'Wine', 'UWaveGestureLibraryAll', 'NonInvasiveFetalECGThorax2', 'FiftyWords', 'FordB', 
                    'Coffee', 'CBF', 'ShapeletSim', 'InsectWingbeatSound', 'Lightning7', 'WordSynonyms', 'ArrowHead', 'ScreenType', 
                    'SonyAIBORobotSurface1', 'Plane', 'TwoPatterns', 'InlineSkate', 'CricketX', 'ECGFiveDays', 'FaceAll', 'ECG200', 
                    'BirdChicken', 'PhalangesOutlinesCorrect', 'ProximalPhalanxTW', 'CinCECGTorso', 'Phoneme', 'SyntheticControl', 
                    'Earthquakes', 'TwoLeadECG', 'Wafer', 'DistalPhalanxOutlineCorrect', 'Fish', 'Meat', 'LargeKitchenAppliances', 
                    'StarLightCurves', 'Mallat', 'ShapesAll', 'DistalPhalanxTW', 'HandOutlines', 'FaceFour', 'ItalyPowerDemand', 
                    'UWaveGestureLibraryX', 'ProximalPhalanxOutlineAgeGroup', 'Lightning2', 'FacesUCR', 'ToeSegmentation2', 'Car', 
                    'FordA', 'ProximalPhalanxOutlineCorrect', 'Worms', 'MiddlePhalanxOutlineCorrect']

#combine own results of DTW with best reported results (some diff here)
for key in results_DTW_own.keys():
    results_DTW_own[key] = min(results_DTW_own[key],results_DTW.get(key,1.0))

#for key in results_mr_petsc_plus.keys():
#    results_mr_petsc_file[key] = min(results_mr_petsc_def.get(key,1.0), results_mr_petsc_file.get(key,1.0), results_mr_petsc_plus[key])
    

#load results WEASEL
import pandas as pd
files = ['BOSS_TESTFOLDS.csv', 'S-BOSS_TESTFOLDS.csv', 'cBOSS_TESTFOLDS.csv',  #'STC_TESTFOLDS.csv', #'WEASEL_TESTFOLDS.csv',
         'ROCKET_TESTFOLDS.csv', 'HIVE-COTEv1_TESTFOLDS.csv', 'InceptionTime_TESTFOLDS.csv', 'ResNet_TESTFOLDS.csv', 'ProximityForest_TESTFOLDS.csv',
         'TS-CHIEF_TESTFOLDS.csv']
results_UCR = {}
for file in files:      
    df = pd.read_csv(file)
    df['mean'] = df.apply(lambda row: np.mean(row[1:]), axis=1)
    result_m = {}
    for index, row in df.iterrows():
        result_m[row['folds:']] = 1.0 - row['mean']
    results_UCR[file.replace('_TESTFOLDS.csv','')] = result_m



print('DTW\t DTW2\t DTW_OWN\t BOP\t SAXVSM\t BOSS\t MRPETSC_DEF\t MRPETSC_DEF2\t MR_PETSC++\t MrSEQL\t ROCKET\t', end='');
for method, results in results_UCR.items():
    print('{}\t '.format(method), end='')
print('')

for dataset in datasets_bakeoff:
    for results in [results_DTW, results_DTW2, results_DTW_own, results_BOP, results_SAXVSM, results_BOSS, 
                    results_mr_petsc_def, results_mr_petsc_file, results_mr_petsc_plus, results_MrSEQL, results_ROCKET]: 
        #print(results)
        #print(results.get(dataset,-1))
        print('{:6.3f}'.format(results.get(dataset,-1)), end='\t');
    for method, results in results_UCR.items():
        print('{:6.3f}'.format(results.get(dataset,-1)), end='\t');
    print('')
      
datasets_subset = set()

#save data for CD
f = open('results_univariate_cd.csv','w')
f.write('classifier_name,dataset_name,accuracy\n')
for dataset in datasets_bakeoff:
    filtered = False
    for method, results in results_UCR.items():
        if not dataset in results:
            filtered = True
    if filtered: 
        continue
    print('Filtering {}'.format(dataset))   
    for results, method in zip([results_DTW_own, results_BOP, results_SAXVSM, #results_BOSS, results_ROCKET
                                results_mr_petsc_plus, results_MrSEQL],
                                ['DTW', 'BOP', 'SAXVSM','MR-PETSC', 'MrSEQL']):
        f.write('{},{},{}\n'.format(method,dataset,1.0 - results.get(dataset,1.0)))
    for method, results in results_UCR.items():
        f.write('{},{},{}\n'.format(method,dataset,1.0 - results.get(dataset,1.0)))
f.close()
print('saved results_univariate_cd.csv')


plot_differences('results_mr_petsc_plus', results_UCR['ROCKET'], 'MR-PETSC', 'ROCKET')

                    


