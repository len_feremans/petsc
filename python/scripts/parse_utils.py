import matplotlib.pyplot as plt
from matplotlib import rc
import numpy as np

#plot differences BOSS / MR-PETSC-PLUS
def plot_differences(dct1, dct2, label1, label2):
    datasets = sorted(list(set(dct1.keys()).intersection(set(dct2.keys()))))
    print("found {} datasets".format(len(datasets)))
    arr_1 = [1.0 - dct1[dataset] for dataset in datasets]
    arr_2 = [1.0 - dct2[dataset] for dataset in datasets]
    wins_label_1 = sum([1 if arr_1[i] > arr_2[i] and abs(arr_1[i]-arr_2[i]) > 0.01 else 0 for i in range(0,len(arr_1))])
    wins_label_2 = sum([1 if arr_2[i] > arr_1[i] and abs(arr_1[i]-arr_2[i]) > 0.01 else 0 for i in range(0,len(arr_1))])
    equal_almost = sum([1 if abs(arr_1[i]-arr_2[i]) <= 0.01 else 0 for i in range(0,len(arr_1))])
    print("wins label 1/label 2: {} {}; equal <=0.01: {}".format(wins_label_1,wins_label_2,equal_almost))
    
    wins_label_1 = sum([1 if arr_1[i] > arr_2[i] else 0 for i in range(0,len(arr_1))])
    wins_label_2 = sum([1 if arr_2[i] > arr_1[i] else 0 for i in range(0,len(arr_1))])
    print("wins (ignoring almost) label 1/label 2: {} {};".format(wins_label_1,wins_label_2))
    
    
    #rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
    #rc('font',**{'family':'serif','serif':['Palatino']})
    plt.figure(figsize=(20,20), frameon=False)
    fig, ax = plt.subplots()
    ax.scatter(arr_1, arr_2, color='r', alpha=0.3)
    mini = max(0, min(np.min(arr_1),np.min(arr_2)) - 0.1)
    mini = float('{:.1f}'.format(mini))
    plt.axis('square')
    plt.xlim(mini, 1)
    plt.ylim(mini, 1)
    plt.xticks(np.arange(mini, 1.01, step=0.1), fontsize=10)
    plt.yticks(np.arange(mini, 1.01, step=0.1), fontsize=10) 
    ax.plot([0, 1], [0, 1], transform=ax.transAxes)
    plt.xlabel(label1, fontsize=18)
    plt.ylabel(label2, fontsize=18)
    #plt.axis('scaled')
    ax.grid(color=(0.9,0.9,0.9,0.9), linestyle='dotted')
    fname = '../../temp/0_compare_{}_{}.eps'.format(label1,label2)
    plt.savefig(fname)
    print('Saved ' + fname)
    