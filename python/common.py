import os, timeit, subprocess, sys
import uuid
import multiprocessing as mp
import numpy as np
import pandas as pd
from sys import stderr
#from sktime.utils.load_data import load_from_tsfile_to_dataframe
from sktime.utils.data_io import load_from_tsfile_to_dataframe
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
from sklearn.linear_model import SGDClassifier
from sklearn.linear_model import RidgeClassifierCV
from sklearn.neighbors import KNeighborsClassifier
from sklearn.preprocessing import StandardScaler
from sklearn.pipeline import make_pipeline
import random
from scipy.constants.constants import alpha

def copy_ts(filename, filename_output, data_x, data_y):
    #copy header ts file
    f = open(filename, 'r')
    fo = open(filename_output,'w')
    for line in f:
        if line.startswith('@'):
            fo.write(line)
    f.close()
    #save data
    nr_series = data_y.shape[0]
    for i in range(0,nr_series):
        arr = data_x['dim_0'].iloc[i].to_numpy().tolist()
        y = data_y[i]
        for i in range(0, len(arr)-1):
            fo.write('{:.6f}'.format(arr[i]))
            fo.write(',')
        fo.write('{:.6f}'.format(arr[len(arr)-1]))
        fo.write(':')
        fo.write(y)
        fo.write('\n')
    fo.close()
    
#i.e. to split training in validation train/test
def copy_ts_mv_single_file(filename, filename_output, data_x, data_y):
    nr_of_dimensions = 0
    #copy header ts file
    f = open(filename, 'r')
    fo = open(filename_output,'w')
    for line in f:
        if line.startswith('@'):
            fo.write(line)
        if line.startswith('@dimensions'):
            nr_of_dimensions = int(line[len('@dimensions '):])
    f.close()
    #save data
    nr_series = data_y.shape[0]
    for i in range(0,nr_series):
        for d in range(0,nr_of_dimensions):
            arr = data_x['dim_{}'.format(d)].iloc[i].to_numpy().tolist()
            y = data_y[i]
            for i in range(0, len(arr)-1):
                fo.write('{:.6f}'.format(arr[i]))
                fo.write(',')
            fo.write('{:.6f}'.format(arr[len(arr)-1]))
            fo.write(':')
        fo.write(y)
        fo.write('\n')
    fo.close()
    
def copy_ts_multivariate(filename, data_x, data_y):
    nr_of_dimensions = 0
    #copy header ts to memory
    lines = []
    f = open(filename, 'r')
    for line in f:
        if line.startswith('@'):
            lines.append(line)
        if line.startswith('@dimensions'):
            nr_of_dimensions = int(line[len('@dimensions '):])
    f.close()
    #copy header for each dimension
    filenames = []
    for dimension in range(0,nr_of_dimensions):
        filename_output_dim_i = filename + '_dim_{:03d}.ts'.format(dimension)
        filenames.append(filename_output_dim_i)
        fo = open(filename_output_dim_i,'w')
        for line in lines:
            fo.write(line)
        fo.close()
    #save data for each dimension
    nr_series = data_y.shape[0]
    for dimension in range(0,nr_of_dimensions):
        filename_output_dim_i = filename + '_dim_{:03d}.ts'.format(dimension)
        fo = open(filename_output_dim_i,'a') #append
        for i in range(0,nr_series):
            arr = data_x['dim_{}'.format(dimension)].iloc[i].to_numpy().tolist()
            y = data_y[i]
            for i in range(0, len(arr)-1):
                fo.write('{:.6f}'.format(arr[i]))
                fo.write(',')
            fo.write('{:.6f}'.format(arr[len(arr)-1]))
            fo.write(':')
            fo.write(y)
            fo.write('\n')
        fo.close()
        #print('saved ' + filename_output_dim_i)
    return filenames
    
CLASSPATH = '../target/petsc-0.9.0-jar-with-dependencies.jar:../lib/jmotif-sax-1.1.5-SNAPSHOT-jar-with-dependencies.jar'

def run_cmd(cmd, output_fname, verbose=False):
    try:
        if os.path.exists(output_fname):
            os.remove(output_fname)
        if verbose:
             subprocess.call(cmd)
        else:
            p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            out, err = p.communicate(timeout=600)
            str_err = err.decode("utf-8")
            if len(str_err) > 0:
                stderr.write(str_err) 
                return 1
        return 0
    except Exception as e:
        stderr.write(str(e))
        stderr.write("process aborted")
        return 1
    #run scikir learn classification
    if os.stat(output_fname).st_size==0: #no patterns found -> file is empty
        stderr.write("output empty")
        return 1

def clear(*fnames):
    for fname in fnames:
        if os.path.exists(fname):
            os.remove(fname)

DEFAULT_MAX_HEAP_SIZE = '-Xmx8G'

#PETSC Pattern-based Embedding for Time Series Classification
def petsc(train_filename, test_filename, window, w, alphabet, k, min_len, rdur):
    #run java: mining + embedding    
    embedding_train_fname = '../temp/petsc_embedding_train.txt' + str(uuid.uuid1())
    embedding_test_fname = '../temp/petsc_embedding_test.txt' + str(uuid.uuid1())
    cmd = ['java', DEFAULT_MAX_HEAP_SIZE, '-cp', CLASSPATH, 
                     'be.uantwerpen.mining.MineTopKSequentialPatterns', 
                     '-input', train_filename, '-input_test', test_filename,
                    #segmenting, sax
                    '-window', str(window),
                    '-paa_win', str(w), '-bins', str(alphabet), 
                    #mining constraints
                    '-k', str(k), 
                    '-duration', str(rdur), 
                    '-min_size', str(min_len), 
                    '-max_size', str(w),  #equal to max_size
                    #output
                    '-output', embedding_train_fname, '-output_test', embedding_test_fname]
    start = timeit.default_timer()
    result = run_cmd(cmd, embedding_train_fname)
    if result == 1:
        return 1
    try:
        X_embtrain = pd.read_csv(embedding_train_fname,header=None) 
        X_embtest = pd.read_csv(embedding_test_fname,header=None)
    except Exception as e: #e.g. no patterns
        print(e)
        print("process aborted")
        return 1
    #elastic net
    train_x_UNUSED, train_y = load_from_tsfile_to_dataframe(train_filename) #need Y labels
    test_x_UNUSED, test_y = load_from_tsfile_to_dataframe(test_filename) #need Y labels
    try:
        clf = make_pipeline(StandardScaler(), SGDClassifier(max_iter=1000, tol=1e-3, penalty='elasticnet', n_jobs=-1))
        clf.fit(X_embtrain, train_y)
        y_pred = clf.predict(X_embtest)
        err = 1.0 - accuracy_score(test_y, y_pred)
    except Exception as e: #e.g. no patterns
        print(e)
        print("process aborted")
        return 1
    stop = timeit.default_timer()
    clear(embedding_train_fname,embedding_test_fname)
    print("Error PETSC(f={}, window={}, (w={}, alpha={}), (k={}, min_len={}, rdur={})): {:.4f}. Elapsed: {:.1f}s".format(
            os.path.basename(train_filename), window, w, alphabet, k, min_len, rdur, err, stop-start))
    return err

def petsc_soft(train_filename, test_filename, window, w, alphabet, k, min_len, tau): #rdur always 1.0 in current impl
    #run java: mining + embedding    
    embedding_train_fname = '../temp/petsc_embedding_train.txt' + str(uuid.uuid1())
    embedding_test_fname = '../temp/petsc_embedding_test.txt' + str(uuid.uuid1())
    cmd = ['java', DEFAULT_MAX_HEAP_SIZE, '-cp', CLASSPATH, 
                     'be.uantwerpen.mining.MineTopKSequentialPatterns', 
                     '-input', train_filename, '-input_test', test_filename,
                    #segmenting, sax
                    '-window', str(window),
                    '-paa_win', str(w), '-bins', str(alphabet), 
                    #mining constraints
                    '-k', str(k), 
                    '-duration', str(1.0), 
                    '-min_size', str(min_len), 
                    '-max_size', str(w),  #equal to max_size
                    #for soft
                    '-soft', 'true',
                    '-tau', str(tau),
                    #output
                    '-output', embedding_train_fname, '-output_test', embedding_test_fname]
    start = timeit.default_timer()
    result = run_cmd(cmd, embedding_train_fname)
    if result == 1:
        return 1
    try:
        X_embtrain = pd.read_csv(embedding_train_fname,header=None) 
        X_embtest = pd.read_csv(embedding_test_fname,header=None)
    except Exception as e: #e.g. no patterns
        print(e)
        print("process aborted")
        return 1
    #elastic net
    train_x_UNUSED, train_y = load_from_tsfile_to_dataframe(train_filename) #need Y labels
    test_x_UNUSED, test_y = load_from_tsfile_to_dataframe(test_filename) #need Y labels
    try:
        clf = make_pipeline(StandardScaler(), SGDClassifier(max_iter=1000, tol=1e-3, penalty='elasticnet', n_jobs=-1))
        clf.fit(X_embtrain, train_y)
        y_pred = clf.predict(X_embtest)
        err = 1.0 - accuracy_score(test_y, y_pred)
    except Exception as e: #e.g. no patterns
        print(e)
        print("process aborted")
        return 1
    stop = timeit.default_timer()
    clear(embedding_train_fname,embedding_test_fname)
    print("Error PETSC-SOFT(f={}, window={}, (w={}, alpha={}), (k={}, min_len={}, tau={}))): {:.4f}. Elapsed: {:.1f} ".format(
        os.path.basename(train_filename),window,w,alphabet,k,min_len,tau,
        err, stop-start))
    return err

#Discriminative mining
def petsc_disc(train_filename, test_filename, window, w, alphabet, k, min_len, rdur):
    start = timeit.default_timer()
    #embedding
    embedding_train_fname = '../temp/petsc_disc_embedding_train.txt' + str(uuid.uuid1())
    embedding_test_fname = '../temp/petsc_disc_embedding_test.txt' + str(uuid.uuid1())
    cmd = ['java', DEFAULT_MAX_HEAP_SIZE, '-cp', CLASSPATH, 
                     'be.uantwerpen.mining.MineTopKDiscriminativeSequentialPatternsNew', 
                     '-input', train_filename, '-input_test', test_filename,
                    #preprocessing
                    '-paa_win', str(w), '-bins', str(alphabet), 
                    #constraints
                    '-duration', str(rdur), 
                    '-min_size', str(min_len), 
                    '-max_size', str(w), 
                    #mining
                    '-window', str(window),
                    '-k', str(k), 
                    #output
                    '-output', embedding_train_fname, '-output_test', embedding_test_fname]
    #if redundancy_cover:
    #    cmd = cmd + ['-redundancy_cover','true']
    result = run_cmd(cmd, embedding_train_fname)
    if result == 1:
        return 1
    try:
        X_embtrain = pd.read_csv(embedding_train_fname,header=None) 
        X_embtest = pd.read_csv(embedding_test_fname,header=None)
    except Exception as e: #e.g. no patterns
            print(e)
            print("process aborted")
            return 1000000
    #elastic net
    train_x_UNUSED, train_y = load_from_tsfile_to_dataframe(train_filename) #need Y labels
    test_x_UNUSED, test_y = load_from_tsfile_to_dataframe(test_filename) #need Y labels
    try:
        clf = make_pipeline(StandardScaler(), SGDClassifier(max_iter=1000, tol=1e-3, penalty='elasticnet', n_jobs=-1))
        clf.fit(X_embtrain, train_y)
        y_pred = clf.predict(X_embtest)
        err = 1.0 - accuracy_score(test_y, y_pred)
    except Exception as e: #e.g. no patterns
        print(e)
        print("process aborted")
        return 1
    stop = timeit.default_timer()
    clear(embedding_train_fname,embedding_test_fname)    
    print("Error PETSC-DISC(f={}, window={}, (w={}, alpha={}), (k={}, min_len={}, rdur={})): {:.4f}. Elapsed: {:.1f} ".format(
        os.path.basename(train_filename), window, w, alphabet, k, min_len, rdur, err, stop-start))
    return err


     

def mr_petsc(train_filename, test_filename, verbose=False):
    w = 15
    alphabet = 4
    min_len = 5
    rdur = 1.1
    k = 200
    return mr_petsc_params(train_filename, test_filename, w, alphabet, k, min_len, rdur, False, verbose)

def mr_petsc_params_rel(train_filename, test_filename, w, alphabet, k, min_len_rel, rdur, soft=False):
    return mr_petsc_params(train_filename, test_filename, w, alphabet, k, int(min_len_rel * w), rdur, soft)
        
def mr_petsc_params(train_filename, test_filename, w, alphabet, k, min_len, rdur, soft=False, verbose=False, stride=1):
    start = timeit.default_timer()
    X_train, y_train = load_from_tsfile_to_dataframe(train_filename) 
    X_test, y_test = load_from_tsfile_to_dataframe(test_filename)    
    fase1_elapsed = 0
    fase2_elapsed = 0
    #embedding
    try:
        start_fase1 = timeit.default_timer()
        embedding_train_fname = '../temp/mr_petsc_embedding_train.txt' + str(uuid.uuid1())
        embedding_test_fname = '../temp/mr_petsc_embedding_test.txt' + str(uuid.uuid1())
        embeddings_train = []
        embeddings_test = []
        nr_series = y_train.shape[0]
        window = min([X_train['dim_0'].iloc[i].to_numpy().shape[0] for i in range(0,nr_series)])
        while window > w:
            cmd = ['java', DEFAULT_MAX_HEAP_SIZE, '-cp', CLASSPATH, 
                            'be.uantwerpen.mining.MineTopKSequentialPatterns', 
                             '-input', train_filename, '-input_test', test_filename,
                            #preprocessing
                            '-paa_win', str(w), '-bins', str(alphabet), 
                            #constraints
                            '-min_size', str(min_len), 
                            '-max_size', str(w), 
                            #mining
                            '-window', str(window),
                            '-stride', str(stride),
                            '-k', str(k), 
                            #output
                            '-output', embedding_train_fname, '-output_test', embedding_test_fname]
            if not soft:
                cmd = cmd + ['-duration', str(rdur)]
            else:
                cmd = cmd + ['-duration', '1.0', '-soft', 'true', '-tau', '2.0']
            result = run_cmd(cmd, embedding_train_fname, verbose)
            if result == 1:
                return 1
            X_embtrain = pd.read_csv(embedding_train_fname,header=None) 
            X_embtest = pd.read_csv(embedding_test_fname,header=None) 
            embeddings_train.append(X_embtrain)
            embeddings_test.append(X_embtest)
            clear(embedding_train_fname,embedding_test_fname)
            window = window // 2
        #concat
        train_concat = pd.concat(embeddings_train,axis=1)
        test_concat = pd.concat(embeddings_test,axis=1)
        stop_fase1 = timeit.default_timer()
        fase1_elapsed = stop_fase1 - start_fase1
    except Exception as e:
        print(e)
        print("process aborted")
        return 1
    err = 1
    try:
        start_fase2 = timeit.default_timer()
        clf = make_pipeline(StandardScaler(), SGDClassifier(max_iter=1000, tol=1e-3, penalty='elasticnet', n_jobs=-1))
        clf.fit(train_concat, y_train)
        #clf = RidgeClassifierCV(alphas = np.logspace(-3, 3, 10), normalize = True)
        #clf.fit(train_concat, y_train)    
        y_pred = clf.predict(test_concat)
        err = 1.0 - accuracy_score(y_test, y_pred)
        stop_fase2 = timeit.default_timer()
        fase2_elapsed = stop_fase2 - start_fase2
    except Exception as e: #e.g. no patterns
        print(e)
        print("process aborted")
        return 1
    stop = timeit.default_timer()
    print("MR-PETSC(f={}, (w={}, alpha={}), (k={}, min_len={}, rdur={}) error is {:.4f}. Elapsed: {:.1f} ({:.1f}/{:.1f}) ".format(
       os.path.basename(train_filename), w, alphabet, k, min_len, rdur, err, stop-start, fase1_elapsed, fase2_elapsed))
    return err

def mr_petsc_embed(train_filename, test_filename, window, w, alphabet, k, min_len, rdur, stride=1):
    embedding_train_fname = '../temp/mr_petsc_embedding_train.txt' + str(uuid.uuid1())
    embedding_test_fname = '../temp/mr_petsc_embedding_test.txt' + str(uuid.uuid1())
    cmd = ['java', DEFAULT_MAX_HEAP_SIZE, '-cp', CLASSPATH, 
                        'be.uantwerpen.mining.MineTopKSequentialPatterns', 
                         '-input', train_filename, '-input_test', test_filename,
                        #preprocessing
                        '-paa_win', str(w), '-bins', str(alphabet), 
                        #constraints
                        '-duration', str(rdur), 
                        '-min_size', str(min_len), 
                        '-max_size', str(w), 
                        #mining
                        '-window', str(window),
                        '-stride', str(stride),
                        '-k', str(k), 
                        #output
                        '-output', embedding_train_fname, '-output_test', embedding_test_fname]
    result = run_cmd(cmd, embedding_train_fname)
    if result == 1:
        return (None, None)
    X_embtrain = pd.read_csv(embedding_train_fname,header=None) 
    X_embtest = pd.read_csv(embedding_test_fname,header=None)
    clear(embedding_train_fname,embedding_test_fname)
    #print(X_embtrain.head())
    return X_embtrain, X_embtest

def mr_petsc_multivariate_rel(train_filename, test_filename, w, alphabet, k, min_len_rel, rdur, stride):
    return mr_petsc_multivariate(train_filename, test_filename, w, alphabet, k, int(min_len_rel * w), rdur, stride)
    
def mr_petsc_multivariate(train_filename, test_filename, w, alphabet, k, min_len, rdur, stride):
    start = timeit.default_timer()
    X_train, y_train = load_from_tsfile_to_dataframe(train_filename) 
    X_test, y_test = load_from_tsfile_to_dataframe(test_filename)    

    #Split each dimension into seperate file
    filenames_train = copy_ts_multivariate(train_filename, X_train, y_train)
    filenames_test = copy_ts_multivariate(test_filename, X_test, y_test)
    
    #embedding
    embeddings_train = []
    embeddings_test = []
    nr_series = y_train.shape[0]
    for dimension in range(0, len(filenames_train)):
        window = min([X_train['dim_{}'.format(dimension)].iloc[i].to_numpy().shape[0] for i in range(0,nr_series)])
        while window > w:
            X_embtrain, X_embtest = mr_petsc_embed(filenames_train[dimension], filenames_test[dimension], window, w, alphabet, k, min_len, rdur, stride)
            if X_embtrain is None:
                continue
            #print(X_embtrain.shape)
            #print(X_embtest.shape)
            embeddings_train.append(X_embtrain)
            embeddings_test.append(X_embtest)
            window = window // 2 
    #concat
    train_concat = pd.concat(embeddings_train,axis=1)
    test_concat = pd.concat(embeddings_test,axis=1)
    #print(train_concat.shape)
    #print(y_train.shape)
    #print(test_concat.shape)
    #print(y_test.shape)
    err = 1
    try:
        clf = make_pipeline(StandardScaler(), SGDClassifier(max_iter=1000, tol=1e-3, penalty='elasticnet', n_jobs=-1)) #DONE: Multi-threaded
        clf.fit(train_concat, y_train)
        y_pred = clf.predict(test_concat)
        err = 1.0 - accuracy_score(y_test, y_pred)
    except Exception as e: #e.g. no patterns
        print(e)
        print("process aborted")
        return 1
    stop = timeit.default_timer()
    print("MR-PETSC-MULTIVARIATE(f={}, (w={},alpha={}), (k={},min_len={},rdur={})) error on is {:.4f}. Elapsed: {:.1f} ".format(
        os.path.basename(train_filename), w, alphabet, k, min_len, rdur, err, stop-start))
    return err


#Bag of Patterns
def bag_of_patterns(train_filename, test_filename, window, w, alphabet):
    start = timeit.default_timer()
    #embedding
    embedding_train_fname = '../temp/bop_embedding_train.txt' + str(uuid.uuid1())
    embedding_test_fname = '../temp/bop_embedding_test.txt' + str(uuid.uuid1())
    cmd = ['java', DEFAULT_MAX_HEAP_SIZE, '-cp', CLASSPATH, 
                     'be.uantwerpen.mining.BagOfPatterns', 
                     '-input', train_filename, '-input_test', test_filename,
                    #preprocessing
                    '-paa_win', str(w), '-bins', str(alphabet), 
                    #mining
                    '-window', str(window),
                    #output
                    '-output', embedding_train_fname, '-output_test', embedding_test_fname]
    result = run_cmd(cmd, embedding_train_fname)
    if result == 1:
        return 1
    #knn
    try:
        X_embtrain = pd.read_csv(embedding_train_fname,header=None) 
        X_embtest = pd.read_csv(embedding_test_fname,header=None)
    except Exception as e: #e.g. no patterns
        print(e)
        print("process aborted")
        return 1
    train_x_UNUSED, y_train = load_from_tsfile_to_dataframe(train_filename) #need Y labels
    test_x_UNUSED, y_test = load_from_tsfile_to_dataframe(test_filename) #need Y labels
    #ok
    try:
        knn = KNeighborsClassifier(n_neighbors=1)
        knn.fit(X_embtrain, y_train)
        y_pred = knn.predict(X_embtest)
        err = 1.0 - accuracy_score(y_test, y_pred)
    except Exception as e:
        print(e)
        print("process aborted")
        return 1
    stop = timeit.default_timer()
    clear(embedding_train_fname,embedding_test_fname)
    print("Error BOP(f={}, window={}, (w={},alpha={})): {:.4f}. Elapsed: {:.1f} ".format(
            os.path.basename(train_filename), window, w, alphabet, err, stop-start))
    return err


# ---------- parameter optimalisation -----------
#For optimising preprocessing parameters $(\Delta t, w, \alpha)$ we use \emph{random search} on a validation set \citep{bergstra2012random}. 
#That is, we iterate through a fixed number of random sampled parameter settings and keep the parameter setting 
#with the best accuracy after $100$ iterations. 
#Here, $\Delta t$ is randomly sampled between $25$ and $|S_i|$, $w$ between $5$ and $25$ and 
#$\alpha$ between $3$ and $10$.
#e.g. call (deltat, w, alpha) = random_search_sax_petsc(train_filename,  train_x, validation_x, train_y, validation_y)

def petsc_rel(train_filename, test_filename, window, w, alphabet, k, min_len_rel, rdur):
    return petsc(train_filename, test_filename, window, w, alphabet, k, int(min_len_rel * w), rdur)

def petsc_soft_rel(train_filename, test_filename, window, w, alphabet, k, min_len_rel, tau_rel):
    return petsc_soft(train_filename, test_filename, window, w, alphabet, k, int(min_len_rel * w), 1 / (2.0 * alphabet) * tau_rel)

def random_search_petsc(train_filename, test_filename, iter=100):
    print(">>PETSC parameter optimisation")
    #compute minsize
    train_x, train_y = load_from_tsfile_to_dataframe(train_filename)
    nr_series = train_y.shape[0]
    minlen = 10000000
    for i in range(0,nr_series):
        len_i = train_x['dim_0'].iloc[i].to_numpy().shape[0]
        minlen = min(minlen, len_i)
    #random search     
    mini_length, maxi_length = window_limits(train_x,train_y)
    best_err, best_params = random_search_multi_threaded(petsc_rel, {
                            'train_filename': train_filename, 'test_filename': test_filename, 
                            #preprocessing dynamic
                            'window': list(range(mini_length, maxi_length)), 
                            'w': list(range(5,31)),
                            'alphabet': list(range(3,13)),  
                            #mining fixed
                            'k': list(range(500,1500)),
                            'min_len_rel': [0.1,0.2,0.3,0.4],
                            'rdur': [1.0,1.1,1.2,1.5]}, 
                            check= lambda p: p['w'] <= p['window'] and int(p['min_len_rel'] * p['w']) >= 3,
                            max_iter=iter)
    return best_params



def run_petsc_classic_with_parameter_optimisation(train_filename, test_filename, iter=100):
    #load data for training
    train_x, train_y = load_from_tsfile_to_dataframe(train_filename)
    test_x, test_y = load_from_tsfile_to_dataframe(test_filename)
    
    #Parameter optimalisation
    train_valid_x, test_valid_x, train_valid_y, test_valid_y = train_test_split(train_x, train_y, test_size=0.33, random_state=42)
    fname_validation_train = train_filename + '_validation_train.ts'
    fname_validation_test = train_filename + '_validation_test.ts'
    copy_ts(train_filename, fname_validation_train, train_valid_x, train_valid_y)
    copy_ts(train_filename, fname_validation_test, test_valid_x, test_valid_y)
    #search:
    best_params = random_search_petsc(fname_validation_train, fname_validation_test, iter=iter)
    
    #Run PETSC outer loop
    err= petsc(train_filename, test_filename, best_params['window'], best_params['w'], best_params['alphabet'], 
               best_params['k'], int(best_params['w'] * best_params['min_len_rel']) , best_params['rdur'])
    print('PETSC error on {:} is {:.3f} with parameters {:}'.format(os.path.basename(train_filename), err, best_params))
    return err

def random_search_bop(train_filename, test_filename, iter=100):
    print(">>BOP parameter optimisation")
    #compute minsize
    train_x, train_y = load_from_tsfile_to_dataframe(train_filename)
    nr_series = train_y.shape[0]
    minlen = 10000000
    for i in range(0,nr_series):
        len_i = train_x['dim_0'].iloc[i].to_numpy().shape[0]
        minlen = min(minlen, len_i)
    mini_length, maxi_length = window_limits(train_x,train_y)
    #random search     
    best_err, best_params = random_search_multi_threaded(bag_of_patterns, {
                            'train_filename': train_filename, 'test_filename': test_filename, 
                            #preprocessing dynamic
                            'window': list(range(mini_length, maxi_length)), 
                            'w': list(range(5,31)),
                            'alphabet': list(range(3,13))}, 
                            check= lambda p: p['w'] <= p['window'],
                            max_iter=iter)
    return best_params

def run_bag_of_patterns_with_parameter_optimisation(train_filename, test_filename, iter=100):
    #load data for training
    train_x, train_y = load_from_tsfile_to_dataframe(train_filename)
    test_x, test_y = load_from_tsfile_to_dataframe(test_filename)
    
    #Parameter optimalisation
    train_valid_x, test_valid_x, train_valid_y, test_valid_y = train_test_split(train_x, train_y, test_size=0.33, random_state=42)
    fname_validation_train = train_filename + '_validation_train.ts'
    fname_validation_test = train_filename + '_validation_test.ts'
    copy_ts(train_filename, fname_validation_train, train_valid_x, train_valid_y)
    copy_ts(train_filename, fname_validation_test, test_valid_x, test_valid_y)
    #search:
    best_params = random_search_bop(fname_validation_train, fname_validation_test, iter=iter)
    
    #Run BOP outer loop
    err= bag_of_patterns(train_filename, test_filename, best_params['window'], best_params['w'], best_params['alphabet'])
    print('BOP error on {:} is {:.3f} with parameters {:}'.format(os.path.basename(train_filename), err, best_params))
    return err

def petsc_disc_rel(train_filename, test_filename, window, w, alphabet, k, min_len_rel, rdur):
    return petsc_disc(train_filename, test_filename, window, w, alphabet, k, int(min_len_rel * w), rdur)

def window_limits(train_x, train_y):
    nr_series = train_y.shape[0]
    minlen = 10000000
    for i in range(0,nr_series):
        len_i = train_x['dim_0'].iloc[i].to_numpy().shape[0]
        minlen = min(minlen, len_i)
    mini_length_search = int(minlen*0.1) #TODO: also for other datasets
    maxi_length_search = minlen
    if mini_length_search < 3: 
        mini_length_search = 3
    return mini_length_search, maxi_length_search
    
            
#Note: copy/paste petsc
def random_search_petsc_disc(train_filename, test_filename, iter=100):
    print(">>PETSC-DISC parameter optimisation")
    #compute minsize
    train_x, train_y = load_from_tsfile_to_dataframe(train_filename)
    nr_series = train_y.shape[0]
    mini_length, maxi_length = window_limits(train_x,train_y)
    #random search     
    best_err, best_params = random_search_multi_threaded(petsc_disc_rel, {
                            'train_filename': train_filename, 'test_filename': test_filename, 
                            #preprocessing dynamic
                            'window': list(range(mini_length, maxi_length)), 
                            'w': list(range(5,31)),
                            'alphabet': list(range(3,13)),  
                            #mining fixed
                            'k': list(range(500,2500)),
                            'min_len_rel': [0.1,0.2,0.3,0.4],
                            'rdur': [1.0,1.1,1.2,1.5]},     
                            check= lambda p: p['w'] <= p['window'] and int(p['min_len_rel'] * p['w']) >= 3,
                            max_iter=iter)
    return best_params

#Note: copy/paste petsc
def run_petsc_disc_with_parameter_optimisation(train_filename, test_filename, iter=100):
    #load data for training
    train_x, train_y = load_from_tsfile_to_dataframe(train_filename)
    test_x, test_y = load_from_tsfile_to_dataframe(test_filename)
    
    #Parameter optimalisation
    train_valid_x, test_valid_x, train_valid_y, test_valid_y = train_test_split(train_x, train_y, test_size=0.33, random_state=42)
    fname_validation_train = train_filename + '_validation_train.ts'
    fname_validation_test = train_filename + '_validation_test.ts'
    copy_ts(train_filename, fname_validation_train, train_valid_x, train_valid_y)
    copy_ts(train_filename, fname_validation_test, test_valid_x, test_valid_y)
    #search:
    best_params = random_search_petsc_disc(fname_validation_train, fname_validation_test, iter=iter)
    
    #Run PETSC outer loop
    err= petsc_disc(train_filename, test_filename, best_params['window'], best_params['w'], best_params['alphabet'], 
               best_params['k'], int(best_params['w'] * best_params['min_len_rel']) , best_params['rdur'])
    print('PETSC-DISC error on {:} is {:.3f} with parameters {:}'.format(os.path.basename(train_filename), err, best_params))
    return err


def random_search_petsc_soft(train_filename, test_filename, iter=100):
    print(">>PETSC-SOFT parameter optimisation")
    #compute minsize
    train_x, train_y = load_from_tsfile_to_dataframe(train_filename)
    nr_series = train_y.shape[0]
    minlen = 10000000
    for i in range(0,nr_series):
        len_i = train_x['dim_0'].iloc[i].to_numpy().shape[0]
        minlen = min(minlen, len_i)
    #random search    
    mini_length, maxi_length = window_limits(train_x,train_y)
    best_err, best_params = random_search_multi_threaded(petsc_soft_rel, {
                            'train_filename': train_filename, 'test_filename': test_filename, 
                            #preprocessing dynamic
                            'window': list(range(mini_length, maxi_length)), 
                            'w': list(range(5,31)),
                            'alphabet': list(range(3,13)),  
                            #mining fixed
                            'k': list(range(500,1500)),
                            'min_len_rel': [0.1,0.2,0.3,0.4],
                            'tau_rel': [1.0,2.0,3.0]}, 
                            max_iter=iter,
                            check= lambda p: p['w'] <= p['window'] and int(p['min_len_rel'] * p['w']) >= 3)
    return best_params



def run_petsc_soft_with_parameter_optimisation(train_filename, test_filename, iter=100):
    #load data for training
    train_x, train_y = load_from_tsfile_to_dataframe(train_filename)
    test_x, test_y = load_from_tsfile_to_dataframe(test_filename)
    
    #Parameter optimalisation
    train_valid_x, test_valid_x, train_valid_y, test_valid_y = train_test_split(train_x, train_y, test_size=0.33, random_state=42)
    fname_validation_train = train_filename + '_validation_train.ts'
    fname_validation_test = train_filename + '_validation_test.ts'
    copy_ts(train_filename, fname_validation_train, train_valid_x, train_valid_y)
    copy_ts(train_filename, fname_validation_test, test_valid_x, test_valid_y)
    #search:
    best_params = random_search_petsc_soft(fname_validation_train, fname_validation_test, iter=iter)
    
    #Run PETSC outer loop
    err= petsc_soft_rel(train_filename, test_filename, best_params['window'], best_params['w'], best_params['alphabet'], 
               best_params['k'], best_params['min_len_rel'] , best_params['tau_rel'])
    print('PETSC-SOFT error on {:} is {:.3f} with parameters {:}'.format(os.path.basename(train_filename), err, best_params))
    return err

def random_search_mr_petc(train_filename, test_filename, iter=100):
    print(">>MR-PETSC parameter optimisation")
    #compute minsize
    train_x, train_y = load_from_tsfile_to_dataframe(train_filename)
    nr_series = train_y.shape[0]
    minlen = 10000000
    for i in range(0,nr_series):
        len_i = train_x['dim_0'].iloc[i].to_numpy().shape[0]
        minlen = min(minlen, len_i)
    #random search    
    mini_length, maxi_length = window_limits(train_x,train_y)    
    best_err, best_params = random_search_multi_threaded(mr_petsc_params_rel, {
                            'train_filename': train_filename, 'test_filename': test_filename, 
                            #preprocessing dynamic
                            'w': list(range(5,31)),
                            'alphabet': list(range(3,13)),  
                            #mining fixed
                            'k': list(range(500,1500)),
                            'min_len_rel': [0.1,0.2,0.3,0.4],
                            'rdur': [1.0,1.1,1.2,1.5]}, 
                            check=lambda p: int(p['min_len_rel'] * p['w']) >= 3,
                            max_iter=iter)
    return best_params


def run_mr_petsc_parameter_optimisation(train_filename, test_filename, iter=100):
    #load data for training
    train_x, train_y = load_from_tsfile_to_dataframe(train_filename)
    test_x, test_y = load_from_tsfile_to_dataframe(test_filename)
    
    #Parameter optimalisation
    train_valid_x, test_valid_x, train_valid_y, test_valid_y = train_test_split(train_x, train_y, test_size=0.33, random_state=42)
    fname_validation_train = train_filename + '_validation_train.ts'
    fname_validation_test = train_filename + '_validation_test.ts'
    copy_ts(train_filename, fname_validation_train, train_valid_x, train_valid_y)
    copy_ts(train_filename, fname_validation_test, test_valid_x, test_valid_y)
    #search:
    best_params = random_search_mr_petc(fname_validation_train, fname_validation_test, iter=iter)
    
    #Run PETSC outer loop
    err= mr_petsc_params_rel(train_filename, test_filename, best_params['w'], best_params['alphabet'], 
               best_params['k'], best_params['min_len_rel'] , best_params['rdur'])
    print('MR-PETSC error on {:} is {:.3f} with parameters {:}'.format(os.path.basename(train_filename), err, best_params))
    return err

# def random_search_mr_petc_soft(train_filename, test_filename, iter=100):
#     print(">>MR-PETSC-SOFT parameter optimisation")
#     #compute minsize
#     train_x, train_y = load_from_tsfile_to_dataframe(train_filename)
#     nr_series = train_y.shape[0]
#     minlen = 10000000
#     for i in range(0,nr_series):
#         len_i = train_x['dim_0'].iloc[i].to_numpy().shape[0]
#         minlen = min(minlen, len_i)
#     #random search    
#     mini_length, maxi_length = window_limits(train_x,train_y)    
#     best_err, best_params = random_search_multi_threaded(mr_petsc_params_rel, {
#                             'train_filename': train_filename, 'test_filename': test_filename, 
#                             #preprocessing dynamic
#                             'w': list(range(5,31)),
#                             'alphabet': list(range(3,13)),  
#                             #mining fixed
#                             'k': list(range(500,1500)),
#                             'min_len_rel': [0.1,0.2,0.3,0.4],
#                             'rdur': [1.0],
#                             'soft': [True],
#                             }, 
#                             check= lambda p: int(p['min_len_rel'] * p['w']) >= 3,
#                             max_iter=iter)
#     return best_params
# 
# 
# def run_mr_petsc_soft_parameter_optimisation(train_filename, test_filename, iter=100):
#     #load data for training
#     train_x, train_y = load_from_tsfile_to_dataframe(train_filename)
#     test_x, test_y = load_from_tsfile_to_dataframe(test_filename)
#     
#     #Parameter optimalisation
#     train_valid_x, test_valid_x, train_valid_y, test_valid_y = train_test_split(train_x, train_y, test_size=0.33, random_state=42)
#     fname_validation_train = train_filename + '_validation_train.ts'
#     fname_validation_test = train_filename + '_validation_test.ts'
#     copy_ts(train_filename, fname_validation_train, train_valid_x, train_valid_y)
#     copy_ts(train_filename, fname_validation_test, test_valid_x, test_valid_y)
#     #search:
#     best_params = random_search_mr_petc_soft(fname_validation_train, fname_validation_test, iter=iter)
#     
#     #Run PETSC outer loop
#     err= mr_petsc_params_rel(train_filename, test_filename, best_params['w'], best_params['alphabet'], 
#                best_params['k'], best_params['min_len_rel'] , best_params['rdur'], True)
#     print('MR-PETSC-SOFT error on {:} is {:.3f} with parameters {:}'.format(os.path.basename(train_filename), err, best_params))
#     return err



# --------------------------- Parameter optimalisation --------------------
#e.g. random_search('bop', params{'train': train, 'test': test, 'paa': range(5,25,5), 'alphabet': range(4,10), 'window': range(5,25,5)}
def random_search(method, param_ranges, max_iter=100, check=lambda p: True):
    best_params = {}
    best_err = 1000000
    #visited = set()
    it = 0
    timings = []
    while it < max_iter:
        print('{}/{}'.format(it,max_iter))
        #take random sample of parameters
        random_params = {}
        for param, param_range in param_ranges.items():
            if type(param_range) is list:
               random_value_idx = random.randint(0,len(param_range)-1)
               random_params[param] = param_range[random_value_idx]
            else:
               random_params[param] = param_range
        #check
        if not check(random_params):
            continue
        #check if already ran with same parameters
        #h = hash(frozenset(random_params.items())) #see https://stackoverflow.com/questions/5884066/hashing-a-dictionary
        #if h in visited:
        #    continue
        #else:
        #    visited.add(h)
        #run
        #print(random_params)
        start = timeit.default_timer()
        try:
            err = method(**random_params)
        except Exception as e: #e.g. no patterns
            print(e)
            print("process aborted")
            continue
        stop = timeit.default_timer()
        timings.append(stop-start)
        if err < best_err:
            best_params = random_params
            best_err = err
            print("Best parameters: " + str(random_params) + " error: " + '{:.3f}'.format(err))
            if err == 0.0:
                break
        it+=1
    print("Total time: {:.3f} Avg time: {:.3f} Max: {:.3f} Min: {:.3f}".format(sum(timings), sum(timings)/float(len(timings)), max(timings), min(timings)))
    return best_err, best_params

def _run_with_params(method, params):
    start = timeit.default_timer()
    try:
        print('run_with_params{}{}'.format(method,params))
        err = method(**params)
    except Exception as e:  # e.g. no patterns
        stderr.write(str(e))
        stderr.write("process aborted")
        return
    stop = timeit.default_timer()
    return (err, stop - start)
    

#todo: als tijd ... error 0 -> stoppen https://stackoverflow.com/questions/33447055/python-multiprocess-pool-how-to-exit-the-script-when-one-of-the-worker-process
def random_search_multi_threaded(method, param_ranges, max_iter=100, check=lambda p: True):
    #collect parameters
    start = timeit.default_timer()
    it = 0
    parameters = []
    while it < max_iter:
        #take random sample of parameters
        random_params = {}
        for param, param_range in param_ranges.items():
            if type(param_range) is list:
               random_value_idx = random.randint(0,len(param_range)-1)
               random_params[param] = param_range[random_value_idx]
            else:
               random_params[param] = param_range
        #check
        if not check(random_params):
            continue
        parameters.append(random_params)
        it+=1
    #run multi-threaded
    print('Running {} parameters settings on {} processors'.format(len(parameters),mp.cpu_count()))
  
    pool = mp.Pool(mp.cpu_count()//2) #divided by two...
    results = [pool.apply_async(_run_with_params, (method,params)) for params in parameters]
    pool.close() 
    pool.join()
    results = [result.get() for result in results]
    #report
    best_err = 1000000
    best_params = None
    timings = []
    for idx, result in enumerate(results):
        timings.append(result[1])
        if result[0] < best_err:
            best_params = parameters[idx]
            best_err = result[0]
    print("Best parameters: " + str(best_params) + " error: " + '{:.3f}'.format(best_err))
    stop = timeit.default_timer()
    print("Total time: {:.3f} Avg time: {:.3f} Max: {:.3f} Min: {:.3f}".format(stop-start, sum(timings)/float(len(timings)), max(timings), min(timings)))
    return best_err, best_params


#e.g. car_product([{'train':'t.txt'}], 'x', [1,2,3]) -> [{'train': 't.txt', 'x':1}, {'train': 't.txt', 'x':2}...]
def car_product(current, attribute, values):
    output = []
    for d in current:
        for val in values:
            d_new = d.copy()
            d_new[attribute] = val
            output.append(d_new)
    return output

def grid_search(method, param_ranges):
    #enumerate options
    parameter_options = [{}]
    for param, param_range in param_ranges.items():
        if not type(param_range) is list:
            parameter_options = car_product(parameter_options, param, [param_range])
        else:
            parameter_options = car_product(parameter_options, param, param_range)
    print('Found ' + str(len(parameter_options)) + " parameter settings for grid search")
    #run
    best_params = {}
    best_err = 1000000
    for i,params in enumerate(parameter_options):
        print('{}/{}'.format(i,len(parameter_options)))
        #run
        err = method(**params)
        if err < best_err:
            best_params = params
            best_err = err
            print("Best parameters: " + str(params) + " error: " + '{:.3f}'.format(err))
    return best_err, best_params


