from sktime.classification.distance_based import KNeighborsTimeSeriesClassifier
from sktime.classification.dictionary_based import *
from sktime.classification.shapelet_based import MrSEQLClassifier
from sktime.transformers.series_as_features.rocket import Rocket
from sktime.utils.data_io import load_from_tsfile_to_dataframe

from sklearn.linear_model import RidgeClassifierCV

import timeit, os
import common
from common import *

dir = '../data/Univariate_ts/'
selection_saxvsm = ['Adiac','Beef','CBF','Coffee','ECG200','FaceAll','FaceFour','Fish', 'GunPoint',
             'Lightning2', 'Lightning7', 'OliveOil', 'OSULeaf', 'SyntheticControl', 'SwedishLeaf', 'Trace',
             'TwoPatterns', 'Wafer','Yoga']
    
datasets_bakeoff = ['SwedishLeaf', 'GunPoint', 'Trace', 'ElectricDevices', 'Strawberry', 'DiatomSizeReduction', 'OSULeaf', 
                    'SonyAIBORobotSurface2', 'UWaveGestureLibraryZ', 'OliveOil', 'MiddlePhalanxOutlineAgeGroup', 'MedicalImages', 
                    'ECG5000', 'Haptics', 'BeetleFly', 'UWaveGestureLibraryY', 'ToeSegmentation1', 'Symbols', 'Ham', 'Computers', 
                    'NonInvasiveFetalECGThorax1', 'Yoga', 'Adiac', 'WormsTwoClass', 'MoteStrain', 'Herring', 'MiddlePhalanxTW', 'Beef', 
                    'ChlorineConcentration', 'CricketZ', 'CricketY', 'DistalPhalanxOutlineAgeGroup', 'SmallKitchenAppliances', 
                    'RefrigerationDevices', 'Wine', 'UWaveGestureLibraryAll', 'NonInvasiveFetalECGThorax2', 'FiftyWords', 'FordB', 
                    'Coffee', 'CBF', 'ShapeletSim', 'InsectWingbeatSound', 'Lightning7', 'WordSynonyms', 'ArrowHead', 'ScreenType', 
                    'SonyAIBORobotSurface1', 'Plane', 'TwoPatterns', 'InlineSkate', 'CricketX', 'ECGFiveDays', 'FaceAll', 'ECG200', 
                    'BirdChicken', 'PhalangesOutlinesCorrect', 'ProximalPhalanxTW', 'CinCECGTorso', 'Phoneme', 'SyntheticControl', 
                    'Earthquakes', 'TwoLeadECG', 'Wafer', 'DistalPhalanxOutlineCorrect', 'Fish', 'Meat', 'LargeKitchenAppliances', 
                    'StarLightCurves', 'Mallat', 'ShapesAll', 'DistalPhalanxTW', 'HandOutlines', 'FaceFour', 'ItalyPowerDemand', 
                    'UWaveGestureLibraryX', 'ProximalPhalanxOutlineAgeGroup', 'Lightning2', 'FacesUCR', 'ToeSegmentation2', 'Car', 
                    'FordA', 'ProximalPhalanxOutlineCorrect', 'Worms', 'MiddlePhalanxOutlineCorrect']

datasets_bakeoff = sorted(datasets_bakeoff)


#check files
for dataset in datasets_bakeoff:
    train_filename = os.path.join(dir, dataset) + '/' + dataset + '_TRAIN.ts' 
    print('Dataset: {}. Size: {:.1f}MB'.format(dataset, os.stat(train_filename).st_size/1000000))

    
run_boss = False   
run_rival = False

for dataset in ['HandOutlines','StarLightCurves']:
#\emph{HandOutlines} & 2 & 1000 & 370 & 2709
#\emph{StarLightCurves} & 3 & 1000 & 8236 & 1024
    common.DEFAULT_MAX_HEAP_SIZE = '-Xmx16G'
    train_filename = os.path.join(dir, dataset) + '/' + dataset + '_TRAIN.ts' 
    test_filename = os.path.join(dir, dataset) + '/' + dataset + '_TEST.ts' 
    start = timeit.default_timer()
    #s = mr_petsc(train_filename, test_filename, verbose=True)
    s= mr_petsc_params(train_filename, test_filename, w=15, alphabet=4, k=200, min_len=5, rdur=1.1, soft=False, verbose=True, stride=10)
    stop =  timeit.default_timer()
    print('MR-PETCH-DEFAULT:{}: {} :{}'.format(dataset, stop-start, s))
    #stats:
    #\emph{HandOutlines} & 2 & 1000 & 370 & 1000/2709
    #\emph{StarLightCurves} & 3 & 1000 & 8236 & 1000/1024

if run_rival:  
    for dataset in datasets_bakeoff:
        print('Dataset: {}'.format(dataset))
        #MR-PETSC DEFAULT
        start = timeit.default_timer()
        train_filename = os.path.join(dir, dataset) + '/' + dataset + '_TRAIN.ts' 
        test_filename = os.path.join(dir, dataset) + '/' + dataset + '_TEST.ts' 
        s = mr_petsc(train_filename, test_filename)
        stop = timeit.default_timer()
        print('MR-PETSC-DEFAULT: {}: {} : {}'.format(dataset,stop-start, s))
        #'DTW'
        start = timeit.default_timer()
        X_train, y_train = load_from_tsfile_to_dataframe(train_filename)
        X_test, y_test = load_from_tsfile_to_dataframe(test_filename)
        knn = KNeighborsTimeSeriesClassifier(n_neighbors=1, metric="dtw")
        knn.fit(X_train, y_train)
        s = knn.score(X_test, y_test)
        stop = timeit.default_timer()
        print('DTW: {}: {} : {}'.format(dataset,stop-start, 1.0 - s))
        #'BOSS'
        if run_boss:
            start = timeit.default_timer()
            X_train, y_train = load_from_tsfile_to_dataframe(train_filename)
            X_test, y_test = load_from_tsfile_to_dataframe(test_filename)
            boss = BOSSEnsemble(random_state=47)
            boss.fit(X_train, y_train)
            s = boss.score(X_test,y_test)
            stop = timeit.default_timer() 
            print('BOSS ensemble: {}: {} : {}'.format(dataset,stop-start, 1.0 - s))
        #MrSQL
        start = timeit.default_timer()
        X_train, y_train = load_from_tsfile_to_dataframe(train_filename)
        X_test, y_test = load_from_tsfile_to_dataframe(test_filename)
        ss_clf = MrSEQLClassifier(seql_mode='fs', symrep=['sax']) #not: ['sax', 'sfa']
        ss_clf.fit(X_train, y_train)
        s = ss_clf.score(X_test, y_test)
        stop = timeit.default_timer() 
        print('MrSEQL: {}: {} : {}'.format(dataset,stop-start, 1.0 - s))
        #ROCKET
        start = timeit.default_timer()
        X_train, y_train = load_from_tsfile_to_dataframe(train_filename)
        X_test, y_test = load_from_tsfile_to_dataframe(test_filename)
        rocket = Rocket() # by default, ROCKET uses 10,000 kernels
        rocket.fit(X_train)
        X_train_transform = rocket.transform(X_train)
        classifier = RidgeClassifierCV(alphas = np.logspace(-3, 3, 10), normalize = True)
        classifier.fit(X_train_transform, y_train)
        X_test_transform = rocket.transform(X_test)
        s = classifier.score(X_test_transform, y_test)
        stop = timeit.default_timer() 
        print('ROCKET: {}: {} : {}'.format(dataset,stop-start, 1.0 - s))

for dataset in datasets_bakeoff:
    print('Dataset: {}'.format(dataset))
    #MR-PETSC tuned
    start = timeit.default_timer()
    train_filename = os.path.join(dir, dataset) + '/' + dataset + '_TRAIN.ts' 
    test_filename = os.path.join(dir, dataset) + '/' + dataset + '_TEST.ts'     
    s = run_mr_petsc_parameter_optimisation(train_filename, test_filename)
    stop = timeit.default_timer()
    print('MR-PETSC-TUNED-VALIDATION: {}: {} : {}'.format(dataset,stop-start, s))
    