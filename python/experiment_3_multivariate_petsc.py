#Table X show the mean error and standard deviation +runtime? after 10 runs 
#on 20 datasets from the UCR archive where we compare PETSC, PETSC-SOFT, PETSC-DISC and MR-PETSC.
import os, timeit, subprocess
import numpy as np
import pandas as pd
from sktime.utils.data_io import load_from_tsfile_to_dataframe
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
from common import *
import common

dir = '../data/Multivariate_ts/'
selection = ['ArticularyWordRecognition',
'AtrialFibrillation',
'BasicMotions',
'CharacterTrajectories',
'Cricket',
#'DuckDuckGeese', #300mb
'ERing',
'Epilepsy',
'EthanolConcentration',
#'FaceDetection',  #800mb
'FingerMovements',
'HandMovementDirection',
#'Handwriting',
#'Heartbeat', -> 80mb
#'InsectWingbeat', -> to big 600mb
#'JapaneseVowels', -> |S|=8
'LSST',
'Libras',
#'MotorImagery', -> to big 530mb
'NATOPS',
#'PhonemeSpectra',
'RacketSports',
'SelfRegulationSCP1',
'SelfRegulationSCP2',
#'SpokenArabicDigits',
'StandWalkJump',
'UWaveGestureLibrary',
'EigenWorms'] #300mb
#'PenDigits']  # -> |S|= 8
#'PEMS-SF', 420mb


selection_plus = selection + ['PEMS-SF','MotorImagery', 'FaceDetection', 'DuckDuckGeese', 'InsectWingbeat']
selection_TODO = ['SelfRegulationSCP2','StandWalkJump', 'UWaveGestureLibrary','EigenWorms'] #300mb
selection = selection_TODO 

#print stats
print_stats = True
print(len(selection))
for dataset in selection:
    train_filename = os.path.join(dir, dataset) + '/' + dataset + '_TRAIN.ts' 
    test_filename = os.path.join(dir, dataset) + '/' + dataset + '_TEST.ts'
    print('filesize: {:.1f}mb'.format((os.path.getsize(train_filename) + os.path.getsize(test_filename))/1000000.0))
    if print_stats:
        train_x, train_y = load_from_tsfile_to_dataframe(train_filename)
        test_x, test_y = load_from_tsfile_to_dataframe(test_filename)
        labels = set()
        for label in train_y:
            labels.add(label)
        for label in test_y:
            labels.add(label)
        min_len = 1000
        max_len = 0
        for i in range(0, train_y.shape[0]):
            ts_len = train_x['dim_0'].iloc[i].to_numpy().shape[0]
            min_len = min(ts_len, min_len)
            max_len = max(ts_len, max_len)    
        nr_of_dimensions = 0
        #copy header ts to memory
        lines = []
        f = open(train_filename, 'r')
        nr_of_dimensions = -1
        for line in f:
            if line.startswith('@dimensions'):
                nr_of_dimensions = int(line[len('@dimensions '):])
        f.close()
        print('\emph{{{}}} & {} & {} & {} & {} & {}'.format(dataset, len(labels),
                    train_x.shape[0], test_x.shape[0], nr_of_dimensions, min_len if min_len == max_len else '{}/{}'.format(min_len,max_len)))

# def window_limits(train_x, train_y):
#     nr_series = train_y.shape[0]
#     minlen = 10000000
#     for i in range(0, nr_series):
#         len_i = train_x['dim_0'].iloc[i].to_numpy().shape[0]
#         minlen = min(minlen, len_i)
#     mini_length_search = int(minlen * 0.1)  # TODO: also for other datasets
#     maxi_length_search = minlen
#     if mini_length_search < 3: 
#         mini_length_search = 3
#     return mini_length_search, maxi_length_search
# 
# def random_search_mr_petsc_multivariate(train_filename, test_filename, iter=100):
#     print(">>MR-PETSC-MULTIVARIATE parameter optimisation")
#     # compute minsize
#     train_x, train_y = load_from_tsfile_to_dataframe(train_filename)
#     nr_series = train_y.shape[0]
#     minlen = 10000000
#     for i in range(0, nr_series):
#         len_i = train_x['dim_0'].iloc[i].to_numpy().shape[0]
#         minlen = min(minlen, len_i)
#     # random search    
#     # (train_filename, test_filename, w, alphabet, k, min_len_rel, rdur):
#     best_err, best_params = random_search_multi_threaded(mr_petsc_multivariate_rel, {
#                             'train_filename': train_filename, 'test_filename': test_filename,
#                             # preprocessing dynamic
#                             'w': list(range(5, 31)),
#                             'alphabet': list(range(3, 13)),
#                             # mining fixed
#                             'k': list(range(500, 1500)),
#                             'min_len_rel': [0.1, 0.2, 0.3, 0.4],
#                             'rdur': [1.0, 1.1, 1.2, 1.5]},
#                             check=lambda p: int(p['min_len_rel'] * p['w']) >= 3,
#                             max_iter=iter)
#     return best_params



# def run_mr_petsc_multivariate_parameter_optimisation(train_filename, test_filename, iter=100):
#     # load data for training
#     train_x, train_y = load_from_tsfile_to_dataframe(train_filename)
#     test_x, test_y = load_from_tsfile_to_dataframe(test_filename)
#     
#     # Parameter optimalisation
#     train_valid_x, test_valid_x, train_valid_y, test_valid_y = train_test_split(train_x, train_y, test_size=0.33, random_state=42)
#     TODO: BUG HERE
#
#     fname_validation_train = train_filename + '_validation_train.ts'
#     fname_validation_test = train_filename + '_validation_test.ts'
#     copy_ts_mv_single_file(train_filename, fname_validation_train, train_valid_x, train_valid_y)
#     copy_ts_mv_single_file(train_filename, fname_validation_test, test_valid_x, test_valid_y)
#     # search:
#     best_params = random_search_mr_petsc_multivariate(fname_validation_train, fname_validation_test, iter=iter)
#     
#     # Run PETSC outer loop
#     err = mr_petsc_multivariate(train_filename, test_filename, best_params['w'], best_params['alphabet'],
#                best_params['k'], int(best_params['min_len_rel'] * best_params['w']), best_params['rdur'])
#     print('MR-PETSC-MULTIVARIATE error on {:} is {:.3f} with parameters {:}'.format(os.path.basename(train_filename), err, best_params))
#     return err



run = True  
default = True
if run:
    common.DEFAULT_MAX_HEAP_SIZE = '-Xmx32G'
    if default:
        print("MR-PETSC-MULTIVARIATE default params")
        
       

        dct_results = {}
        for dataset in selection:
            train_filename = os.path.join(dir, dataset) + '/' + dataset + '_TRAIN.ts' 
            test_filename = os.path.join(dir, dataset) + '/' + dataset + '_TEST.ts' 
            err = mr_petsc_multivariate(train_filename, test_filename, w=15, alphabet=4, k=200, min_len=5, rdur=1.1, stride=10) #for ['SelfRegulationSCP2','StandWalkJump', 'UWaveGestureLibrary','EigenWorms']
            dct_results[dataset] = err
        print(dct_results)
        
    
    dct_results_folded = {}
    for dataset in selection: #selection_saxvsm:
        dct_results_folded[dataset] = {}
       
    print("MR-PETSC-MULTIVARIATE ramdom search params") 
    folds = 1
    for i in range(0,folds): 
        dct_results = {}
        for dataset in selection:
            if dataset == 'EigenWorms':
                continue
            train_filename = os.path.join(dir, dataset) + '/' + dataset + '_TRAIN.ts' 
            test_filename = os.path.join(dir, dataset) + '/' + dataset + '_TEST.ts' 
            #err = run_mr_petsc_multivariate_parameter_optimisation(train_filename, test_filename)
            #try alphabet: 4/8, rdur=1.0/1.1 and k/100
            best_err = 1.0
            stride = 10 #for ['SelfRegulationSCP2','StandWalkJump', 'UWaveGestureLibrary','EigenWorms']
            best_params = None
            for k in [100,250,500]:
                for alphabet in [4,8,12]:
                    for rdur in [1.0,1.1,1.5]:
                        for w in [10,15,20]:
                                if k == 250 and alphabet==4 and rdur==1.1 and w == 15:
                                    print("X ", end='') 
                                err = mr_petsc_multivariate(train_filename, test_filename, w,alphabet,k,min_len=w//3,rdur=rdur, stride=stride)
                                if err < best_err:
                                    best_err = err
                                    best_params = (w,alphabet,k,rdur)
            print("Error: {}. Best parameters: {}".format(best_err,best_params))
            dct_results[dataset] = best_err
            dct_results_folded[dataset][fold] = best_err
        print(dct_results)
    print(dct_results_folded)
      
