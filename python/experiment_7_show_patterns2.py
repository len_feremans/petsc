import timeit, os, sys, collections, math
from sktime.utils.data_io import load_from_tsfile_to_dataframe
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from common import *
from utils import *
from distutils.tests import support
from matplotlib import cm
from matplotlib.collections import LineCollection
from matplotlib.colors import ListedColormap, BoundaryNorm

def match_seq_pattern(pattern, window_ts, rdur):
    for i in range(0, len(window_ts) - len(pattern) +1):
        matching_pattern_idx = 0
        max_len = int(rdur * len(pattern))
        match = False
        first = i
        for j in range(0, max_len): #rdur is 1
            if i+j >= len(window_ts):
                break
            if window_ts[i+j] == pattern[matching_pattern_idx]:
                matching_pattern_idx += 1
                if matching_pattern_idx == 1:
                    first = i+j
                if matching_pattern_idx == len(pattern):
                    match = True
                    break
        if match:
            return first
    return -1

#small test
i = match_seq_pattern([0,5,5,1],[2,2,3,4,0,5,5,1], 1.0)
print(i)
i = match_seq_pattern([0,1],[2,2,3,4,0,5,5,1], 2.0)
print(i)
i = match_seq_pattern([0,1],[2,2,3,4,0,5,5,1], 1.5)
print(i)

def support_in_time_series(pattern, windows_ts, rdur):
    support = 0
    for window_ts in windows_ts:
        if match_seq_pattern(pattern, window_ts, rdur) != -1:
            support+=1
    return support

#compute attributions patterns in differents resolutions for final decision:
#current_ts_index: current id timeseries
#resolutions: all windows size, e.g. 100,50,25,12
#window_data_all: windows for each time series in each resolution, e.g. window_data_all[50][10] contains all windows for time series with idx=10 in resolution=50
#timeseries_indices_all: offsets that map time series index to window_data, e.g. timeseries_indices_all[50][10] -> 999.. this means windows_data_all[50][10] is for time series with idx 999
#patterns: all patterns. each pattern is a 4-tuple with (pattern, support, window, coefficient)
def attribution_patterns_decision(current_ts_index, windows_first_ts, patterns, window, w, rdur, length_timeseries):
    attribution = [0.0 for i in range(0,length_timeseries)]
    #compute visual matching
    PAA_factor = window / float(w) #e.g. if segment 48 and w = 12 -> each PAA symbol is average over 4 values
    #for each pattern in current resolution
    emb = []
    for pattern in patterns:
         support = support_in_time_series(pattern[0], windows_first_ts, rdur)
         #for each window in current resolution
         for window_start, window_ts in enumerate(windows_first_ts):
            match_offset = match_seq_pattern(pattern[0], window_ts, rdur)
            if match_offset !=-1:
                #get position in continuous signal
                start_continuous = int(window_start + match_offset * PAA_factor)
                end_continuous = int(start_continuous + len(pattern[0]) * PAA_factor)
                #get value (Used to be divided by MAXI coefficient)
                coefficient = pattern[3]
                value_segment = coefficient / float(support)  #divide coefficients times support
                #divide by length for resolution
                value_segment = value_segment / float(end_continuous - start_continuous)
                for i in range(start_continuous, end_continuous):
                    attribution[i] += value_segment
         emb.append(support)
    print(emb)
    return attribution   
    
def paint_attribution(ax, label_str, attribution, timeseries_index, X_test, y_test, y_pred, unique_labels, ylim):
    print(attribution)
    #render 
    label = int(y_test[timeseries_index])
    color_label = (0.3,0.3,0.3)
    ts = X_test['dim_0'].iloc[timeseries_index].to_numpy().tolist()
    #axes[0].plot(ts, color=color_label, label='Time series')
    # make line collection
    segments = [[(idx,ts[idx]),(idx+1,ts[idx+1])] for idx in range(0,len(ts)-1)]
    #if sum attribution patterns is positive: red, else blue
    c = [(1.0,0.0,0.0,0.9) if attribution[idx] <= 0 else (0.0,0.0,1.0,0.9)  for idx in range(0,len(ts)-1)]
    #width line is related to attribution
    max_attribution = max([abs(v) for v in attribution])
    if max_attribution == 0.0:
        max_attribution = 0.0001
    widths = [abs(attribution[idx])/max_attribution*5 for idx in range(0,len(ts)-1)]
    lc = LineCollection(segments, colors = c, linewidths=widths)
    ax.plot(ts, color=color_label, lw=1)
    ax.add_collection(lc)
    ax.set_xlabel(label_str, fontsize=20,color=(0,0,0),alpha=0.9)
    ax.autoscale()
    ax.set_ylim(ylim)
    ax.set_ymargin(0.1)
    ax.spines['right'].set_visible(False)
    ax.spines['top'].set_visible(False)
    #setup_axes(ax)
    #ax.get_xaxis().set_visible(True)
    #ax.get_xaxis().set_ticks([])
    
def run_mr_petsc_and_show_attribution(train_filename, test_filename, w, alphabet, k, min_len, rdur, class_labels_str):
    X_train, y_train = load_from_tsfile_to_dataframe(train_filename) 
    X_test, y_test = load_from_tsfile_to_dataframe(test_filename)    
    nr_series = y_train.shape[0]
    length_timeseries = min([X_train['dim_0'].iloc[i].to_numpy().shape[0] for i in range(0, nr_series)])
    unique_labels = sorted(list(set(y_test.tolist())))
    print("Labels :{}".format(unique_labels))
    # embedding
    embeddings_train = []
    embeddings_test = []
    patterns_all = []
    windows = []
    window_data_all = []
    timeseriesidx_all = []
    embedding_train_fname = '../temp/mr_petsc_embedding_train.txt' + str(uuid.uuid1())
    embedding_test_fname = '../temp/mr_petsc_embedding_test.txt' + str(uuid.uuid1())
    window = length_timeseries
    while window > w:
        pattern_file = '../temp/mr_petsc_patterns' + str(window) + '.txt'
        output_d_train = '../temp/mr_petsc_windows_train' + str(window) + '.txt'
        output_d_test = '../temp/mr_petsc_windows_test' + str(window) + '.txt'
        cmd = ['java', '-Xmx8G', '-cp', CLASSPATH,
                            'be.uantwerpen.mining.MineTopKSequentialPatterns',
                             '-input', train_filename, '-input_test', test_filename,
                            # preprocessing
                            '-paa_win', str(w), '-bins', str(alphabet),
                            # constraints
                            '-min_size', str(min_len),
                            '-max_size', str(w),
                            '-duration', str(rdur),
                            # mining
                            '-window', str(window),
                            '-k', str(k),
                            # output
                            '-output', embedding_train_fname, '-output_test', embedding_test_fname,
                            # viz
                            '-output_patterns', pattern_file,
                            '-output_discretisation_train', output_d_train,
                            '-output_discretisation_test', output_d_test]
        run_cmd(cmd, embedding_train_fname)
        X_embtrain = pd.read_csv(embedding_train_fname, header=None) 
        X_embtest = pd.read_csv(embedding_test_fname, header=None) 
        patterns_window = parse_patterns(pattern_file)
        #append patterns
        patterns_all.extend([[pattern, support, window] for pattern,support in patterns_window])
        for i in range(0,5):
            print('{}: {}'.format(patterns_window[i][0],patterns_window[i][1]))
        #append embedding vectors
        embeddings_train.append(X_embtrain)
        embeddings_test.append(X_embtest)
        windows.append(window)
        #append SAX representation/windows in different resolutions
        #e.g. 0 0 0 1: 0
        #     0 0 1 3: 0
        #.... where these are first two windows of time series with idx 0
        window_data, timeseriesidx = parse_discrete_file_with_labels(output_d_test)
        window_data_all.append(window_data)
        timeseriesidx_all.append(timeseriesidx)
        #divide
        window = window // 2
    #concat
    train_concat = pd.concat(embeddings_train, axis=1)
    print(train_concat.head())
    test_concat = pd.concat(embeddings_test, axis=1)
   
    #scale
    scaler = StandardScaler()
    scaler.fit(train_concat)
    train_concat_s = scaler.transform(train_concat)
    test_concat_s = scaler.transform(test_concat)
    #train_concat_s = train_concat
    #test_concat_s = test_concat
    #classify
    clf = SGDClassifier(max_iter=1000, tol=1e-3, penalty='elasticnet', n_jobs=-1, verbose=1, loss='log', fit_intercept=False)
    clf.fit(train_concat_s, y_train)
    #print results
    y_pred = clf.predict(test_concat_s)
    err = 1.0 - accuracy_score(y_test, y_pred)
    print("MR-PETSC(f={}, (w={}, alpha={}), (k={}, min_len={}, rdur={}) error is {:.4f}".format(os.path.basename(train_filename), w, alphabet, k, min_len, rdur, err))
    
    #display attribution
    coefficients = clf.coef_.tolist()[0]
    print('intercept:{} coeff:{}'.format(clf.intercept_,coefficients))
        
    maxi = max([abs(coeff) for coeff in coefficients])
    for idx, pattern in enumerate(patterns_all):
        patterns_all[idx] = pattern + [coefficients[idx]] #before contains: pattern, support, window
    for window in windows:
        print("----- window: {}".format(window))
        patterns_with_window = filter(lambda p: p[2] == window, patterns_all)
        srted =  sorted(patterns_with_window, key=lambda p: abs(p[3]), reverse=True)
        for pattern in srted[0:min(10,len(srted))]:
            print("{:.3f} {} support={:.3f} window={}".format(pattern[3]/maxi, 
                                                   ''.join([chr(97 + el) for el in pattern[0]]), 
                                                   pattern[1] / float(nr_series * (length_timeseries - pattern[2] +1)), pattern[2]))
    #print first test example:
    print("First test:{}".format(['{:.3f}'.format(x) for x in X_test['dim_0'].iloc[i].to_numpy().tolist()]))
    print("Label: true: {}, predicted:{}".format(y_test[0], y_pred[0]))
    print("First embedding:{}".format(['{:.3f}'.format(x) for x in test_concat.iloc[0].to_numpy().tolist()]))
    print("First embedding (scaled) :{}".format(['{:.3f}'.format(x) for x in test_concat_s[0].tolist()]))
    
    #select first 5 timeseries with label A and first 5 timeseries with label B
    fig, axes = plt.subplots(nrows=1, ncols=2, figsize=(10, 3))
    indices = []
    for i in range(0,len(y_test)):
        if int(y_test[i]) == int(unique_labels[0]): 
            indices.append(i)
        if len(indices) == 1:
            break
    for i in range(0,len(y_test)):
        if int(y_test[i]) == int(unique_labels[1]):
            indices.append(i)
        if len(indices) == 2:
            break       
    ylim = [0,0]
    for i, current_ts_index in enumerate(indices):
        ts= ts = X_test['dim_0'].iloc[current_ts_index].to_numpy().tolist()
        ylim[0] = min(min(ts),ylim[0])
        ylim[1] = max(max(ts),ylim[1])
    for i, current_ts_index in enumerate(indices):
        print("time series {}: label{}: predicted{}".format(current_ts_index,y_test[current_ts_index], y_pred[current_ts_index]))
        attribution_all = [0.0 for i in range(0,length_timeseries)]
        for resolution_idx, window in enumerate(windows):
            print("   window {}".format(window))
            #get windows for current time series in resolution
            window_data_in_resolution = window_data_all[resolution_idx]
            timeseries_indices = timeseriesidx_all[resolution_idx]
            windows_first_ts = [window_data_in_resolution[window_index] for window_index, ts_index in enumerate(timeseries_indices) if ts_index == current_ts_index]
            #get patterns in resolution
            patterns_in_resolution = list(filter(lambda p: p[2] == window, patterns_all))
            #compute attribution
            attribution =  attribution_patterns_decision(current_ts_index, windows_first_ts, patterns_in_resolution, window, w, rdur, length_timeseries)
            #sum of different resolutions
            for k in range(0,length_timeseries):
                attribution_all[k] += attribution[k]
            print("    attribution: mean: {:.3f} {}".format(np.mean(attribution), attribution))
        #paint
        print("    attribution all: mean: {:.3f} {}".format(np.mean(attribution_all), attribution_all))
        if i == 0:
            ax = axes[0]
            label = class_labels_str[0]
        else:
            ax = axes[1]
            label = class_labels_str[1]
        paint_attribution(ax, label, attribution_all, current_ts_index, X_test, y_test, y_pred, unique_labels, ylim)
    plt.tight_layout()
    #plt.show()
    plt.savefig("../temp/attribution-{}-patterns.pdf".format(os.path.basename(train_filename)), dpi=300)
    print('saved ' + "../temp/attribution-{}-patterns.pdf".format(os.path.basename(train_filename)))
    plt.close()
    return err

#Gunpoint
train_filename = '../data/Univariate_ts/GunPoint/GunPoint_TRAIN.ts'
test_filename = '../data/Univariate_ts/GunPoint/GunPoint_TEST.ts'
params = {'w':20, 'alphabet':12, 'k':250,  'min_len':6, 'rdur': 1.0}
run_mr_petsc_and_show_attribution(train_filename, test_filename,  
                                  params['w'], params['alphabet'], 
                                  params['k'], params['min_len'], params['rdur'], 
                                  ['Gun','Point'])

#ECG200
train_filename = '../data/Univariate_ts/ECG200/ECG200_TRAIN.ts'
test_filename = '../data/Univariate_ts/ECG200/ECG200_TEST.ts'
params_ECG200 = {'window': 46, 'w': 8, 'alphabet': 4, 'k': 50, 'min_len': 3, 'rdur': 1.0}
run_mr_petsc_and_show_attribution(train_filename, test_filename,  
                                  params_ECG200['w'], params_ECG200['alphabet'], 
                                  params_ECG200['k'], params_ECG200['min_len'], params_ECG200['rdur'],
                                  ['Normal','Ischemia'])

#Coffee
train_filename = '../data/Univariate_ts/Coffee/Coffee_TRAIN.ts'
test_filename = '../data/Univariate_ts/Coffee/Coffee_TEST.ts'
params_coffee = {'window': 33, 'w': 27, 'alphabet': 11, 'k': 646, 'min_len': 3, 'rdur': 1.0}
run_mr_petsc_and_show_attribution(train_filename, test_filename,  
                                  params_coffee['w'], params_coffee['alphabet'], 
                                  params_coffee['k'], params_coffee['min_len'], params_coffee['rdur'],
                                  ['Robusta','Aribica']) 

#Two patterns
train_filename = '../data/Univariate_ts/TwoPatterns/TwoPatterns_TRAIN.ts'
test_filename = '../data/Univariate_ts/TwoPatterns/TwoPatterns_TEST.ts'
params_coffee = {'window': 86, 'w': 12, 'alphabet': 3, 'k': 1085, 'min_len': 4, 'rdur': 1.1}
run_mr_petsc_and_show_attribution(train_filename, test_filename,  
                                  params_coffee['w'], params_coffee['alphabet'], 
                                  params_coffee['k'], params_coffee['min_len'], params_coffee['rdur'],
                                  ['Pattern A','Pattern B']) 

