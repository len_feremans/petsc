import numpy as np
import collections
import pandas as pd

# --------------------------- utils --------------------
def parse_int_csv_var_length(fname):
    f = open(fname,'r')
    data = []
    for line in f:
        ts = []
        if line.strip() == '':
            continue
        for num in line.strip().split(','):
            ts.append(int(num))
        data.append(np.array(ts))
        #if len(data) == 1:
        #    print(data[0])
    f.close()
    print("parsed " + fname + " length:" + str(len(data)) + " columns (first row):" + str(len(data[0])))
    return data


def parse_discrete_file_with_labels(fname):
    f = open(fname,'r')
    data = []
    labels = []
    for line in f:
        ts = []
        label = -1
        if line.strip() == '':
            continue
        for num in line.strip().split(','):
            if not ':' in num:
                ts.append(int(num))
            else:
                label = int(num.split(":")[1])
                ts.append(int(num.split(":")[0]))
        data.append(np.array(ts))
        labels.append(label)
        if len(data) == 1:
            print(data[0])
    f.close()
    print("parsed " + fname + " length:" + str(len(data)) + " columns (first row):" + str(len(data[0])))
    return data,labels

#e.g. input like: 0,0,0,0,0,0,0,0,0,0;5
def parse_patterns(fname):
    f = open(fname,'r')
    patterns = []
    for line in f:
        pattern = []
        support = -1
        if line.strip() == '':
            continue
        for num in line.strip().split(','):
            if ';' in num:
                pattern.append(int(num.split(';')[0]))
                support = int(num.split(';')[1])
            else:
                pattern.append(int(num))
        patterns.append([pattern, support])
    f.close()
    print("parsed " + fname + " length:" + str(len(patterns)) + " first: " + str(patterns[0]))
    return patterns

#e.g. input like: 0,0,0,0,0,0,0;2;59,60,61,62,63,64,65;75,76,77,78,79,80,81;109,110,111,112,113,114,115
#                 (pattern)    (ts) (first occurrence)    (next occurrence)     (another occurrence)
def parse_occurrences(fname):
    f = open(fname,'r')
    dct = collections.defaultdict(lambda: {})
    for line in f:
        try:
            if line.strip() == '':
                continue
            tokens = line.strip().split(';')
            pattern = tuple([int(t) for t in tokens[0].split(',')])
            ts = int(tokens[1])
            occurrences = []
            for i in range(2,len(tokens)):
                if tokens[i].strip() == '':
                    continue
                occurrence = [int(t) for t in tokens[i].split(',')] #e.g. last occurrences is ended by ;
                occurrences.append(occurrence)
            dct[pattern][ts] = occurrences
        except Exception as inst:
            print("Exception while parsing occurrences: " + str(inst) + " line: " + line)
        
    f.close()
    print("parsed " + fname + " length:" + str(len(dct)) + " first: " + str(dct[list(dct.keys())[0]]))
    return dct

def setup_axes(ax):
    ax.spines['top'].set_visible(False)
    ax.spines['bottom'].set_visible(False)
    ax.spines['left'].set_visible(False)
    ax.spines['right'].set_visible(False)
    ax.get_xaxis().set_visible(False)
    ax.get_yaxis().set_ticklabels([])
    ax.set_yticks([])

tableau20 = [(31, 119, 180), (174, 199, 232), (255, 127, 14), (255, 187, 120),  
             (44, 160, 44), (152, 223, 138), (214, 39, 40), (255, 152, 150),  
             (148, 103, 189), (197, 176, 213), (140, 86, 75), (196, 156, 148),  
             (227, 119, 194), (247, 182, 210), (127, 127, 127), (199, 199, 199),  
             (188, 189, 34), (219, 219, 141), (23, 190, 207), (158, 218, 229)]         
for i in range(len(tableau20)):  
    r, g, b = tableau20[i]  
    tableau20[i] = (r / 255., g / 255., b / 255.)
    
