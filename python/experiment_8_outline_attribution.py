#Additional experiment  for showing patterns in MPEG-7 images (such as Beetle/Fly or Chicken/Bird)
#Image data from http://www.ehu.eus/ccwintco/index.php/MPEG-7_Core_Experiment_CE-Shape-1_Test_Set._Benchmarking_image_database_for_shape_recognition_techniques

import PIL
from PIL import Image
from PIL import ImageShow
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import collections  as mc
from pylab import *
import cv2
import math
from os import listdir
from os.path import isfile, join
from sklearn.svm._libsvm import predict
from common import *
from utils import *
from sktime.utils.data_io import load_from_tsfile_to_dataframe

#PRE_PROCESSING: RADIAL DISTANCE METHOD 
#For image, extract longest contour using cv2 and compute distances from contour point to centre 
def get_contour(im, show=True):
    a = np.asarray(im)
    contours, hierarchy = cv2.findContours(a, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    #possible different contours
    print([c.shape[0] for c in contours])
    contours = list(contours)
    contours.sort(key=lambda c: c.shape[0], reverse=True)
    #center first contour
    M = cv2.moments(contours[0]) 
    cX = int(M["m10"] / M["m00"])
    cY = int(M["m01"] / M["m00"])
    #show  contour
    if show:
        cim = np.zeros_like(a)
        cv2.drawContours(cim, [contours[0]], -1, 255, 1)
        plt.matshow(cim, cmap=plt.cm.gray)
        plt.plot([contours[0][0][0][0]],[[contours[0][0][0][1]]], "-o")
        plt.plot([cX],[cY],"-o")
        plt.show()
    return (cX,cY), [(point[0][0], point[0][1]) for point in contours[0]]


#radial distance method (see https://izbicki.me/blog/converting-images-into-time-series-for-data-mining)
def get_ts(center, contour):
    ts = []
    for point in contour:
       d = math.sqrt((point[0] - center[0])**2 + (point[1] - center[1]) **2)
       ts.append(d)
    ts =  np.array(ts)
    ts = (ts - np.mean(ts)) / np.std(ts) #z-norm
    return ts

def get_all_ts(files, show=True):
    contour_all = []
    ts_all = []
    for file in files:
        im = Image.open(file)
        #PIL.ImageShow.show(im)
        center, contour = get_contour(im, show=False)
        #print(center, contour)
        contour_all.append(contour)
        ts = get_ts(center, contour)
        ts_all.append(ts)
        if show:
            plt.figure(figsize=(10,3))
            plt.plot(list(range(0, len(ts))), ts)
            plt.title(file)
            plt.show(block=False)
    if show:
        plt.show()
    return ts_all, contour_all

#Make sure all time series / contours are of equal length using sampling
def sample(ts, length=512):
    step = len(ts)/length
    res = []
    a = 0 #bresenham interpolation
    while a < len(ts):
        if len(res) == length:
            break
        res.append(ts[int(math.floor(a))])
        a = a + step
    return res

#Save to sktime ts format for PETSC
def save_to_ts(ts_all, ts_all2, ofile):
    print(f'save_to_ts: {len(ts_all)} / {len(ts_all2)} -> {ofile}')
    data = '''@problemName BeetleFly
@timeStamps false
@missing false
@univariate true
@equalLength true
@seriesLength 512
@classLabel true 1 2
@data
'''
    for ts in ts_all:
        ts = sample(ts)
        data = data + ','.join(['{:.9f}'.format(x) for x in ts]) + ':1\n'
    for ts in ts_all2:
        ts = sample(ts)
        data = data + ','.join(['{:.9f}'.format(x) for x in ts]) + ':2\n'
    f = open(ofile,'w')
    f.write(data)
    f.close()
    print(f"saved {ofile}")

#Pattern mining

def print_vec(label, vec):
    print("{}: (len={}) {}".format(label, len(vec), [float('{:.3f}'.format(x)) for x in vec]))

#Math occurrences in python for rendering patterns attribution in original time series (and outline)
def match_seq_pattern(pattern, window_ts, rdur):
    for i in range(0, len(window_ts) - len(pattern) +1):
        matching_pattern_idx = 0
        max_len = int(rdur * len(pattern))
        match = False
        first = i
        for j in range(0, max_len): #rdur is 1
            if i+j >= len(window_ts):
                break
            if window_ts[i+j] == pattern[matching_pattern_idx]:
                matching_pattern_idx += 1
                if matching_pattern_idx == 1:
                    first = i+j
                if matching_pattern_idx == len(pattern):
                    match = True
                    break
        if match:
            return first
    return -1

#if rdur=1, use this (default now)
def match_seq_pattern_faster(pattern, window_ts):
    window_ts = window_ts.tolist()
    for i in range(0, len(window_ts) - len(pattern) +1):
        matching_pattern_idx = 0
        max_len = len(pattern)
        if window_ts[i:i+max_len] == pattern:
            return i
    return -1


#small test
i = match_seq_pattern([0,5,5,1],[2,2,3,4,0,5,5,1], 1.0)
print(i)
i = match_seq_pattern([0,1],[2,2,3,4,0,5,5,1], 2.0)
print(i)
i = match_seq_pattern([0,1],[2,2,3,4,0,5,5,1], 1.5)
print(i)

#Using rdur
def support_in_time_series(pattern, windows_ts, rdur):
    support = 0
    for window_ts in windows_ts:
        if match_seq_pattern(pattern, window_ts, rdur) != -1:
            support+=1
    return support

#Assuming rdur=1
#Support = number of windows matching
def support_in_time_series_fast(pattern, windows_ts):
    support = 0
    for window_ts in windows_ts:
        if match_seq_pattern_faster(pattern, window_ts) != -1:
            support+=1
    return support

#Support = number of time series matching
def support_in_time_series_fast_instance(pattern, windows_ts, timeseriesidx):
    support = set()
    for window_ts, ts_idx in zip(windows_ts,timeseriesidx):
        if match_seq_pattern_faster(pattern, window_ts) != -1:
           support.add(ts_idx)
    return len(support)

#Jaccard similarity (based on occurrences in each window in entire dataset)
def jaccard(occ1, occ2):
    inter = occ1.intersection(occ2)
    d = float(len(occ1) + len(occ2) - len(inter))
    jaccard = len(inter) / d if d != 0 else 0
    return jaccard
  
#rdur ignored, since using   match_seq_pattern_faster
def get_occurrences(pattern, window_data, rdur):
    occ = set()
    for idx, window_ts in enumerate(window_data):
        #if match_seq_pattern(pattern[0], window_ts, rdur) != -1:
        if match_seq_pattern_faster(pattern[0], window_ts) != -1:
            occ.add(idx)
    return occ

#remove overlapping patterns, i.e. patterns where jaccard > jaccard_t
def remove_overlapping(patterns_with_occ, jaccard_t):
    overlapping = []
    for idx in range(0,len(patterns_with_occ)):
        for idx2 in range(idx+1, len(patterns_with_occ)):
           j = jaccard(patterns_with_occ[idx][3], patterns_with_occ[idx2][3])
           if j > jaccard_t:
                #print("overlapping " + str(patterns_with_occ[idx][0]) + "," + str(patterns_with_occ[idx2][0]))
                overlapping.append(idx)
                break
    print("Number of patterns: " + str(len(patterns_with_occ)))
    overlapping = sorted(list(set(overlapping)),reverse=True)
    for idx in overlapping:
        del patterns_with_occ[idx]
    print("Number of non-overlapping patterns: " + str(len(patterns_with_occ)))
    return patterns_with_occ, overlapping

#Print some stats after running gradient descent
def report_coefficients_and_patterns(coefficients, patterns, nr_of_windows):
    print_vec("coefficients", coefficients)
    print(f'Number of non-zero coefficients: {(coefficients!=0).sum()}')
    print(f'Number of almost-zero <0.001 coefficients: {(np.abs(coefficients)<0.001).sum() - (coefficients==0).sum()}')
    print(f'Number of positive/negative coefficients (>+-0.001): {(coefficients>0.001).sum()} / {(coefficients<-0.001).sum()}')
    for idx, p in enumerate(patterns):
        if isinstance(p,int):
            print(f'error pattern {idx} == {p} is int')
    srted =  sorted(patterns, key=lambda p: abs(p[2]), reverse=True) #sort on coefficient
    for pattern in srted[0:min(10,len(srted))]:
        print("{:.3f} {} support={:.3f}".format(pattern[2], 
                                                #''.join([chr(97 + el) for el in pattern[0]]), 
                                                pattern[0],
                                                pattern[1] / nr_of_windows))
        
        
        


## ATTRIBUTION RENDERING

#Renders single image / time series from test set
def render_attribution_single_ts(fig, subplot_id, ts_idx, window_data_test, timeseriesidx_test, patterns,  X_test, y_test, y_pred, test_contours, window, w):
    '''
    @fig: figure for subplots
    @subplot_id: if for subplot
    @ts_idx: id/offset (in test set) of instance
    @window_data_test: all windows for all time series
    @timeseriesidx_test: same dimensions as window_data_test and contains original instance id for each time series
    @patterns: Normally, non-redundant set of patterns. Each patterns contains [symbols, support-windows, weight-model, occurrences-all-windows, supportInstanceClassA, supportInstanceClass]
    @X_test: original time series
    @y_test: original labels
    @y_pred: predicted labels (in case of Beetle/Fly: 1 = beetle and 2 is Fly
    @test_contours: contours corresponding with original time series
    @window: window parameter for PETSC
    @w: SAX word size for PETSC
    '''
    print("render_attribution_single_ts")
    PAA_factor = window / float(w) #e.g. window=64 and PAA_FACTOR =33
    ts_data =  X_test['dim_0'].iloc[ts_idx].to_numpy()
    contour = test_contours[ts_idx]
    label = y_test[ts_idx]
    predicted_label = y_pred[ts_idx]
    label_str = 'beetle' if label == '1' else 'fly'
    windows = [window_data_test[window_index] for window_index, ts_index in enumerate(timeseriesidx_test) if ts_index == ts_idx]
    selected_patterns = [pattern for pattern in patterns if pattern[2] != 0] 
                                         
    #compute attribution for each pattern in each selected time series
    ts_attributions = [[0.0 for i in range(0, ts_data.shape[0])] for pattern in selected_patterns]
    for pattern_idx, pattern in enumerate(selected_patterns):
        first = True
        support = support_in_time_series_fast(pattern[0], windows)
        for window_start, window_ts in enumerate(windows):
            match_offset = match_seq_pattern_faster(pattern[0], window_ts) #no rdur...
            if match_offset !=-1: 
                #get position in continuous signal
                start_continuous = int(window_start + match_offset * PAA_factor)
                end_continuous = int(start_continuous + len(pattern[0]) * PAA_factor)
                coefficient = pattern[2] 
                value_segment = coefficient / float(support)  #divide coefficients times support in current ts
                ts_attributions[pattern_idx][start_continuous:end_continuous] = ts_attributions[pattern_idx][start_continuous:end_continuous] + value_segment
                #if first:
                #    print(f'match window[{window_start}] {window} of pattern {pattern} and value is {value_segment} ({coefficient}/{support})')
                #    first = False
    #plot TS
    plot_ts = False
    if plot_ts:
        x = list(range(0,len(ts_data)))
        plt.figure(figsize=(20,3))
        plt.plot(x, ts_data, '-o')
        plt.plot(x, ts_attribution, '-x')
        plt.title(f'TS {ts_idx} Label {label_str}{"False Negative" if label != predicted_label else ""}')
        plt.show()
    
    #plot contour
    contour = sample(contour) #as done with original time series
    x = list(range(0,len(ts_data)))
    ax = fig.add_subplot(4,5,subplot_id)
    ax.set_aspect('equal', adjustable='box')
    lines = []
    #Min/max scale contour
    max_x = float(max([contour[i][0] for i in range(0,len(contour))]))
    max_y = float(max([contour[i][1] for i in range(0,len(contour))]))
    for i in range(0,len(contour)-1):
        c1 = contour[i] 
        c2 = contour[i+1] 
        lines.append([(c1[0]/max_x,c1[1]/max_y),(c2[0]/max_x,c2[1]/max_y)])
    lc = mc.LineCollection(lines, linewidths=2, color='darkgrey')
    ax.add_collection(lc)
    #show attribution (see https://stackoverflow.com/questions/22408237/named-colors-in-matplotlib)
    colors_blue = ['dodgerblue', 'lightsteelblue', 'deepskyblue', 'royalblue', 'navy', 'darkturquoise', 'c']
    colors_red = ['lightcoral', 'orangered', 'firebrick', 'tomato', 'chocolate', 'gold', 'orange']
    for pattern_idx, pattern in enumerate(selected_patterns):
        lines = []
        linewidths = []
        ts_attribution = ts_attributions[pattern_idx]
        color = None
        #Only show patterns per-class (or both in case prediction is off)
        #i.e. if weight < 0 -> weight for pattern is specific to Beetle class. 
        #If label != beetle or predictable label != beetle -> do not render 'beetle' patterns
        if pattern[2] < 0 and label != '1' and predicted_label !='1':
            continue
        #same for Fly, i.e. only show "fly" patterns for fly instances of where predictions are wrong
        if pattern[2] > 0 and label != '2' and predicted_label !='2':
            continue
        if pattern[2] < 0:
            color = colors_blue[pattern_idx % len(colors_blue)]
        elif pattern[2] > 0:
            color = colors_red[pattern_idx % len(colors_red)]
        for i in range(0,len(contour)-1):
            c1 = contour[i] 
            c2 = contour[i+1] 
            lines.append([(c1[0]/max_x,c1[1]/max_y),(c2[0]/max_x,c2[1]/max_y)])
            value_segment = ts_attribution[i]
            if value_segment !=0:
                linewidths.append(1 + abs(value_segment)*4) 
            else:
                linewidths.append(0) 
        lc2 = mc.LineCollection(lines, color=color, linewidths=linewidths, alpha=0.6)
        ax.add_collection(lc2)  
    color_outline = colors_blue[0]
    #show edge around each instance
    if label != '1':
        color_outline = colors_red[0]
    if label != predicted_label:
        color_outline = 'orange'
    print(label, color_outline)
    ax.set_axis_off()
    autoAxis = ax.axis()
    rec = Rectangle((autoAxis[0],autoAxis[2]),(autoAxis[1]-autoAxis[0]),(autoAxis[3]-autoAxis[2]),fill=False,lw=2,color=color_outline)
    rec = ax.add_patch(rec)
    rec.set_clip_on(False)
    #plt.xlabel('TS {}. Label {} {}'.format(ts_idx, label_str, 'False' if label != predicted_label else ''))
    #plt.show()
    print("end render_attribution_single_ts")
    return ax
           
def run_petsc_and_show_attribution(train_filename, test_filename, test_contours, window, w, alphabet, k, min_len, rdur, alpha, jaccard_t, class_labels_str):
    '''
    First run PETSC with parameters
    Run SGD for linear model with alpha
    Report accuracy
    Than remove patterns with zero weight 
    Than remove redundant patterns using Jaccard (threshold set to 0.5)
    Run SGD for linear model again with non-redundant patterns
    Report accuracy again
    Than show top-2o pattern attribution in outline image
    
    @train_filename: original filename in sktime ts format
    @test_contours: 2D contour points for each time series instance (in same order) as in test ts data
    @window/@w/@alphabet/k/min_len/rdur: parameters PETSC
    @alpha: for regularisation of SGD classifier (default 0.0001)
    @jaccard_t: threshold for non-redundant pattern filtering
    '''
    X_train, y_train = load_from_tsfile_to_dataframe(train_filename) 
    X_test, y_test = load_from_tsfile_to_dataframe(test_filename)    
    nr_series = y_train.shape[0]
    length_timeseries = min([X_train['dim_0'].iloc[i].to_numpy().shape[0] for i in range(0, nr_series)])
    unique_labels = sorted(list(set(y_test.tolist())))
    print("Labels :{}".format(unique_labels))
    
    #run petsc
    #save embedding and SAX representation (i.e. sliding windows)
    embedding_train_fname = '../temp/petsc_embedding_train.txt'
    embedding_test_fname = '../temp/petsc_embedding_test.txt'
    pattern_file = '../temp/petsc_patterns' + str(window) + '.txt'
    output_d_train = '../temp/mr_petsc_windows_train' + str(window) + '.txt'
    output_d_test = '../temp/mr_petsc_windows_test' + str(window) + '.txt'
    cmd = ['java', '-Xmx8G', '-cp', CLASSPATH,
                            'be.uantwerpen.mining.MineTopKSequentialPatterns',
                             '-input', train_filename, '-input_test', test_filename,
                            # preprocessing
                            '-paa_win', str(w), '-bins', str(alphabet),
                            # constraints
                            '-min_size', str(min_len),
                            '-max_size', str(w),
                            '-duration', str(rdur),
                            # mining
                            '-window', str(window),
                            '-k', str(k),
                            # output
                            '-output', embedding_train_fname, '-output_test', embedding_test_fname,
                            # viz
                            '-output_patterns', pattern_file,
                            '-output_discretisation_train', output_d_train,
                            '-output_discretisation_test', output_d_test]
    run_cmd(cmd, embedding_train_fname)
    X_embtrain = pd.read_csv(embedding_train_fname, header=None) 
    X_embtest = pd.read_csv(embedding_test_fname, header=None) 
    patterns = parse_patterns(pattern_file)
    for i in range(0,5):
        print('{}: {}'.format(patterns[i][0],patterns[i][1]))
    window_data_train, timeseriesidx_train = parse_discrete_file_with_labels(output_d_train)
    window_data_test, timeseriesidx_test = parse_discrete_file_with_labels(output_d_test) #ALSO BUG
    
    #Removed scaling, since for interpretation, z-normalising makes is more difficult, i.e. pattern occurs 8 times, and mean is 9, so negative weight,
    #but pattern occurs...
    #scaler = StandardScaler()
    #scaler.fit(X_embtrain)
    #train_concat_s = scaler.transform(X_embtrain)
    #test_concat_s = scaler.transform(X_embtest)
    train_concat_s = X_embtrain
    test_concat_s = X_embtest
    
    #Classify
    #Increased alpha to 1 (default is 0.0001), i.e. 1 / C ~ alpha_sgd * n_samples
    #see also https://stats.stackexchange.com/questions/216095/how-does-alpha-relate-to-c-in-scikit-learns-sgdclassifier
    clf = SGDClassifier(max_iter=1000, tol=1e-3, alpha=alpha, penalty='elasticnet', n_jobs=-1, verbose=1, loss='log', fit_intercept=False)
    clf.fit(train_concat_s, y_train)
    y_pred = clf.predict(test_concat_s)
    acc = accuracy_score(y_test, y_pred)
    print("PETSC(f={}, (w={}, alpha={}), (k={}, min_len={}, rdur={}) acc is {:.4f}".format(os.path.basename(train_filename), 
                                                                                           w, alphabet, k, min_len, rdur, acc))
    coefficients = np.array(clf.coef_.tolist()[0])
    coefficients = coefficients / coefficients.max() #max normalising coefficients
    nr_of_windows= float(nr_series * (length_timeseries - window +1))
    print(f'nr_of_window: {nr_of_windows} ')
    for idx, pattern in enumerate(patterns):
        patterns[idx] = pattern + [coefficients[idx]] 
    report_coefficients_and_patterns(coefficients, patterns, nr_of_windows)
    
    #find occurrences (slow since python loops)
    patterns_with_occ = []
    for idx, pattern in enumerate(patterns):
        if idx % 100 == 0:
            print("Find occurrences " + str(idx) + "/" + str(len(patterns)))
        #filter weights with 0 weight
        if abs(pattern[2]) > 0:
            occ = get_occurrences(pattern, window_data_train, rdur)
            pattern = pattern + [occ]
            patterns_with_occ.append(pattern)

            
    #remove redundant patterns: Jaccard is hard-coded to 0.5!
    patterns_with_occ, overlapping = remove_overlapping(patterns_with_occ, jaccard_t)
    
    #filtering redundant patterns in embedding
    remove_patterns_set = set()
    #overlapping: id does not match original id, since we remove zero-weight patterns, account with this here
    new_id_to_old_id = {}
    new_id = 0
    for idx in range(0, len(patterns)):
        pattern = patterns[idx]
        if abs(pattern[2]) > 0:
            new_id_to_old_id[new_id] = idx
            new_id +=1
        else:
            remove_patterns_set.add(idx)
    for new_id in overlapping:
        remove_patterns_set.add(new_id_to_old_id[new_id])
    keep = set(list(range(0,len(patterns)))) - remove_patterns_set
    print(f'keep: {len(keep)}')
    keep = np.array(list(keep))
    train_concat_s = train_concat_s.iloc[:,keep]
    test_concat_s = test_concat_s.iloc[:,keep]
    
    #run classifier again
    #Note: alpha=1 like before (default 0.0001)
    clf = SGDClassifier(max_iter=1000, tol=1e-3, alpha=alpha, penalty='elasticnet', n_jobs=-1, verbose=1, loss='log', fit_intercept=False)
    clf.fit(train_concat_s, y_train)
    y_pred = clf.predict(test_concat_s)
    acc = accuracy_score(y_test, y_pred)
    print("PETSC(f={}, (w={}, alpha={}), (k={}, min_len={}, rdur={}) acc is {:.4f}".format(os.path.basename(train_filename), 
                                                                                           w, alphabet, k, min_len, rdur, acc))
    coefficients = np.array(clf.coef_.tolist()[0])
    coefficients = coefficients / coefficients.max() #NEW: Normalize
    for idx, pattern_with_occ in enumerate(patterns_with_occ):
        pattern_with_occ[2] = coefficients[idx] #update coefficients
    report_coefficients_and_patterns(coefficients, patterns_with_occ, nr_of_windows)
    
      
    #print first test example:
    print_vec("First test time series",  X_test['dim_0'].iloc[i].to_numpy().tolist())
    print("Label: true: {}, predicted:{}".format(y_test[0], y_pred[0]))
    print_vec("First embedding:{}", X_embtest.iloc[0].to_numpy().tolist())
    print_vec("First embedding (filtered)", test_concat_s.iloc[0].tolist())
    print(f"Number of windows {len(window_data_test)} Number of windows per ts {len(window_data_test) / nr_series}")
    print_vec("First window", window_data_test[0])
    print(timeseriesidx_test)
    
    #select top-k most discriminative patterns:
    discrimi_patterns = []
    for pattern in patterns_with_occ:
        if pattern[2] == 0:
            continue
        supportA = support_in_time_series_fast_instance(pattern[0], window_data_test[0:len(window_data_test)//2], timeseriesidx_test[0:len(window_data_test)//2])
        supportB = support_in_time_series_fast_instance(pattern[0], window_data_test[len(window_data_test)//2:], timeseriesidx_test[len(window_data_test)//2:])
        pattern = pattern + [supportA] + [supportB] + [supportA-supportB]
        discrimi_patterns.append(pattern)
    #discrimi_patterns.sort(key=lambda p: abs(p[6]), reverse=True) #pattern is: pattern, support, weight, occ, supportA, supportB, supportA-supportB]
    discrimi_patterns.sort(key=lambda p: abs(p[2]), reverse=True) #pattern is: pattern, support, weight, occ, supportA, supportB, supportA-supportB]
    print('discriminative patterns')
    for pattern in discrimi_patterns:
        #del pattern[3] #delete occurrences
        print(pattern)
    discrimi_patterns = discrimi_patterns[0:20]
    
    #render outlines and pattern attribution
    fig=plt.figure(figsize=(20,10))
    for ts_idx in range(0,20,1):
        ax = render_attribution_single_ts(fig, ts_idx+1, ts_idx, window_data_test, timeseriesidx_test, discrimi_patterns,  X_test, y_test, y_pred, test_contours, window, w)
    plt.tight_layout()
    plt.show()
    
    #show legend seperate fig
    fig = plt.figure(figsize=(2,5))
    colors_blue = ['dodgerblue', 'lightsteelblue', 'deepskyblue', 'royalblue', 'navy', 'darkturquoise', 'c']
    colors_red = ['lightcoral', 'orangered', 'firebrick', 'tomato', 'chocolate', 'gold', 'orange']
    legends = []
    for pattern_idx, pattern in enumerate(discrimi_patterns):
        color = None
        labelp = None
        if pattern[2] < 0:
            color = colors_blue[pattern_idx % len(colors_blue)]
            labelp = f'P-{pattern_idx}: w={pattern[2]:.2f}, sup(Beetle)={pattern[4]}'
        elif pattern[2] > 0:
            color = colors_red[pattern_idx % len(colors_red)]
            labelp = f'P-{pattern_idx}: w={pattern[2]:.2f}, sup(Fly)={pattern[5]}'
        def make_proxy(color):
            return Line2D([0, 1], [0, 1], color=color, label=labelp)
        proxy = make_proxy(color)
        legends.append((proxy, labelp))
    fig.legend([proxy for proxy, label in legends],[label for proxy, label in legends])
    plt.tight_layout()
    plt.show()

if __name__ == '__main__': 
    image_dir = '/Users/lfereman/git2/pets_private/data/mpeg7shapeB/'
    onlyfiles = [join(image_dir, f) for f in listdir(image_dir) if isfile(join(image_dir, f))]
    #beetle_files = [file for file in onlyfiles if '/octopus' in file]
    #fly_files = [file for file in onlyfiles if '/turtle' in file]
    #beetle_files = [file for file in onlyfiles if '/bird' in file]
    #fly_files = [file for file in onlyfiles if '/chicken' in file]
    beetle_files = [file for file in onlyfiles if '/beetle' in file]
    fly_files = [file for file in onlyfiles if '/fly' in file]
    print(f'{len(beetle_files)} / {len(fly_files)}')

    #load data and pre-process images using radial distance method
    ts_all, contour_all = get_all_ts(beetle_files, show=False)
    ts_all2, contour_all2 = get_all_ts(fly_files, show=False)
    print(f'{len(ts_all)} / {len(contour_all)}')
    print(f'{len(ts_all2)} / {len(contour_all2)}')

    #create train/test split
    fraction = 0.5
    size = math.ceil(len(ts_all)*fraction)
    rem = len(ts_all) - size
    train_filename = './mybeetlyfly_TRAIN.ts'
    test_filename = './mybeetlyfly_TEST.ts'
    save_to_ts(ts_all[0:size],ts_all2[0:size], train_filename)
    save_to_ts(ts_all[rem:],ts_all2[rem:], test_filename)
    test_contours = contour_all[rem:] + contour_all2[rem:]


    #TIME SERIES CLASSIFICATION WITH PETSC
    #from experiment_7_show_patterns2 import run_mr_petsc_and_show_attribution
    run_petsc_and_show_attribution(train_filename, test_filename, test_contours, 
                                   window=64, w=30, alphabet=12, k=1000, min_len=8, rdur=1, alpha=1, jaccard_t=0.5, class_labels_str=['Beetle','Fly'])
    #run_petsc_and_show_attribution(train_filename, test_filename, test_contours, 
    #                               window=32, w=30, alphabet=12, k=1000, min_len=8, rdur=1, alpha=0.0001, jaccard_t=0.5, class_labels_str=['Beetle','Fly'])
    
    


