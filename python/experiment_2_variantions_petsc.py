#Table X show the mean error and standard deviation +runtime? after 10 runs 
#on 20 datasets from the UCR archive where we compare PETSC, PETSC-SOFT, PETSC-DISC and MR-PETSC.
import os, timeit, subprocess
import numpy as np
import pandas as pd
from sktime.utils.data_io import load_from_tsfile_to_dataframe
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
from common import *

    
dir = '../data/Univariate_ts/'
selection = ['UWaveGestureLibraryAll', 'Computers', 'BeetleFly', 'CBF', 'Mallat', 
             'Earthquakes', 'AllGestureWiimoteX', 'InsectEPGRegularTrain', 'FordA', 'DodgerLoopDay', 'ScreenType',
             'OSULeaf', 'Lightning7', 'InsectWingbeatSound', 'TwoPatterns']

selection_saxvsm = ['Adiac','Beef','CBF','Coffee','ECG200','FaceAll','FaceFour','Fish', 'GunPoint',
             'Lightning2', 'Lightning7', 'OliveOil', 'OSULeaf', 'SyntheticControl', 'SwedishLeaf', 'Trace',
             'TwoPatterns', 'Wafer','Yoga']

all = '''ACSF1
Adiac
AllGestureWiimoteX
AllGestureWiimoteY
AllGestureWiimoteZ
ArrowHead
Beef
BeetleFly
BirdChicken
BME
Car
CBF
Chinatown
ChlorineConcentration
CinCECGTorso
Coffee
Computers
CricketX
CricketY
CricketZ
Crop
DiatomSizeReduction
DistalPhalanxOutlineAgeGroup
DistalPhalanxOutlineCorrect
DistalPhalanxTW
DodgerLoopDay
DodgerLoopGame
DodgerLoopWeekend
Earthquakes
ECG200
ECG5000
ECGFiveDays
ElectricDevices
EOGHorizontalSignal
EOGVerticalSignal
EthanolLevel
FaceAll
FaceFour
FacesUCR
FiftyWords
Fish
FordA
FordB
FreezerRegularTrain
FreezerSmallTrain
Fungi
GestureMidAirD1
GestureMidAirD2
GestureMidAirD3
GesturePebbleZ1
GesturePebbleZ2
GunPoint
GunPointAgeSpan
GunPointMaleVersusFemale
GunPointOldVersusYoung
Ham
HandOutlines
Haptics
Herring
HouseTwenty
InlineSkate
InsectEPGRegularTrain
InsectEPGSmallTrain
InsectWingbeatSound
ItalyPowerDemand
LargeKitchenAppliances
Lightning2
Lightning7
Mallat
Meat
MedicalImages
MelbournePedestrian
MiddlePhalanxOutlineAgeGroup
MiddlePhalanxOutlineCorrect
MiddlePhalanxTW
MixedShapesRegularTrain
MixedShapesSmallTrain
MoteStrain
NonInvasiveFetalECGThorax1
NonInvasiveFetalECGThorax2
OliveOil
OSULeaf
PhalangesOutlinesCorrect
Phoneme
PickupGestureWiimoteZ
PigAirwayPressure
PigArtPressure
PigCVP
PLAID
Plane
PowerCons
ProximalPhalanxOutlineAgeGroup
ProximalPhalanxOutlineCorrect
ProximalPhalanxTW
RefrigerationDevices
Rock
ScreenType
SemgHandGenderCh2
SemgHandMovementCh2
SemgHandSubjectCh2
ShakeGestureWiimoteZ
ShapeletSim
ShapesAll
SmallKitchenAppliances
SmoothSubspace
SonyAIBORobotSurface1
SonyAIBORobotSurface2
StarLightCurves
Strawberry
SwedishLeaf
Symbols
SyntheticControl
ToeSegmentation1
ToeSegmentation2
Trace
TwoLeadECG
TwoPatterns
UMD
UWaveGestureLibraryAll
UWaveGestureLibraryX
UWaveGestureLibraryY
UWaveGestureLibraryZ
Wafer
Wine
WordSynonyms
Worms
WormsTwoClass
Yoga'''

selection_all = [line.strip() for line in all.split("\n")]


#print stats
selection_saxvsm = ['HandOutlines','StarLightCurves']
for dataset in selection_saxvsm:
    train_filename = os.path.join(dir, dataset) + '/' + dataset + '_TRAIN.ts' 
    test_filename = os.path.join(dir, dataset) + '/' + dataset + '_TEST.ts' 
    train_x, train_y = load_from_tsfile_to_dataframe(train_filename)
    test_x, test_y = load_from_tsfile_to_dataframe(test_filename)
    labels = set()
    for label in train_y:
        labels.add(label)
    for label in test_y:
        labels.add(label)
    min_len = 1000
    max_len = 0
    for i in range(0, train_y.shape[0]):
        ts_len = train_x['dim_0'].iloc[i].to_numpy().shape[0]
        min_len = min(ts_len, min_len)
        max_len = max(ts_len, max_len)    
    print('\emph{{{}}} & {} & {} & {} & {}'.format(dataset, len(labels),
                train_x.shape[0], test_x.shape[0], min_len if min_len == max_len else '{}/{}'.format(min_len,max_len)))
    
def run(selection):
    iter = 100
    folds = 5
    dct_results = {}
    for dataset in selection: #selection_saxvsm:
        dct_results[dataset] = {}
            
    for i in range(0,folds): #i.e. on fold takes about 1 day
        for dataset in selection: #selection_saxvsm:
            dct_results[dataset][i] = {}
        print(">>fold {}".format(i+1))
        for dataset in selection:
            train_filename = os.path.join(dir, dataset) + '/' + dataset + '_TRAIN.ts' 
            test_filename = os.path.join(dir, dataset) + '/' + dataset + '_TEST.ts' 
            err1 = run_petsc_classic_with_parameter_optimisation(train_filename, test_filename, iter)
            err2 = run_bag_of_patterns_with_parameter_optimisation(train_filename, test_filename, iter)
            err3 = run_mr_petsc_parameter_optimisation(train_filename, test_filename, iter)
            #err4 = run_mr_petsc_soft_parameter_optimisation(train_filename, test_filename, iter)
            err5 = run_petsc_disc_with_parameter_optimisation(train_filename, test_filename, iter)
            err6 = run_petsc_soft_with_parameter_optimisation(train_filename, test_filename, iter)
            dct_results[dataset][i]['petsc'] = err1
            dct_results[dataset][i]['bop'] = err2
            dct_results[dataset][i]['mr-petsc'] = err3
            #dct_results[dataset][i]['mr-petsc-soft'] = err4
            dct_results[dataset][i]['petsc-disc'] = err5
            dct_results[dataset][i]['petsc-soft'] = err6
            print(dct_results[dataset][i])
        print(dct_results)
    print(dct_results)

#run(['GunPoint'])
run(selection_saxvsm)
#run(selection_all)    

 