#1) plot 1:
#   - show 2 pairs of time series
#   - show pattern-based embedding
#   - show bag-of-patterns
#2) plot 2:
#   - visualisation of transaction database
#4) Plot 3:
#   - visualisation of patterns
from sktime.utils.load_data import load_from_tsfile_to_dataframe
import matplotlib.pyplot as plt
from matplotlib.patches import ConnectionPatch
import numpy as np
import pandas as pd
import collections
from saxpy.znorm import znorm
from saxpy.paa import paa
from saxpy.sax import ts_to_string
from saxpy.alphabet import cuts_for_asize
import subprocess
from utils import *
import sys
import os


 #compute min distance between each ts (and embedding) in-class, and out-class 
def stats(all_timeries, all_labels, current_ts_idx, current_label):
    in_class_dist = 1000000
    out_class_dist = 1000000
    for idx in range(0,len(all_timeries)):
        if idx == current_ts_idx:
            continue
        dist = np.linalg.norm(all_timeries[current_ts_idx]-all_timeries[idx]) #l2 norm    
        if all_labels[idx] == current_label:
             in_class_dist = min(in_class_dist,dist)
        else:
             out_class_dist = min(out_class_dist,dist)
    return (in_class_dist, out_class_dist)
 
def min_dist(all_timeries, current_ts_idx):
    min_dist = 1000000
    min_idx = -1
    for idx in range(0,len(all_timeries)):
        if idx == current_ts_idx:
            continue
        dist = np.linalg.norm(all_timeries[current_ts_idx]-all_timeries[idx]) #l2 norm    
        if dist < min_dist:
            min_idx = idx
            min_dist = dist
    return min_idx
           

def render_embedding(preprocessing_file,embedding_fname1, embedding_fname2, label1, label2, plot_fname, filter=True):
    #load data
    ts_discrete, ts_labels = (None,None)
    if preprocessing_file.endswith('.txt'):
        ts_discrete, ts_labels = parse_discrete_file_with_labels(preprocessing_file)
    if preprocessing_file.endswith('.ts'):
        ts_discrete, ts_labels = load_from_tsfile_to_dataframe(preprocessing_file) 
        #convert from dataframe to list of numpy arrays
        ts_discrete = [ts_discrete['dim_0'].iloc[i].to_numpy() for i in range(0, ts_labels.shape[0])]
        ts_labels = [int(ts_labels[i]) for  i in range(0,ts_labels.shape[0])] #label is string after loading by load_from_tsfile
    embedding1 = parse_int_csv_var_length(embedding_fname1)
    embedding2 =  parse_int_csv_var_length(embedding_fname2) 
    
    
    if filter:
        #filter data: show 2 instances for each label
        ts_indexes = []
        visited = set()
        for i in range(0,len(ts_labels)): 
            label = ts_labels[i]
            if label in visited:
                continue
            if label > 7: #added Len
                continue
            next_idx  = -1
            for j in range(i+1,len(ts_labels)):
                if  ts_labels[j]  == label: 
                    next_idx = j
                    break
            ts_indexes.append(i)
            ts_indexes.append(next_idx)
            visited.add(label)
        ts_discrete = [ts_discrete[idx] for idx in ts_indexes]
        ts_labels = [ts_labels[idx] for idx in ts_indexes]
        embedding1 = [embedding1[idx] for idx in ts_indexes]
        embedding2 = [embedding2[idx] for idx in ts_indexes]
        
        
    
    # ----------------------------------------- PLOT 1  ---------------------------------------- 
    fig, axes = plt.subplots(nrows=len(ts_discrete), ncols=3, figsize=(15, 8))
    for idx in range(0,len(ts_discrete)):
        label = int(ts_labels[idx])
        ts = ts_discrete[idx]
        in_class_dist, out_class_dist = stats(ts_discrete, ts_labels, idx, label)
        c = tableau20[label % len(tableau20)]
        label_plot = 'S' + str(idx+1) + " " #"(label: " + str(label) + ") " 
        #if out_class_dist < in_class_dist: 
        #    label_plot = '\n1-NN ED fails'
        # "min_dist: " + ('NOK' if out_class_dist < in_class_dist else '') +
        #           '{:.1f} (in)'.format(in_class_dist) + "," + '{:.1f} (out)'.format(out_class_dist))
        axes[idx,0].plot(ts, color=c, label=str(label))
        axes[idx,0].set_ylabel(label_plot, fontsize=8, rotation='horizontal', horizontalalignment='right')
        setup_axes(axes[idx,0])
        
    for idx in range(0,len(ts_discrete)):
        label = ts_labels[idx]
        in_class_dist, out_class_dist = stats(embedding1, ts_labels, idx, label)
        c = tableau20[label % len(tableau20)]
        #label_plot = (label1 + str(idx+1) + "(" + str(label) + ") " + 
        #             "\n min_dist: " + ('NOK' if out_class_dist < in_class_dist else '') +
        #             '{:.1f} (in)'.format(in_class_dist) + "," + '{:.1f} (out)'.format(out_class_dist))
        label_plot = '' 
        #if out_class_dist < in_class_dist: 
        #    label_plot = '\n1-NN ED fails'
        axes[idx,1].plot(embedding1[idx], color=c, label=str(label))
        axes[idx,1].set_ylabel(label_plot, fontsize=8, rotation='horizontal', horizontalalignment='right')
        setup_axes(axes[idx,1])
     
    for idx in range(0,len(ts_discrete)):
        label = ts_labels[idx]
        in_class_dist, out_class_dist = stats(embedding2, ts_labels, idx, label)
        c = tableau20[label % len(tableau20)]
        #label_plot = (label2 + str(idx+1) + "(" + str(label) + ") " + 
        #              "\n min_dist: " + ('NOK' if out_class_dist < in_class_dist else '') +
        #              '{:.1f} (in)'.format(in_class_dist) + "," + '{:.1f} (out)'.format(out_class_dist))
        label_plot = ''
        #if out_class_dist < in_class_dist: 
        #    label_plot = '\n1-NN ED fails'
        axes[idx,2].plot(embedding2[idx], color=c, label=str(label))
        axes[idx,2].set_ylabel(label_plot, fontsize=8, rotation='horizontal', horizontalalignment='right')
        setup_axes(axes[idx,2])
    
    for ax, col in zip(axes[0], ['Time series','PETSC embedding', 'BOP embedding']):
        ax.set_title(col)


    #show nearest neighbour 
    for embedding_idx, embedding in enumerate([ts_discrete, embedding1, embedding2]):
        for idx in range(0,len(ts_discrete)):
            label = int(ts_labels[idx])
            neighbour_idx_ed = min_dist(embedding, idx)
            c = 'green' #tableau20[label % len(tableau20)]
            av = embedding_idx
            offset_x = -250
            base_x = -100
            offset_y = 5
            if embedding_idx == 1:
                offset_x = -30
                base_x = -10
            if embedding_idx == 0:
                offset_x = -0.7 * label -2
                base_x = 0
                offset_y = 1.5
                c = (1.0,0,0,0.9)
            if abs(idx - neighbour_idx_ed) == 1:
                xy = (offset_x,offset_y)
                #xy = (0,1)
                con = ConnectionPatch(xyA=xy, xyB=(base_x,offset_y), coordsA="data", coordsB="data",
                          axesA= axes[idx,av], axesB= axes[idx,av], color=c, lw=2)
                axes[idx,av].add_artist(con)
                con = ConnectionPatch(xyA=xy, xyB=(base_x,offset_y), coordsA="data", coordsB="data",
                          axesA= axes[neighbour_idx_ed,av], axesB= axes[neighbour_idx_ed,av], color=c, lw=2)
                axes[neighbour_idx_ed,av].add_artist(con)
            else:
                xy = (offset_x*2,offset_y)
                #xy = (0,1)
                c = 'red'
                con = ConnectionPatch(xyA=xy, xyB=(base_x,offset_y), coordsA="data", coordsB="data",
                          axesA= axes[idx,av], axesB= axes[idx,av], color=c, lw=2)
                axes[idx,av].add_artist(con)
                con = ConnectionPatch(xyA=xy, xyB=(base_x,offset_y), coordsA="data", coordsB="data",
                          axesA= axes[neighbour_idx_ed,av], axesB= axes[neighbour_idx_ed,av], color=c, lw=2)
                axes[neighbour_idx_ed,av].add_artist(con)
            con = ConnectionPatch(xyA=xy, xyB=xy, coordsA="data", coordsB="data",
                          axesA= axes[idx,av], axesB= axes[neighbour_idx_ed,av], color=c, lw=2)
            axes[idx,av].add_artist(con)
            con = ConnectionPatch(xyA=xy, xyB=xy, coordsA="data", coordsB="data",
                          axesA= axes[neighbour_idx_ed,av], axesB= axes[idx,av], color=c, lw=2)
            axes[neighbour_idx_ed,av].add_artist(con)
        
          
    plt.tight_layout()
    #plt.show()
    plt.savefig(plot_fname, dpi=600)
    print("Saved " + plot_fname)
    
#run("../temp/test_minecover_discrete.txt", "../temp/test_minecover_embedding.txt","../temp/embedding_lin.txt", "../temp/compare_cover_lin.pdf")
def main():
    # print command line arguments
    print(sys.argv)
    render_embedding(sys.argv[1],sys.argv[2],sys.argv[3],sys.argv[4])

if __name__ == "__main__":
    main()