#1) plot 1:
#   - show 2 pairs of time series
#   - show pattern-based embedding
#   - show bag-of-patterns
#2) plot 2:
#   - visualisation of transaction database
#4) Plot 3:
#   - visualisation of patterns
from sktime.utils.data_io import load_from_tsfile_to_dataframe
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import collections
from saxpy.znorm import znorm
from saxpy.paa import paa
from saxpy.sax import ts_to_string
from saxpy.alphabet import cuts_for_asize
import subprocess
from utils import *
import common_viz

# --------------------------- load --------------------
train = '../data/lin_clustering_data26.ts'
train_x, train_y = load_from_tsfile_to_dataframe(train) 


# --------------------------- pre-process: show two sample of each class  --------------------
ts_filtered = []
ts_labels = []
nr_series = train_y.shape[0]
print("#series:" + str(nr_series))
print("lengths:")
for i in range(0,nr_series):
    label = int(train_y[i])
    ts = train_x['dim_0'].iloc[i].to_numpy()
    ts_filtered.append(ts)
    ts_labels.append(label)
    print("series " + str(i) + " len=" + str(ts.shape[0]) + " first 10=" + str(ts[0:10]))

CLASSPATH = '../target/pets-0.1.0-jar-with-dependencies.jar:../lib/jmotif-sax-1.1.5-SNAPSHOT-jar-with-dependencies.jar'


# --------------------------- run pattern mining no gaps fast  --------------------
paa_win_absolute = 12
window = 50
alphabet=4
duration = 1.2
min_size = 6
max_size = 12
k = 1000
#output
preprocessing = '../temp/lin_preprocessing_no_sw.txt'
embedding_fname12 = '../temp/lin_embedding_bosp_fast.txt'
embedding_fname22 = '../temp/lin_embedding_bodsp_fast.txt'
#call
call = True
if call:
    subprocess.call(['java', '-cp', CLASSPATH, 'be.uantwerpen.mining.MineTopKSequentialPatterns', 
                     '-input', train,
                     '-window', str(window),
                     '-paa_win', str(paa_win_absolute), '-bins', str(alphabet), 
                     '-k', str(k), '-duration', str(duration), 
                     '-min_size', str(min_size), '-max_size', str(max_size), 
                     '-sort-alpha','true', #specific for visualisation embedding
                     '-output', embedding_fname12])
    
    #subprocess.call(['java', '-cp', CLASSPATH, 'be.uantwerpen.mining.MineTopKDiscriminativeSequentialPatterns', 
    #                 '-input', train,
    #                '-window', str(window),
    #                '-paa_win', str(paa_win_absolute), '-bins', str(alphabet), 
    #                '-k', str(k),   '-duration', str(duration), 
    #                '-min_size', str(min_size), '-max_size', str(max_size), 
    #                '-redundancy_cover', 'True',
    #                '-output_discretisation_no_sliding_window', preprocessing,
    #                '-output', embedding_fname22
    #               ])


# --------------------------- run bag of patterns  --------------------
paa_win_absolute = 12
window = 50
alphabet=4
#output
embedding_fname2 = '../temp/lin_embedding_bop.txt'
if call:
    subprocess.call(['java', '-cp', CLASSPATH, 'be.uantwerpen.mining.BagOfPatterns', '-input', train,
                '-paa_win', str(paa_win_absolute), '-bins', str(alphabet), 
                '-window', str(window),
                '-output', embedding_fname2])


# ----------------------------------------- PLOT 1  ---------------------------------------- 
common_viz.render_embedding(preprocessing,embedding_fname12, embedding_fname2, 'PETSC', 'BOP', '../temp/embedding_vis_bop_vs_bosp_fast_impl.pdf', filter=True)
#common_viz.render_embedding(preprocessing,embedding_fname22, embedding_fname2, 'PETSC-DISC', 'BOP', '../temp/embedding_vis_bop_vs_bodsp_fast_impl.pdf', filter=True)
