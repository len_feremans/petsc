mvn clean install assembly:single -DskipTests=True
cd python
#
# ----------- PLOT / RUN Grid search Adiac Beef  ----------
#
#plot grid for mining and sax paramaters on Beef and Adiac
python3.8 -u experiment_1_parameters_petsc.py #> exp1.log
#
# ----------- COMPARE STOTA  ----------
#
#compare petsc/petsc-soft/petsc-disc/mr-petch/BOP on selection SAX-VSM
#1)run_petsc_classic_with_parameter_optimisation
#2)run_bag_of_patterns_with_parameter_optimisation
#3)run_mr_petsc_parameter_optimisation
#4)run_petsc_disc_with_parameter_optimisation
#5)run_petsc_soft_with_parameter_optimisation
python3.8  -u experiment_2_variantions_petsc.py #> exp2.log
#run mr_petsc on multivariate:
python3.8  -u experiment_3_multivariate_petsc.py #> exp3.log
#run BOSS, MrSEQL, DTW, MR-PETSC-DEFAULT, MR-PETCH-WITH-PARAMETER-OPTIMALISATION on 85 bake-off datasets
python3.8  -u experiment_6_runtime.py ># exp6.log
#
# ----------- PLOTS / Visualisation Histogram / TOP-k Patterns / Attribution on in_clustering_data26.ts/GunPoint/ECG200 ----------
#
#plot on lin_clustering_data26.ts of BOP versus PETSC
python3.8  -u experiment_4_show_embedding.py
#plot on lin_clustering_data26.ts of PETSC-DISC most discriminative patterns
python3.8  -u experiment_5_show_patterns.py
#plot of attribution MR-PETSCH on on GunPoint, ECG200, Coffee and TwoPatterns
test_filename = '../data/Univariate_ts/GunPoint/GunPoint_TEST.ts'
params = {'w':20, 'alphabet':12, 'k':250,  'min_len':6, 'rdur': 1.0}
python3.8  -u experiment_7_show_patterns2.py
